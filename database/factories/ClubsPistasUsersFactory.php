<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
 		$year = '2018';
        $month = rand(1, 12);
        $day = rand(1, 28);

        $date = Carbon::create($year,$month ,$day , 0, 0, 0);
        $hora = $faker->time($format = 'H:i:s', $max = '21:00:00'); // '20:49:42'
    return [
        'clubs_id' 			=> $faker->numberBetween($min = 1, $max = 30),
        'pista_id' 			=> $faker->numberBetween($min = 1, $max = 20),
        'user_id' 			=> $faker->numberBetween($min = 1, $max = 300),
        'inicio_alquiler' 	=> $date . " " . $hora,
        'fin_alquiler' 		=> $date . " " . $hora + '01:30:00',
        'status'			=> $faker->randomElement(['LIBRE', 'OCUPADA']),
    ];
});
