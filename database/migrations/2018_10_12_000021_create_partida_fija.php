<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidaFija extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas_fijas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubs_id')->unsigned();
            $table->integer('pistas_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('inicio_alquiler');


            $table->index(['clubs_id', 'pistas_id', 'inicio_alquiler']);
            $table->unique(['clubs_id', 'pistas_id', 'inicio_alquiler']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('partidas_fijas');
        Schema::enableForeignKeyConstraints();
    }
}
