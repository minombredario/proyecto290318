<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClubsInstalacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('clubs_instalaciones', function (Blueprint $table) {
            //Relacion

            $table->foreign('instalaciones_id')->references('id')->on('instalaciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('clubs_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
