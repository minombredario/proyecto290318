<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('clubs_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('poblacion_id')->references('id')->on('poblaciones')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('nivel_id')->references('id')->on('niveles')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('rol_id')->references('id')->on('roles')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
