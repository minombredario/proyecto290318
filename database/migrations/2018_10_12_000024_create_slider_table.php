<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('imagen',125);
            $table->string('text',125)->nullable();
            $table->string('caption',125)->nullable();
            $table->string('alt',125)->nullable();
            $table->string('web',125)->nullable();
            $table->string('title',125)->nullable();
            $table->enum('status',  ['BORRADOR','PUBLICADO'])->default('BORRADOR');
            $table->boolean('visible')->default(true);
            

            //Relacion

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('sliders');
        Schema::enableForeignKeyConstraints();
    }
}
