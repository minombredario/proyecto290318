<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoblacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poblaciones', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('postal')->unsigned();
            $table->string('nombre', 128);
            $table->string('slug', 128);
            $table->integer('provincia_id')->unsigned();
            $table->decimal('latitud', 9, 6)->nullable();
            $table->decimal('longitud', 9, 6)->nullable();
            
            //Relacion

            $table->foreign('provincia_id')->references('id')->on('provincias')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['postal', 'nombre']);
            $table->unique(['postal', 'nombre']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('poblaciones');
        Schema::enableForeignKeyConstraints();
    }
}
