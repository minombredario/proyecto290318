<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',128)->unique();
            $table->string('slug', 128);
            $table->string('cif',9);
            $table->string('web',128)->unique();
            $table->string('logo',250);
            $table->integer('telefono')->nullable();
            $table->integer('movil')->nullable();
            $table->string('email', 128)->unique();
            $table->string('password');
            $table->string('responsable',128);
            $table->integer('pistas')->unsigned();
            $table->string('horario',255);
            $table->integer('rol_id') ->unsigned()->default('3');
            $table->string('direccion');
            $table->integer('poblacion_id')->unsigned();
            $table->decimal('latitud', 9, 6)->nullable();
            $table->decimal('longitud', 9, 6)->nullable();
            $table->boolean('cards')->default(TRUE);
            $table->boolean('paralax')->default(TRUE);
            $table->string('imgparalax',255)->nullable();
            $table->boolean('galeria')->default(TRUE);
            $table->boolean('jumbotron')->default(TRUE);
            $table->boolean('imageslider')->default(TRUE);
            $table->boolean('bqsocial')->default(TRUE);
            $table->boolean('patrocinadores')->default(TRUE);
            $table->boolean('brsocial')->default(TRUE);
            $table->boolean('columnas')->default(TRUE);
            $table->boolean('welcome')->default(TRUE);
            $table->boolean('noticias')->default(TRUE);

            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clubs');
        Schema::enableForeignKeyConstraints();
    }
}
