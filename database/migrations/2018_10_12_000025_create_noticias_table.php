<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('titulo',125);
            $table->string('slug',125);
            $table->string('titular',125);
            $table->text('contenido');
            $table->string('imagen',125);
            $table->string('web',125);
            $table->enum('status',  ['BORRADOR','PUBLICADO'])->default('BORRADOR');
            $table->boolean('visible')->default(true);

            //Relacion

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id', 'slug']);
            $table->unique(['club_id', 'slug']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('noticias');
        Schema::enableForeignKeyConstraints();
    }
}
