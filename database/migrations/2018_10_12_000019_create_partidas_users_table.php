<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas_user', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('partida_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('pala')->nullable();
            $table->string('pala2')->nullable();
            $table->string('pala3')->nullable();
            $table->string('pala4')->nullable();

            //$table->rememberToken();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('partidas_user');
        Schema::enableForeignKeyConstraints();
    }
}
