<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsInstalacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs_instalaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubs_id')->unsigned();
            $table->integer('instalaciones_id')->unsigned();
            $table->unique(['clubs_id', 'instalaciones_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clubs_instalaciones');
        Schema::enableForeignKeyConstraints();
    }
}
