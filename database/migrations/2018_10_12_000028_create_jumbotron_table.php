<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJumbotronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jumbotron', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('titulo',128);
            $table->text('contenido');
            $table->string('footer',128)->nullable();
            $table->string('nombre',128)->nullable();
            $table->string('web',255)->nullable();
            $table->boolean('visible')->default(true);

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id', 'titulo']);
            $table->unique(['club_id', 'titulo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('jumbotron');
        Schema::enableForeignKeyConstraints();
    }
}
