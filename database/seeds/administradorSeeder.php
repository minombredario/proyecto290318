<?php

use Illuminate\Database\Seeder;

class administradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administradores')->insert([
            ['nombre' => 'Darío', 'email' => 'minombredario@gmail.com', 'rol_id' => 1 , 'password' =>  bcrypt(123456)]
        ]);
    }
}
