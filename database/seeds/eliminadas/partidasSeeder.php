<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
//php artisan db:seed --class=clubsPartidasSeeder
class partidasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubs = App\Clubs::all()->toArray();
        
        for($i = 1; $i <= 20; $i++){
            $year = "2018";
            $month = rand(4,6);
            $day = rand(1,28);
            $hour = rand(8,23);
            $medias = array('00', '30');
            $minute = $medias[array_rand($medias,1)];
            $second = '00';
            $inicio = Carbon::create($year, $month, $day, $hour, $minute, $second);
            
            $minute2 = $inicio->minute == 30 ? '00' : '30';
            $hour2 = $inicio->minute == 30 ? $inicio->hour + 2 :  $inicio->hour + 1;

            $fin = Carbon::create($year, $month, $day, $hour2, $minute2, $second);
            
            $club = $clubs[array_rand($clubs,1)];
            App\Partidas::create([
                'clubs_id'          => $club['id'],
                'user_id'          => rand(1,300),
                'pistas_id'         => rand(1, $club['pistas']),
                'inicio_alquiler'   => $inicio,
                'fin_alquiler'      => $fin,
                'status'            => 'OCUPADA',

            ])->each(function(App\Partidas $partida){
                $partida->users()->attach([
                    'user_id' => rand(1,300),
                    
                ]);
            });
         };
     		
    }
}

