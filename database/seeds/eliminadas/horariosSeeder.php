<?php

use Illuminate\Database\Seeder;

class horariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('horarios')->insert([
            ['horas' => '00:30:00'],
            ['horas' => '01:00:00'],
            ['horas' => '01:30:00'],
            ['horas' => '02:00:00'],
            ['horas' => '02:30:00'],
            ['horas' => '03:00:00'],
            ['horas' => '03:30:00'],
            ['horas' => '04:00:00'],
            ['horas' => '04:30:00'],
            ['horas' => '05:00:00'],
            ['horas' => '05:30:00'],
            ['horas' => '06:00:00'],
            ['horas' => '06:30:00'],
            ['horas' => '07:00:00'],
            ['horas' => '07:30:00'],
            ['horas' => '08:00:00'],
            ['horas' => '08:30:00'],
            ['horas' => '09:00:00'],
            ['horas' => '09:30:00'],
            ['horas' => '10:00:00'],
            ['horas' => '10:30:00'],
            ['horas' => '11:00:00'],
            ['horas' => '11:30:00'],
            ['horas' => '12:00:00'],
            ['horas' => '12:30:00'],
            ['horas' => '13:30:00'],
            ['horas' => '14:00:00'],
            ['horas' => '14:30:00'],
            ['horas' => '15:00:00'],
            ['horas' => '15:30:00'],
            ['horas' => '16:00:00'],
            ['horas' => '16:30:00'],
            ['horas' => '17:00:00'],
            ['horas' => '17:30:00'],
            ['horas' => '18:00:00'],
            ['horas' => '18:30:00'],
            ['horas' => '19:00:00'],
            ['horas' => '19:30:00'],
            ['horas' => '20:00:00'],
            ['horas' => '20:30:00'],
            ['horas' => '21:00:00'],
            ['horas' => '21:30:00'],
            ['horas' => '22:00:00'],
            ['horas' => '22:30:00'],
            ['horas' => '23:00:00'],
            ['horas' => '23:30:00'],
            ['horas' => '00:00:00']
        ]);
    }
}
