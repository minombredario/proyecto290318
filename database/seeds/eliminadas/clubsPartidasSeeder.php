<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
//php artisan db:seed --class=clubsPartidasSeeder
class clubsPartidasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $users = App\User::all()->toArray();
        $clubs = App\Clubs::all()->toArray();
        $clubPistas = 0;

        for($i = 1; $i <= 200; $i++){
            $year = "2018";
            $month = rand(4,6);
            $day = rand(1,28);
            $hour = rand(8,23);
            $medias = array('00', '30');
            $minute = $medias[array_rand($medias,1)];
            $second = '00';
            $inicio = Carbon::create($year, $month, $day, $hour, $minute, $second);
            
            $minute2 = $inicio->minute == 30 ? '00' : '30';
            $hour2 = $inicio->minute == 30 ? $inicio->hour + 2 :  $inicio->hour + 1;

            $fin = Carbon::create($year, $month, $day, $hour2, $minute2, $second);
            
            
            //$club = $clubs[array_rand($clubs,1)];
            //$users = DB::table('users')->select('*')->where('clubs_id', '=', $club)->toArray();
            //$users = App\User::where('clubs_id','=', $club['id'])->get();
            $user = $users[array_rand($users,1)];

            for ($x = 0; $x < count($clubs); $x++){
                if($clubs[$x]['id'] == $user['clubs_id']){
                    $clubPistas = $clubs[$x]['pistas'];
                }
            }
            //$club = App\Clubs::where('id','=', $user['clubs_id'])->get();
            //$club = $club->toArray();

                      
            App\Partidas::create([
                'clubs_id'         => $user['clubs_id'],
                'user_id'          => $user['id'],
                'pistas_id'        => rand(1, $clubPistas),
                'inicio_alquiler'  => $inicio,
                'fin_alquiler'     => $fin,
                'status'           => 'CERRADA',

            ])->each(function(App\Partidas $partida ){
                $partida->partidaJugador()->attach([
                    $partida->id => [
                        'user_id'    => $partida->user_id,
                        'pala'       => 'user',
                        'pala2'      => 'pala2',
                        'pala3'      => 'pala3',
                        'pala4'      => 'pala4',
                    ]
                
                ]);                
            });
        };
            
    }
}

/*
$user->clubs()->attach([
                $user->clubs_id => [
                    'user_email' => $user->email, //asigno al id del club el email del usuario inertandolo a traves de su id.
                ]
                
            ]);
*/