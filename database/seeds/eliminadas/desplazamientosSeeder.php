<?php

use Illuminate\Database\Seeder;

class desplazamientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('desplazamientos')->insert([
            ['nombre' => '5'],
            ['nombre' => '10'],
            ['nombre' => '15'],
            ['nombre' => '20'],
            ['nombre' => '25'],
           	['nombre' => '30'],
        ]);
    }
}
