<?php

use Illuminate\Database\Seeder;

class diasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dias')->insert([
            ['nombre' => 'LUNES', 'slug' => 'lunes'],
            ['nombre' => 'MARTES', 'slug' => 'martes'],
            ['nombre' => 'MIERCOLES', 'slug' => 'miercoles'],
            ['nombre' => 'JUEVES', 'slug' => 'jueves'],
            ['nombre' => 'VIERNES', 'slug' => 'viernes'],
            ['nombre' => 'SABADO', 'slug' => 'sabado'],
            ['nombre' => 'DOMINGO', 'slug' => 'domingo']
        ]);
    }
}
