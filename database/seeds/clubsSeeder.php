<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class clubsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	factory(App\Clubs::class,30)->create()->each(function(App\Clubs $club){
            $club->instalaciones()->attach([
        		rand(1,3),
        		rand(4,6),
        		rand(7,9),
        	]);
            /*Schema::create("customTable" . $club->id, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('club_id')->unsigned();
                $table->boolean('cards')->default(TRUE);
                $table->boolean('fixedBackground')->default(TRUE);
                $table->boolean('galeria')->default(TRUE);
                $table->boolean('jumbotron')->default(TRUE);
                $table->boolean('imageSlider')->default(TRUE);
                $table->boolean('social')->default(TRUE);
                $table->boolean('patrocinadores')->default(TRUE);
                $table->boolean('navBarSocial')->default(TRUE);
                $table->boolean('twoColumns')->default(TRUE);
                $table->boolean('welcome')->default(TRUE);
                
                $table->foreign('club_id')->references('id')->on('clubs')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

                $table->index(['club_id']);
                $table->unique(['club_id']);
            });*/
        });
    }
}
