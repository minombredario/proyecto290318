<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
/*Route::middleware('auth:admin')->get('/admin', function (Request $request) {
    return $request->admin();
});*/
/*Route::group(['prefix' => 'club/api', 'middleware' => 'auth:club'], function(){
  Route::get('/club', function( Request $request ){
    return $request->user();
  });
});
*/
Route::group(['prefix' => 'auth'], function($router) {

	Route::post('login', 		'AuthController@login');
	Route::post('logout', 		'AuthController@logout');
	Route::post('refresh', 		'AuthController@refresh');
	Route::post('me', 			'AuthController@me');
});

Route::group(['prefix' => 'admin/api'], function(){
	Route::get('cmbClubs', 			'Web\CombosController@comboClubs');
	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');

});

Route::group(['prefix' => 'club/api'], function(){
	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');
	Route::get('cmbClubs', 			'Web\CombosController@comboClubs');

});

Route::prefix('api')->group(function(){
	Route::get('jumbotron/{id}', 			'Web\PageController@getJumbotron');
	Route::get('columnas/{id}', 			'Web\PageController@getColumnas');
	Route::get('welcome/{id}', 				'Web\PageController@getWelcome');
	Route::get('social/{id}',				'Web\PageController@getSocial');
	Route::get('patrocinadores/{id}',		'Web\PageController@getPatrocinadores');
	Route::get('noticias/{id}', 			'Web\PageController@getNoticias');
	Route::get('lstnoticias', 				'Web\PageController@indexNoticias');
	Route::get('partidas', 					'Web\PageController@getPartidas');
	Route::get('galeria/{id}', 				'Web\PageController@getGalerias');
	Route::get('glrs/{galeria}', 			'Web\PageController@galeria');
	Route::get('slider/{id}', 				'Web\PageController@getSlider');

	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');
	Route::get('cmbClubs', 			'Web\CombosController@comboClubs');
	
	Route::post('registerUser', 		'userAuth\RegisterController@register');
});

//Route::group(['middleware' => 'jwt.auth'], function ($router) {





/*Route::prefix('club/ver/api')->group(function(){
	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');

});*/