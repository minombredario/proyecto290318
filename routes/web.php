	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');
*/
//Route::get('/{club}' , 'HomeController@index');
Route::get('/' , 'HomeController@index');
Auth::routes();


/*Route::get('/actualizar', function(){
    exec('composer dump-autoload');
    echo 'composer dump-autoload complete';
});
*/
/*Route::get('/cache', function () {
    $exitCode = Artisan::call('cache:clear');
});*/

//Admin 

//Route::prefix('admin')->group(function(){
Route::group(['prefix' => 'admin'], function(){
	// Authentication Routes...
	Route::get('iniciarSesion', 			'adminAuth\LoginController@showLoginForm')->name('admin.login');
	Route::post('iniciarSesion', 			'adminAuth\LoginController@login');
	Route::get('logout', 					'adminAuth\LoginController@logout');

	// Password Reset Routes...
	Route::get('admin-password/reset', 				'adminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('admin-password/email', 			'adminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('admin-password/reset/{token}', 		'adminAuth\ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('admin-password/reset', 			'adminAuth\ResetPasswordController@reset');


	Route::get('dashboard',    	'Admin\AdminController@index')->name('admin.dashboard');
	Route::resource('prvs',		'Admin\ProvinciasController', ['except' => ['create']]);
	Route::resource('pbls',		'Admin\PoblacionesController', ['except' => ['create']]);
	Route::resource('lvls',		'Admin\NivelesController', ['except' => ['create']]);
	Route::resource('psts',		'Admin\PistasController', ['except' => ['create']]);
	Route::resource('nsts',		'Admin\InstalacionesController', ['except' => ['create']]);
	Route::resource('rls', 		'Admin\RolesController', ['except' => ['create']]);
	Route::resource('clbs', 		'Admin\ClubsController', ['except' => ['create']]);
	Route::resource('ptds', 		'Admin\PartidasController', ['except' => ['create']]);
	Route::resource('pyrs', 		'Admin\JugadoresController', ['except' => ['create']]);

	//upload imagenes
	Route::post('clbs/update/{id}', 	'Admin\ClubsController@update');
	Route::post('pyrs/update/{id}', 	'Admin\JugadoresController@update');

});

//Club
//Route::prefix('club')->group(function(){
Route::group(['prefix' => 'club'], function(){
	// Authentication Routes...
	Route::get('iniciarSesion', 			'clubAuth\LoginController@showLoginForm')->name('club.login');
	Route::post('iniciarSesion', 			'clubAuth\LoginController@login');
	Route::get('logout', 					'clubAuth\LoginController@logout');

	// Password Reset Routes...
	Route::get('admin-password/reset', 				'clubAuth\ForgotPasswordController@showLinkRequestForm')->name('club.password.request');
	Route::post('admin-password/email', 			'clubAuth\ForgotPasswordController@sendResetLinkEmail')->name('club.password.email');
	Route::get('admin-password/reset/{token}', 		'clubAuth\ResetPasswordController@showResetForm')->name('club.password.reset');
	Route::post('admin-password/reset', 			'clubAuth\ResetPasswordController@reset');


	Route::get('dashboard',    					'Club\ClubController@index')->name('club.dashboard');
	Route::get('jugadores', 					'Club\JugadoresController@viewIndex');
	Route::get('ver/{id}', 						'Club\JugadoresController@show');
	Route::resource('pyrs', 					'Club\JugadoresController', ['except' => ['create']]);
	Route::post('pyrs/update/{id}', 			'Club\JugadoresController@update');
	Route::get('clbs/{id}', 					'Club\ClubController@show');
	Route::post('clbs/update/{id}', 			'Club\ClubController@update');
	
	//rutas galeria
	Route::get('glrs', 							'Club\GaleriaController@listaGalerias');
	Route::delete('glrs/{galeria}/{imagen}',  	'Club\GaleriaController@eliminarImagen');
	Route::delete('glrs/{galeria}',  			'Club\GaleriaController@eliminarGaleria');
	Route::get('glrs/{galeria}', 				'Club\GaleriaController@galeria');
	Route::post('glrs', 						'Club\GaleriaController@crearGaleria');
	Route::patch('glrs/{galeria}', 				'Club\GaleriaController@update');
	Route::post('glrs/subirImagenes', 			'Club\GaleriaController@subirImagenes');
	
	//rutas patrocinadores
	Route::get('ptdr', 							'Club\PatrocinadorController@listaPatrocinadores');
	Route::delete('ptdr/{id}',  				'Club\PatrocinadorController@destroy');
	Route::post('ptdr', 						'Club\PatrocinadorController@crearPatrocinador');
	Route::post('ptdr/update/{id}', 			'Club\PatrocinadorController@update');

	//rutas slider
	Route::resource('sldr', 					'Club\SliderController', ['only' => ['index']]);
	Route::delete('sldr/{id}',  				'Club\SliderController@destroy');
	Route::post('sldr', 	 					'Club\SliderController@slider');

	//rutas partidas
	Route::resource('ptds', 					'Club\PartidasController', ['except' => ['create']]);
	Route::patch('ptds/posicion/{id}', 			'Club\PartidasController@updatePos');

	//rutas noticias
	Route::resource('ntcs',						'Club\NoticiasController', ['except' => ['create']]);
	Route::post('ntcs/imagen',					'Club\NoticiasController@subirImagen');
	Route::post('ntcs/update/{id}', 			'Club\NoticiasController@update');

	//rutas club
	Route::get('content/jumbotron', 			    'Club\ClubController@indexJumbotron');
	Route::post('content/jumbotron', 				'Club\ClubController@jumbotron');
	Route::get('content/columnas', 					'Club\ClubController@getColumnas');
	Route::post('content/columnas', 				'Club\ClubController@columnas');
	Route::get('content/welcome', 					'Club\ClubController@getWelcome');
	Route::post('content/welcome', 					'Club\ClubController@welcome');
	Route::get('content/social', 					'Club\ClubController@getSocial');
	Route::post('content/social', 					'Club\ClubController@social');
	Route::post('content/paralax', 					'Club\ClubController@paralax');
	Route::patch('content/update/{id}', 			'Club\ClubController@updateMod');
	//rutas clases y competiciones
	Route::resource('cpt', 			    				'Club\CompeticionController', ['except' => ['create']]);
	Route::resource('cls', 			    				'Club\CLasesController', ['except' => ['create']]);
});



Route::post('pyrs', 							'Web\PageController@registerUser');

Route::get('iniciarSesion', 			'userAuth\LoginController@showLoginForm')->name('user.login');
Route::post('iniciarSesion', 			'userAuth\LoginController@login');
Route::get('logout', 					'userAuth\LoginController@logout');

// Password Reset Routes...
Route::get('password/reset', 			'userAuth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 			'userAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 	'userAuth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 			'userAuth\ResetPasswordController@reset');

