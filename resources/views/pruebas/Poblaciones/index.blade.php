@extends('layouts.back')

@section('content')  

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row add">
                <div class="col-md-12 text-center">
                    <h1>Poblaciones</h1>
                </div>
            </div>
            
            <!--pre>@{{$data}}</pre-->
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-hover">
                        
                        @include('Admin.Poblaciones.partials.fields') 

                    </table>
                </div>
            </div>
            @include('pagination')
        </div>
        <div class="col-md-6">
            <div class="form-group row add">
                <div class="col-md-12 text-center">
                    <h1>Provincias</h1>
                </div>
            </div>
            
            <!--pre>@{{$data}}</pre-->
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-hover">
                        
                        @include('Admin.Provincias.partials.fields') 

                    </table>
                </div>
            </div>
            @include('pagination2')
        </div>
    
</div>



    

@endsection
