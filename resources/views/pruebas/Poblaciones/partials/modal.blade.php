<!-- Ventana modale crear -->
<div class="modal fade" id="create-item" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Añadir Registro</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" v-on:submit.prevent="crear" id="upload"><!--envio del formulario crear-->
                    <!--input type="text" name="_token"  class="form-control" value="{{ csrf_token()}}"></input-->

                    <div class="form-group">
                        <label for="title">Población:</label>
                        <input type="text" id="n-pob" name="nombre" class="form-control" v-model="nuevo.nombre"><!-- cambio datos en el modelo-->
                        </input>
                        <span v-if="formErrors['nombre']" class="error text-danger"><!--tratamiento de errores-->
                            @{{ formErrors['nombre'] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="title">Provincia:</label>
                        <provincias v-bind:eleccion="nuevo">Elige una provincia</provincias>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Añadir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Item Modal -->
<div class="modal fade" data-backdrop="false" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Editar Registro</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" v-on:submit.prevent="actualizar(fillItem.id)">
                    <div class="form-group">
                        <label for="title">Id:</label>
                        <input type="" name="id" class="form-control" v-model="fillItem.id" readonly="readonly"/>
                        <span v-if="formErrorsUpdate['id']" class="error text-danger">
                            @{{ formErrorsUpdate['id'] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="title">Poblacion:</label>
                        <input type="text" name="nombre" class="form-control" v-model="fillItem.nombre">
                        </input>
                        <span v-if="formErrorsUpdate['nombre']" class="error text-danger"><!--tratamiento de errores-->
                            @{{ formErrorsUpdate['nombre'] }}
                        </span>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="title">Provincia:</label>
                            <provincias v-bind:eleccion="fillItem"><template v-model="fillItem">@{{fillItem.provincia}}</template></provincias>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Ventana modal eliminar --> 
<div class="modal fade" data-backdrop="false" id="eliminar-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Eliminar «<template v-model="fillItem">@{{fillItem.nombre}}</template>»</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" v-on:submit.prevent="eliminar(fillItem.id)">

                    <template v-model="fillItem">
                        <p>¿Quieres borrar @{{fillItem.nombre}} de la provincia de @{{fillItem.provincia}}?</p>
                        <p>Esta acción no puede deshacerse.</p>
                    </template>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Eliminar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Ventana modal laoding --> 
<div class="modal fade" id="loading" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog text-center" role="document">
        <img style="width:30px" src="/img/preload/snake.gif" alt="loading" /> 
        
    </div>
</div>
