<tr>
    <td colspan="4">
        <div class="row">
            <div colspan="2" class="col-xs-12 col-md-6">
                <input type="text" id="buscar" class="form-control" placeholder="Buscar Población">
            </div> 
            <div class="col-xs-12 col-md-6">
                <button class="btn btn-primary">
                    <span class="glyphicon glyphicon-search"></span>
                </button>

                <button class="btn btn-primary" >
                    <span class="glyphicon glyphicon-refresh"></span>
                </button>
                <button type="button" @click="openModal('poblacion','añadir')" class="btn btn-primary pull-right" >
                    <span class="glyphicon glyphicon-plus"></span> Añadir
                </button>
            </div>
        </div>
    </td>    
</tr>
<tr>
    <th style="width: 10%">#</th>
    <th style="width: 30%">Población</th>
    <th style="width: 30%">Provincia</th>
    <th style="width: 30%">&nbsp;</th>
</tr>

<tr v-for="item in poblaciones" class="text-capitalize"><!--iteramos el array de poblaciones-->
    <td>@{{item.id}}</td>
    <td class="center">@{{item.nombre}}</td>
    <td>@{{item.provincia.nombre}}</td>
    <td class="text-right">
        <button class="edit-modal btn btn-warning" @click="openModal('poblacion','editar', item)"><!--llamamos a la funcion editar pasando el objeto-->
            <span class="glyphicon glyphicon-edit" title="Editar"></span>
        </button>
        <button class="edit-modal btn btn-danger" @click="openModal('poblacion','borrar', item)"><!--llamamos a la funcion eliminar pasando el objeto-->
            <span class="glyphicon glyphicon-trash" title="Borrar"></span>
        </button>
    </td>
</tr>


<!-- Ventana modale -->
<div class="modal fade" id="modal-pob" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel" >@{{ titulo }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Población:</label>
                    <input type="text" id="n-pob" name="nombre" class="form-control" v-model="nuevo.nombre"><!-- cambio datos en el modelo-->
                    </input>
                    <span v-if="formErrors['nombre']" class="error text-danger"><!--tratamiento de errores-->
                        @{{ formErrors['nombre'] }}
                    </span>
                </div>
               
                    <app-prov v-on:prov="provincia"></app-prov>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-success" @click.prevent="crear(poblacion)">@{{ boton }}</button>
                </div>
           
            </div>
        </div>
    </div>
</div>