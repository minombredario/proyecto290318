
<div class="container" id="listados">

    <div class="mensaje" v-bind:class="clases" v-if="mostrar">
	<p>@{{mensaje}}</p>
        <span class="glyphicon glyphicon-remove pull-right hider" @click="mostrar = !mostrar"></span>
    </div>
    
    <div class="form-group row add">
        <div class="col-md-12 text-center">
            <h1>Provincias</h1>
        </div>
    </div>
        <!--pre>@{{$data}}</pre-->
    <div class="row">
        <div class="table-responsive">
            <table class="table table-hover">
                <template v-if="loading == true" data-toggle="modal" data-backdrop="false" data-target="#loading"></template>
                @include('provincias.partials.fields')
            </table>
        </div>
    </div>
    @include('pagination')
    @include('provincias.partials.modal')
</div>

<script src="{{asset('js/funcionesVue.js') }}"></script>


