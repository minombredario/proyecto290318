<tr>
    <td colspan="4">
        <div class="row">
            <div colspan="2" class="col-xs-12 col-md-6">
                <input type="text" id="buscar" class="form-control" placeholder="Buscar Provicia">
            </div> 
            <div class="col-xs-12 col-md-6">
                <button class="btn btn-primary">
                    <span class="glyphicon glyphicon-search"></span>
                </button>

                <button class="btn btn-primary" >
                    <span class="glyphicon glyphicon-refresh"></span>
                </button>
                <button type="button" data-toggle="modal" data-backdrop="false" data-target="#create-item" class="btn btn-primary pull-right" >
                    <span class="glyphicon glyphicon-plus"></span> Añadir
                </button>
            </div>
        </div>
    </td>    
</tr>
<tr>
    <th class="col-md-2">#</th>
    <th class="col-md-6">Provincia</th>
    <th class="col-md-4">&nbsp;</th>
</tr>

<tr v-for="item in provincias" class="text-capitalize"><!--iteramos el array de provincias-->
    <td>@{{item.id}}</td>
    <td>@{{item.nombre}}</td>
    <td class="text-right">
        <button class="edit-modal btn btn-warning"><!--llamamos a la funcion editar pasando el objeto-->
            <span class="glyphicon glyphicon-edit" title="Editar"></span>
        </button>
        <button class="edit-modal btn btn-danger"><!--llamamos a la funcion eliminar pasando el objeto-->
            <span class="glyphicon glyphicon-trash" title="Borrar"></span>
        </button>
    </td>
</tr>

