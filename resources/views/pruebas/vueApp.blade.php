<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">

		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
				]) !!};
		</script>
	</head>
	<body>
		<div id="app" v-cloak>
			@yield('content')
		</div>



		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/extras.js') }}"></script>
	</body>
</html>