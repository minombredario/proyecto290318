<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		 <meta charset="utf-8">
		 <meta http-equiv="X-UA-Compatible" content="IE=edge">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- CSRF Token -->
		 <meta name="csrf-token" content="{{ csrf_token() }}">
		 <title>{{ config('app.name', 'Laravel') }}</title>
		 <!-- Styles -->
		
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		 <link href="{{ asset('css/app.css') }}" rel="stylesheet">
		 <link href="{{ asset('css/style.css') }}" rel="stylesheet">
		
		 <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	</head>
	<body>
	 <div id="app"></div>

	
	
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <!--script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script-->
    <script src="{{ asset('js/app.js') }}"></script>
	
	</body>
</html>