<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }} | @yield('title') </title>
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
        <link rel="stylesheet" href="{{ asset('css/pe-icon-7-stroke.css') }}"/>
        <!--  Light Bootstrap Table core CSS    -->
        <link rel="stylesheet" href="{{ asset('css/light-bootstrap-dashboard.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/toastr.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
       
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        @yield('style')
    </head>
    <body>
        <div id="app1">
        	<div class="wrapper">
                <div class="sidebar collapse navbar-collapse show" data-color="green" data-image="" id="navbar1">

                    <!--data-color="blue | azure | green | orange | red | purple"
                       

                    -->

                    <div class="sidebar-wrapper">
                        <div class="logo">
                            <a href="#" class="simple-text">
                                {{ config('app.name', 'Padel') }}
                            </a>
                        </div>
                        <nav class="nav flex-column">
                            <a class="nav-link active" href="#">Active</a>
                            <a class="nav-link" href="#">Link</a>
                            <a class="nav-link" href="#">Link</a>
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </nav>
                    </div>
                </div>

                <div class="main-panel">
                    <nav class="navbar navbar-default">

                        <div class="container-fluid fixed-top">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <!--button type="button" class="navbar-toggle" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button-->
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-left">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-refresh"></i> 17
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-comments-o"></i> 0
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            SEO
                                        </a>
                                    </li>
                                </ul>


                                
                            </div>
                        </div>

                    </nav>


                    <div class="content" style="min-width: 100%">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                	@yield('content')
                                </div>
                            </div>
                        </div>
                    </div>


                    <footer class="footer">
                        <div class="container-fluid">
                            <nav class="pull-left">
                                <ul>
                                    <li>
                                        <a href="#">
                                            Home 
                                        </a>
                                    </li>

                                </ul>
                            </nav>
                            <p class="copyright pull-right">
                                &copy; 2017 <a href="#">Darío Navarro</a>
                            </p>
                        </div>
                    </footer>
        			


                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
        @yield('script')

    </body>
</html>