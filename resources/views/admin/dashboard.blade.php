@extends('layouts.vueApp')

@section('content')
	@if(Auth::guard('admin')->check())
		<transition name="slide-fade" mode="out-in" appear>
			<router-view :datos="{{ json_encode($admin) }}"></router-view>
		</transition>
	@endif
@endsection

@section('scripts')
	
 <script type="text/javascript">


</script>
@endsection