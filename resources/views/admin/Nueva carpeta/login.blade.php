@extends('layouts.vueApp')

@section('css')
 <style rel="stylesheet">
	body{
		font-family: 'Roboto', sans-serif;
		background: url({{asset('img/fondo/fondo1.jpg')}}) no-repeat center center fixed;
		background-size: cover;
	}
	/*.main-section{
		margin-top: 130px;
	}
	.modal-content{
		background-color: #434e5a;
		opacity: .8;
		padding: 0 18px;
		border-radius: 10px;
	}
	.user-img img{
		height: 120px;
		width: 120px;
	}
	.user-img{
		margin-top: -60px;
		margin-bottom: 45px;
	}
	.form-group input{
		font-size: 18px;
		letter-spacing: 2px;
		padding-left: 54px;
	}
	.ico1::before, .ico2::before{
		font-family: "Ionicons";
		content: "\f3d9";
		position: absolute;
		font-size: 22px;
		left: 28px;
		padding-top: 4px;
		color: #555e60;
	}
	.ico2::before{
		content: "\f1af";
	}
	.form-input button{
		width: 40%;
		margin: 5px 0 25px;
	}
	.btn-success{
		background-color: #1c6288;
		font-size: 19px;
		border-radius: 5px;
		padding: 7px 14px;
		border: 1px solid #daf1ff;
	}
	.btn-success:hover{
		background-color: #13445e;
		border: 1px solid #daf1ff;
	}
	.forgot{
		padding: 5px 0 25px;
	}
	.forgot a{
		color: #fff;
	}*/

</style>
@endsection

@section('content')
	
	<!--div class="modal-dialog text-center">
		<b-row>
			<b-col sm="9" lg="12" class="mr-auto px-0 main-section">
				<b-row class="modal-content">
					<b-col cols="12" class="user-img">
						<b-img src="{{asset('img/avatar/face.png')}}" fluid rounded center />
					</b-col>

					<b-col cols="12">
						<form class="form-horizontal" method="POST" action="{{ route('admin.login') }}">
							{{ csrf_field() }}
							<b-form-group class="mb-5 ico1 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<b-form-input type="email" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus></b-form-input>
								@if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</b-form-group>
							<b-form-group class="mb-5 ico2 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<b-form-input type="password" name="password" id="password" placeholder="Password" required></b-form-input>
								@if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</b-form-group>
							<b-btn type="submit" variant="success" class="mb-2">Login</b-btn>
						</form>
					</b-col>
					<b-col cols="12" class="forgot">
						<b-link href="#">¿Recuperar contraseña?</b-link>
					</b-col>
				</b-row>
			</b-col>
		</b-row>
	</div-->
<login-component></login-component>
@endsection

@section('scripts')
	

@endsection