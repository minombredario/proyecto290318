<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRIErIpLW6wDyYGCe8R_BGedrucPNWpQA"></script>
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
				'roles' => Auth::user() ? Auth::user()->rol_id : '',
				]) !!};
		</script>
		

	</head>
		<body>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119550678-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  	gtag('config', 'UA-119550678-1');
		  	gtag('create', 'UA-119550678-1', 'auto');
			gtag('send', 'pageview');
		</script>
		
		@yield('css')
		<div id="app" v-cloak>
			@yield('content')   
		</div>
		@yield('scripts')


		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/toastr.js') }}"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	</body>
</html>