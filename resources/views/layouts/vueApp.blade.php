<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
		<link href="" rel='stylesheet' media='print'>
<!--link rel="stylesheet" href="//rawgit.com/seankenny/fullcalendar/v2/dist/fullcalendar.css" /-->

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRIErIpLW6wDyYGCe8R_BGedrucPNWpQA"></script>
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
				'roles' => Auth::user() ? Auth::user()->rol_id : '',
				'name' => Auth::user() ? Auth::user()->nombre : '',
				]) !!};
		</script>
		

	</head>
	<body>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119550678-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  	gtag('config', 'UA-119550678-1');
		  	gtag('create', 'UA-119550678-1', 'auto');
			gtag('send', 'pageview');
		</script>

		<!--script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-118585310-1', 'auto');
			ga('send', 'pageview');
		</script-->
		@yield('css')
		<div id="app" v-cloak>
			@yield('content')   
		</div>
		@yield('scripts')

		<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
		<!--script src="{{ asset('js/app.js') }}"></script-->
		<script src="{{ asset('js/toastr.js') }}"></script>
		
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		

	</body>
</html>