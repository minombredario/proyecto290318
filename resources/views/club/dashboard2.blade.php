@extends('layouts.vueApp')
@section('css')
	
<style>
	.wrapper {
	    display: flex;
	    align-items: stretch;
	}
</style>
@endsection
@section('content')
	@if(Auth::guard('club')->check())
	<div class="wrapper" >
		@include('club.partials.sidebar')
		<b-container fluid id="content">
			@include('club.partials.navBar')
			<b-container fluid id="content">
				@include('club.stats')
			</b-container>
		</b-container>
		
	</div>	
	@endif
@endsection

@section('scripts')
	
 <script type="text/javascript">


</script>
@endsection