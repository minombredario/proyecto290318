@extends('layouts.webLayout')

@section('content')
	<jugador-component :user="{{ json_encode($jugador) }}"></jugador-component>
@endsection