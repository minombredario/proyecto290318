import { setAuthorization } from "./general";

export function login(credenciales){
	return new Promise((res, rej) => {
		axios.post('/auth/login' , credenciales).then( (response) => {
			setAuthorization(response.data.access_token);
            res(response.data);
		}).catch((err) => {
			
			rej("Email o contraseña incorrectos");
			
		})
	})
}

export function getLocalUser(){
	const userStr = localStorage.getItem('user');

	if(!userStr){
		return null;
	}

	return JSON.parse(userStr)
}
