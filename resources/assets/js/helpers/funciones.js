export default{
   data: ()=>({
      apiUrl: 'api',

   }),

	created(){
      //this.apiUrl = `${window.location.host}/club/api`;
      //console.log(`${this.apiUrl}/cmbprvs`);
	},
	methods:{
		getComboProvincias(){
         axios.get(`${this.apiUrl}/cmbprvs`).then((response) =>{
           const provincias = response.data.map((provincia) =>{
             return {
               value: provincia.id,
               text: provincia.nombre,
            }
         });
           provincias.push({value: 0, text:'- Selecciona provincia -'});
           this.$store.commit('provincias', provincias);
        });
      },
      getPoblacion(){
         const me = this.$options.origen == "editar" ? this.edit_ : this.item;
         axios.get(`${this.apiUrl}/pblprv?busqueda=${me.postal}`).then((response)=>{
           if(response.data.length > 0){
             me.poblacion_id = response.data[0].id;
             me.poblacion = response.data[0].nombre;
             me.provincia_id = response.data[0].provincia_id;
             me.provincia = response.data[0].provincia.nombre;
          }else{
             me.poblacion = '';
          }
      }, (error) => {
        console.log(error.response);
         })
      },
      getNiveles(){
         axios.get(`${this.apiUrl}/cmbnvls`).then((response) =>{
            const niveles = response.data.map((nivel) =>{
               return {
                  value: nivel.id,
                  text: nivel.nombre,
               }
            });
            this.$store.commit('niveles', niveles);
         });
      },
      getComboInstalaciones(){
         axios.get(`${this.apiUrl}/cmbnsts`).then((response) =>{

           this.$store.commit('instalaciones', response.data);
        }, (error) => {
           console.log(error.response);
        })
      },
      getClubs(){
         axios.get(`${this.apiUrl}/cmbClubs`).then((response) => {

           const clubs = response.data.map((club) =>{
               return {
                  value: club.id,
                  text: club.nombre,
                  pistas: club.pistas,
               }
            });

           this.$store.commit('clubs', clubs);
         });
      },
      toFormData(obj,logo){ 
         let form_data = new FormData();
         for (let key in obj){
            if(key == 'logo' || key == 'avatar' || key == 'imagen'){
               form_data.append(key, logo);
            }else{
               form_data.append(key, obj[key]);
            }
         }
         return form_data;
      },
      formatHorario(horario){
         let salida = '';
         for(let key in horario){
            switch(key){
               case 'LV':
               salida += "Horario de lunes a viernes de " + horario[key] + "|";
               break;
               case 'LVM':
               salida += "Horario de lunes a viernes de " + horario[key];
               break;
               case 'LVT':
               salida += " y de " + horario[key] + "|";
               break;
               case 'LD':
               salida += "Horario de lunes a domingo de " + horario[key] + "|";
               break;
               case 'LDM':
               salida += "Horario de lunes a domingo de " + horario[key];
               break;
               case 'LDT':
               salida += " y de " + horario[key] + "|";
               break;
               case 'FS':
               salida += "Horario fin de semana de " + horario[key] + "|";
               break;
               case 'FSM':
               salida += "Horario fin de semana de " + horario[key];
               break;
               case 'FST':
               salida += " y de " + horario[key] + "|";
               break;
               case 'F':
               salida += "Horario féstivos de " + horario[key] + "|";
               break;
               case 'FM':
               salida += "Horario féstivos de " + horario[key];
               break;
               case 'FT':
               salida += " y de " + horario[key] + "|";
               break;

            }

         }
         return salida != ''? salida.substring(0,(salida.length-1)): '';
      },
      desgloasarHorarioEditar(horario){
         const regex = /(\d+)/g;
         const me = this.$options.origen == "editar" ? this.edit_ : this.item;
         const status = this.status;
         let horas = horario.match(regex);


         //lunes a vienes ininterrumpido
         if(horario.includes('lunes a viernes') && horario.length == 43){

            status.lv = true;
            me.lvm_start = horas[0] + ":" + horas[1];
            me.lvm_end = horas[2] + ":" + horas[3];
         }
         //lunes a domingo ininterrumpido
         if(horario.includes('lunes a domingo') && horario.length == 43){

            status.lv = false;
            status.ld = true;
            me.lvm_start = horas[0] + ":" + horas[1];
            me.lvm_end = horas[2] + ":" + horas[3];
         }
         //fin de semana ininterrumpido
         if(horario.includes('fin de semana') && horario.length == 38){
            status.fs = true;
            me.fsm_start = horas[0] + ":" + horas[1];
            me.fsm_end = horas[2] + ":" + horas[3];
         }
         
         //festivos ininterrumpido
         if(horario.includes('féstivos') && horario.length == 33){
            status.f = true;
            me.fm_start = horas[0] + ":" + horas[1];
            me.fm_end = horas[2] + ":" + horas[3];
         }
         //lunes a viernes mañana y tarde
         if(horario.includes('lunes a viernes') && horario.length > 43){
            status.lv = false;
            me.lvm_start = horas[0] + ":" + horas[1];
            me.lvm_end = horas[2] + ":" + horas[3];
            me.lvt_start = horas[4] + ":" + horas[5];
            me.lvt_end = horas[6] + ":" + horas[7];
         }
         
         //lunes a domingo mañana y tarde
         if(horario.includes('lunes a domingo') && horario.length > 43){
            status.fs = false;
            status.ldmt = true;
            me.lvm_start = horas[0] + ":" + horas[1];
            me.lvm_end = horas[2] + ":" + horas[3];
            me.lvt_start = horas[4] + ":" + horas[5];
            me.lvt_end = horas[6] + ":" + horas[7];
         }

         //fin de semana mañana y tarde
         if(horario.includes('fin de semana') && horario.length > 38){
            status.fs = false;
            me.fsm_start = horas[0] + ":" + horas[1];
            me.fsm_end = horas[2] + ":" + horas[3];
            me.fst_start = horas[4] + ":" + horas[5];
            me.fst_end = horas[6] + ":" + horas[7];
         }
         
         //festivo mañana y tarde
         if(horario.includes('féstivos') && horario.length > 33){
            status.f = false;
            me.fm_start = horas[0] + ":" + horas[1];
            me.fm_end = horas[2] + ":" + horas[3];
            me.ft_start = horas[4] + ":" + horas[5];
            me.ft_end = horas[6] + ":" + horas[7];
         }

         
         
      },

      disponibilidadJugador(item){
         let disponibilidad = '';
         
         for (let key in item){
            if(item[key] != null && item[key] != ''){
               disponibilidad += `${key}: ${item[key]},`;
            }
         }
         return disponibilidad != ''? disponibilidad.substring(0,(disponibilidad.length-1)): '';
      },
      desglosarDisponibilidadEditar(item){
         let diasHoras = [];
         if (item != null ){
            let dias = item.split(',');
            
            for(let key in dias){
               diasHoras[dias[key].split(':')[0]] = (dias[key].split(':')[1].replace(' ','') + ':' + dias[key].split(':')[2] );
            }
         }
         
         return diasHoras;
      },
      buscarPistas(dia,id){
        axios.get(`${this.apiUrl}/pistasDisponibles?club=${id}&fecha=${dia}`).then(response => {
            this.partidas = response.data;
         }, (error) => {
              console.log(error.response);
         })
      },

   }
}
