import stats				from './components/extras/stats.vue'
import ListPoblaciones 		from './components/Poblaciones/ListPoblaciones.vue'
import ListProvincias 		from './components/Provincias/ListProvincias.vue'
import ListInstalaciones 	from './components/Instalaciones/ListInstalaciones.vue'
import ListNiveles 			from './components/Niveles/ListNiveles.vue'
import ListJugadores 		from './components/Jugadores/ListJugadores.vue'
	import ShowUser 		from './components/Jugadores/ShowUser.vue'
	import CreateUser		from './components/Jugadores/CreateUser.vue'
import ListClubs	 		from './components/Clubs/ListClubs.vue'
	import ShowClub 		from './components/Clubs/ShowClub.vue'
	import CreateClub 		from './components/Clubs/CreateClub.vue'
import ListPartidas 		from './components/Partidas/ListPartidas.vue'
import ListRoles 			from './components/Roles/ListRoles.vue'
import ListPistas 			from './components/Pistas/ListPistas.vue'
import error404 			from './components/layouts/404.vue'
import login	 			from './components/extras/login.vue'
import galeria				from './components/home/galeria.vue'
import index				from './components/index.vue'
import home					from './components/home/home.vue'

//rutas club

//import ListJugadoresClub 	from './components/DashBoardClub/Jugadores/ListJugadores.vue'

function existToken() {
    return !!localStorage.token;
}

export const routes = [
	
	//web
	{path: '/',  component: index, name:'index'},
	{path: '/galeria' , component: galeria},

	

	//panel del club	

	/*{path: '/dashboard' , beforeEnter(to, from, next) {

				window.location = "/club/dashboard"
    		}, name: 'dashboardClub'
   	},
   	{path: '/stats',  component: stats, name: 'statsClub' },

   	{path: '/jugadores', 	 component: ListJugadoresClub, name:'ListJugadoresClub'},
		{path: '/ver/usuario/:nombre', 			component: ShowUser, 	 name: 'ShowUserClub'},
		{path: '/crear/usuario/', 				component: CreateUser, 	 name: 'CreateUserClub'},
		{path: '/partidas/usuario/:nombre', 	component: ListPartidas, name:'PartidasUserClub', props: { ver: 'jugador' }},

*/

/*
En este interceptor entramos cuando la navegación ha sido confirmada,
pero todavía no se ha creado, ni renderizado el componente. 
Nos puede venir bien usarlo para saber si el componente tiene algún 
comportamiento de navegación especial antes de pintarse.

		beforeRouteEnter (to, from, next) { }, 

Se ejecuta cuando la ruta de navegación cambia y el componente está siendo
reutilizado para la siguiente ruta o por la misma. Por ejemplo, imaginemos
que la ruta ha cambiado en la parte dinámica, en sus parámetros.

    	beforeRouteUpdate (to, from, next) { }, 

Por último, contamos con este interceptor que se ejecuta cuando antes de 
cambiar de ruta y sabemos que el componente no va a ser utilizado.

    	beforeRouteLeave (to, from, next) { } 
*/
//{path: '/prueba', alias: '/otraprueba', redirect: {name: 'home'}, component: Home}, redireccion a otra pagina
	//panel unico administrador
	{path: '/dashboard' , beforeEnter: (to, from, next) => {
			
			window.location = "/admin/dashboard";
			console.log('entra')
    		}, name: 'dashboardAdmin'
   	},
   	{path: '/stats',  component: stats, beforeEnter: (to, from, next) => {
			console.log(to.meta);
			
			//to('/galeria');
			//from('/dashboard');
			next();
    		}, beforeRouteLeave: (to, from, next) => {
    			//this.popup().then(next).catch(() => next(false));
		    },  name: 'statsAdmin' },
   
   	{path: '/localizacion', components: {
							seisA: ListPoblaciones,
							seisB: ListProvincias
						}, name : 'localizacionAdmin'
	},
	{path: '/provincias', 	 component: ListProvincias},
	{path: '/instalaciones', component: ListInstalaciones},
	{path: '/niveles', 		 component: ListNiveles},
	{path: '/jugadores', 	 component: ListJugadores, name:'ListJugadores'},
		{path: '/ver/usuario/:nombre', 			component: ShowUser, 	 name: 'ShowUser'},
		{path: '/crear/usuario/', 				component: CreateUser, 	 name: 'CreateUser'},
		{path: '/partidas/usuario/:nombre', 	component: ListPartidas, name:'PartidasUser', props: { ver: 'jugador' }},
	{path: '/clubs', 		 component: ListClubs, name:'ListClubs'},
		{path: '/ver/club/:slug', 			component: ShowClub, 	 name: 'ShowClub'},
		{path: '/crear/club/', 				component: CreateClub, 	 name: 'CreateClub'},
		{path: '/partidas/club/:slug', 		component: ListPartidas, name:'PartidasClub', props: { ver: 'club' }},
	{path: '/partidas', 	 component: ListPartidas, props: { ver: 'todas' }},
	{path: '/pistas',	 	 component: ListPistas},
	{path: '/roles', 		 component: ListRoles},
	{path: '*', 			 component: error404}

];


/*
const router = new VueRouter({
  //mode: 'history', //quita el hash de la barra de navegacion, hay que modificar el servidor para que funcione sin él
  
 routes : routesAd
});
/*switch (rol){
  case 1:
    //router.routes = routes1;
    break;
  case 2:
    //router.routes = routes2;
    break;
  case 3:
    //router.routes = routes3;
    break;
  case 4:
    //router.routes = routes4;
    break; 
}*/
