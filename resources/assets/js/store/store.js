import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'; //almacena aunque se refresque la pagina
//import Cookies from 'js-cookie'; para almacenar datos en cookies
//npm install vuex-persistedstate --save
//npm install vuex --save
import { getLocalUser} from '../helpers/auth';

const user = getLocalUser();
Vue.use(Vuex, createPersistedState);

export const store = new Vuex.Store({
	state:{
    currentUser: user,
    isLoggedIn: !!user,
    loading: false,
    auth_error: null,
    club: [],
		noticia: [],
    //user: [],
    provincias: [],
    instalaciones: [],
    niveles: [],
    clubs: [],
    //noticia: [],
   //galeria: [],
	},
	plugins: [createPersistedState()],
	mutations: {
    //autenticacion usuario
		login(state){
      state.loading = true;
      state.auth_error = null;
    },
    resErrorAuth(state){
      state.auth_error = null;
    },
    loginSuccess(state, n){
      state.auth_error = null;
      state.isLoggedIn = true;
      state.loading = false;
      state.currentUser = Object.assign({}, n.user, {token: n.access_token});

      localStorage.setItem("user", JSON.stringify(state.currentUser));
    },
    loginFailed(state, n){
      state.loading = false;
      state.auth_error = n.error;
    },
    logout(state){
      localStorage.removeItem("user");
      state.isLoggedIn = false;
      state.currentUser = null;
    },


    //combos de arrays
    provincias(state,n){
      state.provincias = n;
    },
    instalaciones(state,n){
      state.instalaciones = n;
    },
    niveles(state,n){
      state.niveles = n;
    },
    clubs(state,n){
      state.clubs = n;
    },
    noticia(state,n){
      state.noticia = n;
    },
    /*club(state, n){
      state.club = n;
    },*/
    /*user(state,n){
      state.user = n;
    },*/
    /*noticia(state,n){
      state.noticia = n;
    },*/
    /*galeria(state,n){
      state.galeria = n;
    }*/
	},
  getters:{
    isLoading(state){
      return state.loading;
    },
    isLoggedIn(state){
      return state.isLoggedIn;
    },
    currentUser(state){
      return state.currentUser;
    },
    authError(state){
      return state.auth_error;
    },
    club(state){
      return state.club;
    }
  },
  actions:{
    login(context){
      context.commit("login");
    }
  },
});

//para almacenar en cookies
/*npm install js-cookie --save
export const store = new Vuex.Store({
  state: {
    count: 0
  },
  plugins: [createPersistedState({
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    }
  })],
  mutations: {
    increment: state => state.count++,
    decrement: state => state.count--
  }
});


y en el vue

new Vue({
  el: '#app',
  computed: {
    count() {
      return store.state.count;
    }
  },
  methods: {
    increment() {
      store.commit('increment');
    },
    decrement() {
      store.commit('decrement');
    }
  }
});



*/	

/*
"devDependencies": { 
    "axios": "^0.17",
    "bootstrap": "^4.0.0-beta.2",
    "cross-env": "^5.1",
    "jquery": "^3.2",
    "laravel-mix": "^1.0",
    "lodash": "^4.17.4",
    "pe7-icon": "^1.0.4",
    "popper.js": "^1.12.6",
    "toastr": "^2.1.4",
    "vue": "^2.5.16",
    "vue-html5-editor": "^1.1.1",
    "vue-template-compiler": "^2.5.16"
  },
  "dependencies": {
    "bootstrap-vue": "^2.0.0-rc.1",
    "bootstrap4-vue": "^0.3.12",
    "ckeditor": "^4.9.2",
    "ckeditor-full": "^4.7.3",
    "font-awesome": "^4.7.0",
    "iconfonts": "^0.12.0",
    "ionicons": "^3.0.0",
    "moment": "^2.22.1",
    "slug": "^0.9.1",
    "vue-awesome": "^2.3.5",
    "vue-ckeditor2": "^2.0.1",
    "vue-d2b": "^1.0.11",
    "vue-datepicker": "^1.3.0",
    "vue-moment": "^3.2.0",
    "vue-quill-editor": "^3.0.6",
    "vue-router": "^3.0.1",
    "vue-toastr": "^2.0.12",
    "vue-ueditor": "^0.1.3",
    "vue2-editor": "^2.4.4",
    "vuex": "^3.0.1",
    "vuex-persistedstate": "^2.4.2"
  }

  */