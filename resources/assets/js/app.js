
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//url amigable
const slug = require('slug');
window.Slug = require('slug');
//importar iconos awesome
import 'vue-awesome/icons'

import BootstrapVue from 'bootstrap-vue';

//importar libreria momentjs
import moment from "moment";
window.moment = require('moment');
moment.locale('es');


// filtros
Vue.filter('capitalize', value => {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
});

Vue.filter('lowercase', value => {
  if (!value) return ''
  value = value.toString()
  return value.toLowerCase()
});

Vue.filter('nif', value => {
  const letras="abcdefghyjklmnñopqrstuvwxyz";
  let nifMay = ''
  if(!value) return ''
    for(let i = 0; i < value.length; i++){
      if (letras.indexOf(value.charAt(i),0)!=-1){
        nifMay += value.charAt(i).toUpperCase();
      }else{
        nifMay += value.charAt(i);
      }
   }
  return nifMay;
})

Vue.filter('stringSlug', value =>{
  if (!value) return ''
  return slug(value); 
});

Vue.filter('formatearFecha', value => {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY');
  }
})
Vue.filter('formatearHora', value => {
  if (value) {
    return moment(String(value)).format('HH:mm');
  }
})

Vue.filter('formatearNivel', value =>{
  const patron = /[.,]/;
  if(patron.test(value)){
    return value;
  }else{
    return value + ".00";
  };
})


Vue.use(BootstrapVue,VueRouter,axios,toastr,slug, moment); 

//importo el archivo con las rutas
import {routesClub} from './rutas/routesClub';
import {routesInvitado} from './rutas/routesInvitado';
import {routesPlayer} from './rutas/routesPlayer';
import {routesAdmin} from './rutas/routesAdmin';

//importo el archivo almacen
import {store} from './store/store';

//instanciar el componente enrutador con las rutas importadas
/*const router = new VueRouter({
  //mode: 'history', //quita el hash de la barra de navegacion, hay que modificar el servidor para que funcione sin él
  routes: routesInvitado
});*/
let router = null;
let rol = null;
try {
  rol = window.Laravel.roles;
} catch (e) {

}

switch (rol) {
    case 1:
        console.log('admin: ' + rol)
        router = new VueRouter({routes: routesAdmin});
        break;
    case 2:
        console.log('Player: ' + rol)
        router = new VueRouter({routes: routesPlayer});
        break;
    case 3:
        console.log('club: ' + rol)
        router = new VueRouter({routes: routesClub});
        break;
    default:
      console.log('invitado: ' + rol)
        router = new VueRouter({routes: routesInvitado});
        break;
};

import login            from './components/extras/login.vue';
import {initialize}     from './helpers/general';

initialize(store, router);

new Vue({
  components: {
    'login-component': login,
    
    },
  router,
  store,
  el:'#app',
  //render: h => h(App),
});

//no hace falta importat la plantilla a montar
/*
let AppLayout = require('./components/App.vue');
new Vue(
   Vue.util.extend(
     { router },
     AppLayout
   )
).$mount('#app');
*/
    
/*
Vamos a querer usar el afterEach guard de navegación. El motivo que queremos afterEach es 
confirmar que el usuario ha navegado a la página con éxito para realizar un seguimiento de la 
navegación válida de la página. No queremos que ocurra pronto y el usuario no navega como beforeEach lo haría un guard.
*/