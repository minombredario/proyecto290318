import layout					from '../components/index.vue';
import componentes			    from '../components/home/home.vue';
import registro 				from '../components/DashBoardUser/auth/registerForm.vue'
import PartidasView				from '../components/DashBoardUser/Partidas/PartidasCuad.vue'
import LigasPage				from '../components/DashBoardUser/Liga/ligaPage.vue'
import ClasesPage				from '../components/DashBoardUser/Clases/clasesPage.vue'
import GaleriasView				from '../components/DashBoardUser/Galeria/GaleriasView.vue'
import GaleriaView				from '../components/DashBoardUser/Galeria/Galeria.vue'
import ShowGaleria				from '../components/extras/galeria.vue'
import ShowPerfil				from '../components/DashBoardUser/User/ShowPerfil.vue'
import NoticiasView 			from '../components/DashBoardUser/Noticias/NoticiasView.vue'
import Noticia 					from '../components/DashBoardUser/Noticias/Noticia.vue'

export const routesInvitado = [
	//{path: '/',		component: layout, name: 'layout'}
	{path: '/', 	redirect: { name: 'layout' },		component: layout
		,children: [
			{path: '/', 					component: componentes, 	name:'layout'},
			{path:'/registro', 				component: registro, 		name:'registrarUsuario'},
			{path: '/partidas',				component: PartidasView, 	name:'partidas',   		meta:{title: 'Partidas', requiresAuth: false}, 		props: { ver: 'club' }},
			{path: '/ligas',				component: LigasPage, 		name:'ligas',			meta:{title: 'Ligas', requiresAuth: false} },
			{path: '/clases',				component: ClasesPage, 		name:'clases',			meta:{title: 'Clases', requiresAuth: false} },
			//{path: '/galeria',		component: GaleriaPage, name:'galeria',		meta:{title: 'Galeria'}},
			{path: '/galerias',				component: GaleriasView, 		name:'galerias',		meta:{title: 'Galeria', requiresAuth: false}
				,children: [
					{path: '/galeria',			component: GaleriaView, 	name:'galeria',			meta:{title: 'Galeria', requiresAuth: false}}
				]
			},
			{path: '/noticias',				component: NoticiasView, 	name:'noticias',		meta:{title: 'Noticias', requiresAuth: false} },
			{path: '/noticia/:slug', 		component: Noticia, 		name:'leerNoticia',		meta:{title: 'Noticia', requiresAuth: false} },		
			//{path: '/galeria/:galeria',		component: ShowGaleria, 	name:'galeria', 		meta:{title: 'Galeria', requiresAuth: false} },
			{path: '/perfil/:nick',			component: ShowPerfil,  	name:'perfil',  		meta:{title: 'Perfil',  requiresAuth: true} },	
			
		]
	},
]