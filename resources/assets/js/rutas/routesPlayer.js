import galeria				from '../components/home/galeria.vue'
import index				from '../components/index.vue'
import home					from '../components/home/home.vue'


export const routesPlayer = [

	//web
	{path: '/',  component: index, name:'index'},
	{path: '/galeria' , component: galeria},

]