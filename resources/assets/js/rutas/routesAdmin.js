import layout				from '../components/AdminDashBoard.vue'

import statsAdmin			from '../components/extras/stats.vue'
import ListPoblaciones 		from '../components/Poblaciones/ListPoblaciones.vue'
import ListProvincias 		from '../components/Provincias/ListProvincias.vue'
import ListInstalaciones 	from '../components/Instalaciones/ListInstalaciones.vue'
import ListNiveles 			from '../components/Niveles/ListNiveles.vue'
import ListJugadores 		from '../components/Jugadores/ListJugadores.vue'
	import ShowUser 		from '../components/Jugadores/ShowUser.vue'
	import CreateUser		from '../components/Jugadores/CreateUser.vue'
import ListClubs	 		from '../components/Clubs/ListClubs.vue'
	import ShowClub 		from '../components/Clubs/ShowClub.vue'
	import CreateClub 		from '../components/Clubs/CreateClub.vue'
import ListPartidas 		from '../components/Partidas/ListPartidas.vue'
import ListRoles 			from '../components/Roles/ListRoles.vue'
import ListPistas 			from '../components/Pistas/ListPistas.vue'
import error404 			from '../components/layouts/404.vue'
import login	 			from '../components/extras/login.vue'


function existToken() {
    return !!localStorage.token;
}

export const routesAdmin = [
   	{path: '/', 	redirect: { name: 'layout' },		component: layout 
		,children: [
			{path: '/', 	component: statsAdmin, name:'layout', meta:{title: 'Stats'}},
	   
			   	{path: '/localizacion', components: {
										seisA: ListPoblaciones,
										seisB: ListProvincias
									}, name : 'localizacionAdmin', meta:{title: 'Localización'}
				},
				{path: '/provincias', 	 component: ListProvincias,  meta:{title: 'Provincias'}},
				{path: '/instalaciones', component: ListInstalaciones,  meta:{title: 'Instalaciones'}},
				{path: '/niveles', 		 component: ListNiveles, meta:{title: 'Niveles'}},
				{path: '/jugadores', 	 component: ListJugadores, name:'ListJugadores',  meta:{title: 'Jugadores'}},
					{path: '/ver/usuario/:nombre', 			component: ShowUser, 	 name: 'ShowUser',  meta:{title: 'Jugador'}},
					{path: '/crear/usuario/', 				component: CreateUser, 	 name: 'CreateUser', meta:{title: 'Jugador'}},
					{path: '/partidas/usuario/:nombre', 	component: ListPartidas, name:'PartidasUser', props: { ver: 'jugador' },  meta:{title: 'Partidas'}},
				{path: '/clubs', 		 component: ListClubs, name:'ListClubs',  meta:{title: 'Clubs'}},
					{path: '/ver/club/:slug', 			component: ShowClub, 	 name: 'ShowClub',  meta:{title: 'Club'}},
					{path: '/crear/club/', 				component: CreateClub, 	 name: 'CreateClub',  meta:{title: 'Club'}},
					{path: '/partidas/club/:slug', 		component: ListPartidas, name:'PartidasClub', props: { ver: 'club' },  meta:{title: 'Partidas'}},
				{path: '/partidas', 	 component: ListPartidas, props: { ver: 'todas' },  meta:{title: 'Partidas'}},
				{path: '/pistas',	 	 component: ListPistas,  meta:{title: 'Pistas'}},
				{path: '/roles', 		 component: ListRoles,  meta:{title: 'Roles'}},
				{path: '*', 			 component: error404}
			]
	},
]



/*
En este interceptor entramos cuando la navegación ha sido confirmada,
pero todavía no se ha creado, ni renderizado el componente. 
Nos puede venir bien usarlo para saber si el componente tiene algún 
comportamiento de navegación especial antes de pintarse.

		beforeRouteEnter (to, from, next) { }, 

Se ejecuta cuando la ruta de navegación cambia y el componente está siendo
reutilizado para la siguiente ruta o por la misma. Por ejemplo, imaginemos
que la ruta ha cambiado en la parte dinámica, en sus parámetros.

    	beforeRouteUpdate (to, from, next) { }, 

Por último, contamos con este interceptor que se ejecuta cuando antes de 
cambiar de ruta y sabemos que el componente no va a ser utilizado.

    	beforeRouteLeave (to, from, next) { } 
*/
//{path: '/prueba', alias: '/otraprueba', redirect: {name: 'home'}, component: Home}, redireccion a otra pagina
	//panel unico administrador
	/*{path: '/dashboard' , beforeEnter: (to, from, next) => {
			
			window.location = "/admin/dashboard";
    		}, name: 'dashboardAdmin'
   	},*/