$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('clubs',{
   
    //props: ['buscar'],
   
    template : '<select class="form-control" v-on:change="datosClub($event.target.value)" name="clubs">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in clubs" v-bind:value="item.id" class="text-capitalize">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
        axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
    },
    
    data() {
        return {
            clubs: null,
           
        }
    },
    methods: {
        datosClub(value) {
            var id = value;
            axios.get('/Npistas?id='+value).then((response) => {
                this.$emit('busqueda', response.data.items);  
            })
            
          
        },   
    },
    
});

Vue.component('hora',{
     props: ['eleccion'],
    
    template:   '<select class="form-control" name="eleccion" v-on:change="elegirHora($event.target.value)">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in hora" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
    mounted() {    
        axios.get('/horas').then((response) => {
                this.hora = response.data;
               
            })
        
    },
    
    data() {
        return {
            hora: null,
            
        }
    },
    
    methods: {
        elegirHora(value) {
            var id = value;
            this.$emit('horario', id);  
        },   
    },
    
    
});

Vue.component('nivel',{
    props: ['eleccion'],
    
    template:   '<select class="form-control" id="csProv" v-model="eleccion.nivel">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in niveles" v-bind:value="item.nombre">{{item.nombre}}</option>'+
                '</select>',
              
    mounted() {
        axios.get('/listNiv').then((response) => {
            this.niveles = response.data;
        })
    },
    
    data() {
        return {
            niveles: null,
            
        }
    },
    
});

new Vue({
    
    el: '#pistas',
    
    data :{
        pageInfo: {
            pageUrl: '/pistas',
        },
        horarios: [],
        alquilerPista:{
            hora_inicio: '',
            hora_fin: '',
            pista: '',
            nombre: '',
            nivel: '',
        },
        dias: [],
        semana : false,
        //elemento: '',
        //operador: '',
        contDia : 0,
        date : new Date(),
        cambioDia :  new Date(),
        meses : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        nombreDiasSemana : ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        anado : [2,1,7,6,5,4,3],
        arraySemana: [],//cargar todos los dias de la semana
        fecha: {
            dia: '',
            numeroDiaSemana: '',
            diaSemana: '',
            numeroMes: '',
            mes: '',
            anyo: '',
            diaLargo: '',
            primerDiaSemana : '',
            ultimoDiaSemana : '',
        },
        pistasOcupadas: [],
        nombreClub: '',
        pistas: '',
        mensaje : '',
        clases : '',
        formErrors:{},
        formErrorsUpdate:{},
       
    },
    computed: {
        now: function () {
            this.fecha.dia = this.date.getDate();
            this.fecha.numeroDiaSemana = this.date.getDay(),
            this.fecha.diaSemana = this.nombreDiasSemana[this.date.getDay()];
            this.fecha.numeroMes = this.date.getMonth();
            this.fecha.mes = this.meses[this.date.getMonth()];
            this.fecha.anyo = this.date.getFullYear();  
            hoy = this.nombreDiasSemana[this.cambioDia.getDay()] + ", " + this.cambioDia.getDate() + " de " + this.meses[this.cambioDia.getMonth()] +" de " + this.cambioDia.getFullYear();
            return hoy;
        }
    },
    
    //arranque
    mounted: function(){
        this.getHorario();
        this.getDias();
    },
        
    //metodos
    methods: {
        
        getHorario: function(){
            axios.get('/horas').then((response) => {
                this.horarios = response.data;
                
            })
        },
        
        getPistas: function(busqueda){
            console.log(busqueda);
            this.pistas = busqueda[0].pistas;
            this.nombreClub = busqueda[0].nombre;
            this.pistasOcupadas = busqueda;
        },
        
        getClubs: function(){
            axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
        },
        
        getDias: function(){
            axios.get('/dias').then((response) => {
                this.dias = response.data;
                //console.log(this.dias);
            })
        },
        
        previous: function(event){
            this.cambiarCalendario(event.target.value,event.target.id);
        },
        next: function(event){
            this.cambiarCalendario(event.target.value,event.target.id);
        },
        hoy: function(event){
            this.contDia = 0;
            this.cambiarCalendario(event.target.value,event.target.id);
        },
        
        cambiarCalendario: function(el,op){
            
            if(el == "agenda" && op == "ant"){
                this.contDia -= 1;
                this.cambiarDia();
            }
            if(el == "agenda" && op == "post"){
                this.contDia += 1;
                this.cambiarDia();
            }
            if(el == "agenda" && op == "hoy"){this.cambiarDia();}
            if(el == "week" && op == "ant"){
                this.contDia -= 7;
                this.calcularSemana();
            }
            if(el == "week" && op == "post"){
                this.contDia += 7;
                this.calcularSemana();
            }

            if(el == "week" && op == "hoy"){this.calcularSemana();}
        },
        
        agenda: function(event){
            $("#ant").val('agenda');
            $("#post").val('agenda');
            $("#hoy").val('agenda');
            this.semana = false;
            //this.calendarioAgenda();
        },
        
        week: function(event){
            $("#ant").val('week');
            $("#post").val('week');
            $("#hoy").val('week');
            this.semana = true;
            this.calcularSemana();
        },
        
        calcularSemana : function(){
            this.arraySemana = new Array();
            // obtenemos el primer dia del año
            var primerdia = new Date(this.fecha.anyo, 0, 1);
            
            //obtenemos el primer dia de la primera semana del año
            fecha=new Date(this.fecha.anyo,0,1);
            primerDiaDelAno=this.anado[fecha.getDay()];
            
            //calculamos el tiempo que ha pasado desde el primer dia del año al dia actual
            fecha= new Date(this.fecha.anyo,0,primerDiaDelAno);
            fecha2= new Date(this.fecha.anyo,this.fecha.numeroMes,((this.fecha.dia+this.contDia)+primerDiaDelAno));
            tiempopasado=fecha2-fecha;
            semanas=Math.floor(tiempopasado/1000/60/60/24/7);
            if(semanas==0){ semanas=52};
	
            // obtenemos la corrección necesaria
            var correccion = 6 - primerdia.getDay();
            
            // obtenemos los dias de la semana
            for(var i = 3; i <= 9; i++){
                this.arraySemana.push(new Date(this.fecha.anyo, 0, (semanas - 1) * 7 + i + correccion));
            };
            //this.fecha.diaLargo = this.nombreDiasSemana[this.fecha.numeroDiaSemana] + ", " + this.fecha.dia + " de " + this.meses[this.fecha.numeroMes] +" de " + this.fecha.anyo;
            this.fecha.primerDiaSemana = this.arraySemana[0].getDate() + " de " + this.meses[this.arraySemana[0].getMonth()];
            this.fecha.ultimoDiaSemana = this.arraySemana[6].getDate() + " de " + this.meses[this.arraySemana[6].getMonth()]; 
            //this.calendarioSemana();            
        },
        
        calendarioSemana: function(){
            axios.get('/semana').then((response) => {
                $("#calendario").html(response.data);
                console.log(response.data);
            })
           
        },
        
        cambiarDia: function(){
            this.cambioDia =  new Date(this.fecha.anyo, this.fecha.numeroMes, (this.date.getDate()+this.contDia));
            //this.diaL = this.nombreDiasSemana[this.cambioDia.getDay()] + ", " + this.cambioDia.getDate() + " de " + this.meses[this.cambioDia.getMonth()] +" de " + this.cambioDia.getFullYear();
            this.calendarioAgenda();	
        },
        
        calendarioAgenda: function(){
            
        },

        prueba: function (busqueda){
              console.log(busqueda);
                this.pistas = busqueda.id;
                this.nombreClub = busqueda.nombre;
        },
        
        create: function(pista){
            console.log(pista);
            $("#create-item").modal('show');
           /*  axios.post(this.pageInfo.pageUrl,input).then((response) => {
                    $("#contenidoPrincipal").html(response.response);
                    //this.nuevo = {'nombre' : ''};
                    $("#create-item").modal('hide');
                    //this.mostrar = true;
                    //this.clases = "alert alert-success";
                    //this.mensaje ="Registro creado con exito.";
                    //this.getItems();

                }, (error) => {
                    $("#contenidoPrincipal").html(error.response.data);
                    //this.formErrors = error.data;
                    //this.mostrar = true;
                    //this.clases = "alert alert-danger";
                    //this.mensaje ="No se ha creado el registro.";

               });*/
        },
        
        alquilerPistaHoras: function(hora,pista){
            this.alquilerPista.hora_inicio = hora.nombre;
            var h = '';
            var m = '';
                if((parseInt(hora.nombre.substring(3,5))+30) == 60){
                    if((parseInt(hora.nombre.substring(0,2))+2)<10){
                        h  = "0" + (parseInt(hora.nombre.substring(0,2))+2);
                    }else{
                        h  = parseInt(hora.nombre.substring(0,2))+2;
                    }
                        m = "00";
                }else if((parseInt(hora.nombre.substring(3,5))+30) == 30){
                    if((parseInt(hora.nombre.substring(0,2))+1)<10){
                        h  = "0" + (parseInt(hora.nombre.substring(0,2))+1);
                    }else{
                        h  = parseInt(hora.nombre.substring(0,2))+1;
                    }
                        m = "30";
                }
            console.log("hora: " + h + " minuto: " + m);
            this.alquilerPista.hora_fin = h + ":" + m + ":00";
           
            this.alquilerPista.pista = pista;
            
        },
    }
});





