$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
Vue.config.debug = true;

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        var diasSemana = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
        var anado = [2,1,7,6,5,4,3];
        var date = new Date();
        var horarios = [];
Vue.component('clubs',{
   
    //props: ['buscar'],
   
    template : '<select class="form-control" v-on:change="datosClub($event.target.value)" name="clubs">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in clubs" v-bind:value="item.id" class="text-capitalize">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
        axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
    },
    
    data() {
        return {
            clubs: null,
           
        }
    },
    methods: {
        datosClub(value) {
            var id = value;
            axios.get('/Npistas?id='+value).then((response) => {
                this.$emit('busqueda', response.data[0] );  
            })
            
          
        },   
    },
    
});

Vue.component('hora',{
     props: ['eleccion'],
    
    template:   '<select class="form-control" name="eleccion" v-on:change="elegirHora($event.target.value)">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in hora" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
    mounted() {    
        axios.get('/horas').then((response) => {
                this.hora = response.data;
               
            })
        
    },
    
    data() {
        return {
            hora: null,
            
        }
    },
    
    methods: {
        elegirHora(value) {
            var id = value;
            this.$emit('horario', id);  
        },   
    },
    
    
});

Vue.component('nivel',{
    props: ['eleccion'],
    
    template:   '<select class="form-control" id="csProv" v-model="eleccion.nivel">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in niveles" v-bind:value="item.nombre">{{item.nombre}}</option>'+
                '</select>',
              
    
    mounted() {
        axios.get('/listNiv').then((response) => {
            this.niveles = response.data;
        })
    },
    
    data() {
        return {
            niveles: null,
            
        }
    },
    
});

new Vue({
    
    el: '#pistas',
    
    data :{
        pageInfo: {
            pageUrl: '/pistas',
        },
        horarios: [],
        alquilerPista:{
            hora_inicio: '',
            hora_fin: '',
            pista: '',
            nombre: '',
            nivel: '',
        },
        dias: [],
        fecha: {
            dia: '',
            NumeroDiaSemana: '',
            diaSemana: '',
            NumeroMes: '',
            mes: '',
            anyo: '',
            primerDiaSemana: '',
            ultimoDiaSemana: '',
        },
        pistasOcupadas: '',
        nombreClub: '',
        pistas: '',
        mensaje : '',
        clases : '',
        formErrors:{},
        formErrorsUpdate:{},
       
            
    },
    
    //arranque
    mounted: function(){
        this.getHorario();       
        //this.getPistas();
        this.getCalendario();
        this.getDias();
        //this.getClubs();
        
    },
        
    //metodos
    methods: {
        getHorario: function(){
            axios.get('/horas').then((response) => {
                this.horarios = response.data;
            })
        },
        
        getPistas: function(busqueda){
            this.pistas = busqueda.pistas;
            this.nombreClub = busqueda.nombre;
        },
        
        getClubs: function(){
            axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
        },
        
        getDias: function(){
            axios.get('/dias').then((response) => {
                this.dias = response.data;
                console.log(this.dias);
            })
        },
        
        getCalendario: function(){
            
            this.fecha.dia = date.getDate();
            this.fecha.NumeroDiaSemana = date.getDay(),
            this.fecha.diaSemana = diasSemana[date.getDay()];
            this.fecha.NumeroMes = date.getMonth();
            this.fecha.mes = meses[date.getMonth()];
            this.fecha.anyo = date.getFullYear();
            
            // obtenemos el primer dia del año
            var primerdia = new Date(this.fecha.anyo, 0, 1);
            
            //obtenemos el primer dia de la primera semana del año
            fecha = new Date(this.fecha.anyo,0,1);
            primerDiaDelAno = anado[fecha.getDay()];
            
            //calculamos el tiempo que ha pasado desde el primer dia del año al dia actual
            fecha = new Date(this.fecha.anyo,0,primerDiaDelAno);
            fecha2 = new Date(this.fecha.anyo,this.fecha.NumeroMes,(parseInt(this.fecha.dia)+primerDiaDelAno));
            tiempopasado = fecha2-fecha;
            
            semanas = Math.floor(tiempopasado/1000/60/60/24/7);
            
            if(semanas == 0){
                semanas = 52
            }

            // obtenemos la corrección necesaria
            var correccion = 6 - primerdia.getDay();
            
            // obtenemos el lunes y domingo de la semana especificada
            var primer = new Date(this.fecha.anyo, 0, (semanas - 1) * 7 + 3 + correccion);
            var ultimo = new Date(this.fecha.anyo, 0, (semanas - 1) * 7 + 9 + correccion);
            this.fecha.primerDiaSemana = primer.getDate();
            this.fecha.ultimoDiaSemana =  ultimo.getDate();
         	
        },
        
        fechaAnterior: function (){
            var primerDia = new Date(this.fecha.anyo, this.fecha.NumeroMes, 1);
            var ultimoDia = new Date(this.fecha.anyo, this.fecha.NumeroMes, 0);
            if(this.fecha.dia >= primerDia.getDate()){
                this.fecha.dia -= 1; //resto uno al dia del mes
                
                //tratamiento de la semana
                this.fecha.NumeroDiaSemana -=1; // resto uno al dia de la semana
                if(this.fecha.NumeroDiaSemana >= 0 && this.fecha.NumeroDiaSemana <= 6){ //si el valor esta entre 0 y 6
                    this.fecha.diaSemana = diasSemana[this.fecha.NumeroDiaSemana]; //el dia de la semana es el valor que toca
                }else if(this.fecha.NumeroDiaSemana < 0){ // si es menor a 0, restablezco los valores.
                    this.fecha.diaSemana = diasSemana[6];
                    this.fecha.NumeroDiaSemana = 6;
                }
                
                if(this.fecha.dia < primerDia.getDate()){
                    this.fecha.dia = ultimoDia.getDate();
                    if(this.fecha.NumeroMes >=0){
                        if(this.fecha.NumeroMes == 0){
                            this.fecha.NumeroMes = 11;
                            this.fecha.mes = meses[this.fecha.NumeroMes];
                            this.fecha.anyo -=1;
                            
                        }else{
                            this.fecha.NumeroMes -= 1;
                            this.fecha.mes = meses[this.fecha.NumeroMes];
                        }
                    }
                }
            }
            
           
        },
        
        fechaPosterior: function(dia) {
            var primerDia = new Date(this.fecha.anyo, this.fecha.NumeroMes, 1);
            var ultimoDia = new Date(this.fecha.anyo, this.fecha.NumeroMes + 1, 0);
            
            if(this.fecha.dia <= ultimoDia.getDate()){
                this.fecha.dia += 1; //resto uno al dia del mes
                
                //tratamiento de la semana
                this.fecha.NumeroDiaSemana +=1; // resto uno al dia de la semana
                if(this.fecha.NumeroDiaSemana >= 0 && this.fecha.NumeroDiaSemana <= 6){ //si el valor esta entre 0 y 6
                    this.fecha.diaSemana = diasSemana[this.fecha.NumeroDiaSemana]; //el dia de la semana es el valor que toca
                }else if(this.fecha.NumeroDiaSemana > 6){ // si es menor a 0, restablezco los valores.
                    this.fecha.diaSemana = diasSemana[0];
                    this.fecha.NumeroDiaSemana = 0;
                }
                
                if(this.fecha.dia > ultimoDia.getDate()){
                    
                    this.fecha.dia = primerDia.getDate();
                    if(this.fecha.NumeroMes <=11){
                        if(this.fecha.NumeroMes == 11){
                            this.fecha.NumeroMes = 0;
                            this.fecha.mes = meses[this.fecha.NumeroMes];
                            this.fecha.anyo +=1;
                            
                        }else{
                            this.fecha.NumeroMes += 1;
                            this.fecha.mes = meses[this.fecha.NumeroMes];

                        }
                    }
                }
            }
           
        },
        
        prueba: function (busqueda){
          console.log(busqueda);
            this.pistas = busqueda.id;
            this.nombreClub = busqueda.nombre;
        },
        
        create: function(pista){
            console.log(pista);
            $("#create-item").modal('show');
           /*  axios.post(this.pageInfo.pageUrl,input).then((response) => {
                    $("#contenidoPrincipal").html(response.response);
                    //this.nuevo = {'nombre' : ''};
                    $("#create-item").modal('hide');
                    //this.mostrar = true;
                    //this.clases = "alert alert-success";
                    //this.mensaje ="Registro creado con exito.";
                    //this.getItems();

                }, (error) => {
                    $("#contenidoPrincipal").html(error.response.data);
                    //this.formErrors = error.data;
                    //this.mostrar = true;
                    //this.clases = "alert alert-danger";
                    //this.mensaje ="No se ha creado el registro.";

               });*/
        },
        
        alquilerPistaHoras: function(hora,pista){
            this.alquilerPista.hora_inicio = hora.nombre;
            var h = '';
            var m = '';
                    if((parseInt(hora.nombre.substring(3,5))+30) == 60){
                        if((parseInt(hora.nombre.substring(0,2))+2)<10){
                            h  = "0" + (parseInt(hora.nombre.substring(0,2))+2);
                        }else{
                            h  = parseInt(hora.nombre.substring(0,2))+2;
                        }
                        
                        m = "00";
                    }else if((parseInt(hora.nombre.substring(3,5))+30) == 30){
                        if((parseInt(hora.nombre.substring(0,2))+1)<10){
                            h  = "0" + (parseInt(hora.nombre.substring(0,2))+1);
                        }else{
                            h  = parseInt(hora.nombre.substring(0,2))+1;
                           
                        }
                         m = "30";
                    }
            console.log("hora: " + h + " minuto: " + m);
            this.alquilerPista.hora_fin = h + ":" + m + ":00";
           
            this.alquilerPista.pista = pista;
            
        },
        
        agenda: function(){
            console.log('agenda');
        },
        mes: function(){
            
           // $("#calendario").html("<p>"+@include('pistas.partials.semana')+"</p>");
        },
        hoy: function(){
            console.log('hoy');
        },
        
        diaDeLaSemana: function(dia){
            diasSemana[dia.getDay()];
            console.log(diasSemana[dia.getDay()]);
        },
    }
});





