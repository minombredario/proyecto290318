$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('clubs',{
   
    //props: ['buscar'],
   
    template : '<select class="form-control" v-on:change="datosClub($event.target.value)" name="clubs">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in clubs" v-bind:value="item.id" class="text-capitalize">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
        axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
    },
    
    data() {
        return {
            clubs: null,
           
        }
    },
    methods: {
        datosClub(value) {
            var id = value;
            axios.get('/Npistas?id='+value).then((response) => {
                this.$emit('busqueda', response.data.items);  
            })
            
          
        },   
    },
    
});

Vue.component('hora',{
     props: ['eleccion'],
    
    template:   '<select class="form-control" name="eleccion" v-on:change="elegirHora($event.target.value)">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in hora" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
    mounted() {    
        axios.get('/horas').then((response) => {
                this.hora = response.data;
               
            })
        
    },
    
    data() {
        return {
            hora: null,
            
        }
    },
    
    methods: {
        elegirHora(value) {
            var id = value;
            this.$emit('horario', id);  
        },   
    },
    
    
});

Vue.component('nivel',{
    props: ['eleccion'],
    
    template:   '<select class="form-control" id="csProv" v-model="eleccion.nivel">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in niveles" v-bind:value="item.nombre">{{item.nombre}}</option>'+
                '</select>',
              
    mounted() {
        axios.get('/listNiv').then((response) => {
            this.niveles = response.data;
        })
    },
    
    data() {
        return {
            niveles: null,
            
        }
    },
    
});


var dataFecha = '';

new Vue({
    el: "#pistas",
    data: {
        mensage: 'prueba',
        alquilerPista: {'id': '', 'pista': '', 'inicio': '', 'fin': '', 'jugadores': []},
        horas: [],
        arraySemana: [],
        hoy: '',
        primerDiaSemana: '',
        ultimoDiaSemana: '',
        fech: '',
        cabecera: '',
        dataFecha: '',
        vista: 'agenda',
        diasSemana: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        mesesDelAno: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        anado: [2, 1, 7, 6, 5, 4, 3],
        date: new Date(),
        cambioDia: new Date(),
        contDia: 0,
        ocupadas: [],
        nombreClub: '',
        pistas: '',
        mensaje : '',
        clases : '',
        formErrors:{},
        formErrorsUpdate:{},
    },
    
    mounted: function () {

        this.horario();
        this.saberSemana();
        this.rellenarPistas();
    },
    
    computed: function () {

    },
    
    watch: {//se queda a la espera de cambios
        arraySemana: function () {
            if (this.vista == 'agenda') {
                this.calendarioAgenda();
                return;
            }
            this.calendarioSemana();
        },
    
        cambioDia: function () {
            this.rellenarPistas();
            this.saberSemana();
            this.calendarioAgenda();
        },

        dataFecha: function () {

        },
    },
    
    methods: {
        
        getPistas: function(busqueda){
            //console.log(busqueda);
            this.cabecera = busqueda[0].pistas;
            this.nombreClub = busqueda[0].nombre;
            this.pistasOcupadas = busqueda;
        },
        
        calendarioAgenda: function () {
            this.cabecera = 4;
            this.dataFecha = this.corregirHoras(this.cambioDia.getDate()) + "-" + this.corregirHoras(this.cambioDia.getMonth() + 1) + "-" + this.cambioDia.getFullYear();
            dataFecha = this.corregirHoras(this.cambioDia.getDate()) + "-" + this.corregirHoras(this.cambioDia.getMonth() + 1) + "-" + this.cambioDia.getFullYear();
            this.fechaCorta();
        },

        calendarioSemana: function () {
            this.cabecera = this.arraySemana;
            this.fech = this.diasSemana;
        },

        previous: function () {

            if (this.vista == 'agenda') {
                this.contDia -= 1;
                this.cambiarDia();
            } else if (this.vista == 'week') {
                this.contDia -= 7;
                this.saberSemana();
            }
        },

        next: function () {

            if (this.vista == 'agenda') {
                this.contDia += 1;
                this.cambiarDia();
            } else if (this.vista == 'week') {
                    this.contDia += 7;
                    this.saberSemana();
                }
            },
            diaActual: function () {
                if (this.vista == 'agenda') {
                    this.contDia = 0;
                    this.cambiarDia();
                } else if (this.vista == 'week') {
                    this.contDia = 0;
                    this.saberSemana();
                }
            },
            
            agenda: function () {
                this.calendarioAgenda();
                this.vista = 'agenda';
            },
            
            week: function () {
                this.calendarioSemana();
                this.vista = "week";
            },
            
            cambiarDia: function () {
                this.cambioDia = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate() + this.contDia);
                this.hoy = "Hoy es " + this.diasSemana[this.date.getDay()] + ", " + this.date.getDate() + " de " + this.mesesDelAno[this.date.getMonth()] + " de " + this.date.getFullYear();
                if (this.vista == 'agenda') {
                    this.calendarioAgenda();
                    return;

                }
                //this.calendarioAgenda();	
            },
            
            ver: function (e) {
                this.alquilerPista = {'inicio': $(e.target).data('hora'), 'fin': this.calcularTiempoFinal($(e.target).data('hora')), 'pista': $(e.target).data('pista')};

                //console.log(this.alquilerPista);

            },
            
            crear: function () {
                var input = new FormData($('#upload')[0]);
                for (var pair of input.entries()) {
                    console.log(pair);
                }

            },
            
            calcularTiempoFinal: function (inicio) {
                inicio = inicio + ":00"; //solo si pasamos la hora formato 00:00
                var start = inicio.split(":"),
                        t1 = new Date(),
                        salida = t1.setHours(start[0], parseInt(start[1]) + 90, start[2]);

                salida = this.corregirHoras(t1.getHours()) + ":" + this.corregirHoras(t1.getMinutes());
                return 	salida;

            },
            
            corregirHoras: function (hora) {
                return (hora < 10 ? '0' : '') + hora;
            },
            
            pistasOcupadas: function () {

                /*$.get('get_eventos.php', function(response){
                 this.ocupadas = response;
                 console.log(this.ocupadas);
                 
                 });*/



            },
            
            rellenarPistas: function () {
                
            },
                        alquilerPistaHoras: function(hora, pista){
                        this.alquilerPista.hora_inicio = hora.nombre;
                                var h = '';
                                var m = '';
                                if ((parseInt(hora.nombre.substring(3, 5)) + 30) == 60){
                        if ((parseInt(hora.nombre.substring(0, 2)) + 2) < 10){
                        h = "0" + (parseInt(hora.nombre.substring(0, 2)) + 2);
                        } else{
                        h = parseInt(hora.nombre.substring(0, 2)) + 2;
                        }
                        m = "00";
                        } else if ((parseInt(hora.nombre.substring(3, 5)) + 30) == 30){
                        if ((parseInt(hora.nombre.substring(0, 2)) + 1) < 10){
                        h = "0" + (parseInt(hora.nombre.substring(0, 2)) + 1);
                        } else{
                        h = parseInt(hora.nombre.substring(0, 2)) + 1;
                        }
                        m = "30";
                        }
                        console.log("hora: " + h + " minuto: " + m);
                                this.alquilerPista.hora_fin = h + ":" + m + ":00";
                                this.alquilerPista.pista = pista;
                        },
            crearTablero: function () {
                $('#calendario .droppable').droppable({
                    accept: '[id^="dragThis"]',
                    greedy: true,
                    tolerance: "intersect",
                    drop: function (event, ui) {

                        var draggable = ui.draggable;
                        var $this = $(this);
                        draggable.css({"width": $this.width(), "height": $this.outerHeight() * 4});
                        var hora = $this.data('hora');
                        var pista = $this.data('pista');
                        ui.helper.find('.evento span').html(pista);
                        ui.helper.find('.start span').html(corregirTiempoInicio(hora));
                        ui.helper.find('.fin span').html(correccionTiempoFinal(hora));

                    },
                    activate: function (event, ui) { //ocurre cuando el draggable se activa (comienza a ser arrastrado)


                    },
                    out: function (event, ui) {


               },
                    over: function (event, ui) {


                    },
                    revert: function (event, ui) {

                    },
                });

            },
            horario: function () {
                for (var i = 0; i < 24; i++) {
                    if (i < 10) {
                        this.horas.push("0" + i + ":00");
                        this.horas.push("0" + i + ":30");
                    } else {
                        this.horas.push(i + ":00");
                        this.horas.push(i + ":30");
                    }
                }

            },
            saberSemana: function () {

                this.arraySemana = new Array();

                // obtenemos el primer dia del año
                var primerdia = new Date(this.date.getFullYear(), 0, 1);

                //obtenemos el primer dia de la primera semana del año
                fecha = new Date(this.date.getFullYear(), 0, 1);
                primerDiaDelAno = this.anado[fecha.getDay()];

                //calculamos el tiempo que ha pasado desde el primer dia del año al dia actual
                fecha = new Date(this.date.getFullYear(), 0, primerDiaDelAno);

                fecha2 = new Date(this.date.getFullYear(), this.date.getMonth(), ((this.date.getDate() + this.contDia) + primerDiaDelAno));

                tiempopasado = fecha2 - fecha;
                semanas = Math.floor(tiempopasado / 1000 / 60 / 60 / 24 / 7);

                if (semanas == 0) {
                    semanas = 52
                }

                // obtenemos la corrección necesaria
                var correccion = 6 - primerdia.getDay();

                // obtenemos los dias de la semana
                for (var i = 3; i <= 9; i++) {
                    this.arraySemana.push(new Date(this.date.getFullYear(), 0, (semanas - 1) * 7 + i + correccion));
                }
                ;

                // mostramos el resultado
                this.hoy = "Hoy es " + this.diasSemana[this.date.getDay()] + ", " + this.date.getDate() + " de " + this.mesesDelAno[this.date.getMonth()] + " de " + this.date.getFullYear();
                this.primerDiaSemana = "El primer día de la semana es " + this.arraySemana[0].getDate() + " de " + this.mesesDelAno[this.arraySemana[0].getMonth()] + " (" + this.diasSemana[this.arraySemana[0].getDay()] + ")";
                this.ultimoDiaSemana = "El último día de la semana es " + this.arraySemana[6].getDate() + " de " + this.mesesDelAno[this.arraySemana[6].getMonth()] + " (" + this.diasSemana[this.arraySemana[6].getDay()] + ")";


                if (this.vista == "agenda") {
                    this.calendarioAgenda();
                } else if (this.vista == "week") {
                    this.calendarioSemana();
                }

                //setTimeout(this.rellenarPistas(), 10000);
            },
            fechaCorta: function () {
                this.fech = this.cambioDia.getDate() + "/" + (this.cambioDia.getMonth() + 1);

         },
        },
});


