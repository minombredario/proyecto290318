$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('clubs',{
    
    template : '<select class="form-control" v-model="this.club" @change="datosclub">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in clubs" v-bind:value="item">{{item.nombre}}</option>'+
                '</select>',
    
    mounted() {
        axios.get('listado_clubs').then((response) => {
                    this.clubs = response.data;
                })
    }, 
    
    data() {
        return {
            clubs: null,
            club: {'id': '', 'nombre': '', 'pistas': ''},
        }
    },
    
    methods: {
        datosclub() {
            this.club = club;
            this.$emit('datosclub', this.club);
           
        },    
    },
       
});


var busqueda = '';
Vue.component('calendar', {
    template: '<div></div>',
    props: {
        eventos: {
            type: Array,
            required: true
        },
       
        club: {
            type: Object,
            required: true,
            default: 1
        },
        editable: {
            type: Boolean,
            required: true,
            default: true
        },
        eventoverlap: {
            type: Boolean,
            required: true,
            default: false
        },
        droppable: {
            type: Boolean,
            required: true,
            default: true
        },
    },
    data: function(){
        return {
            cal: null,
            datosCabecera: [],         
            args :{
                schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                resourceAreaWidth: 230,
                aspectRatio: 1.8,
                scrollTime: '07:00', //hora inicio vista
                Duration: '01:30:00',
                lazyFetching: true,
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,resourceDay'
                },
                /*views: {
                    resourceDay: {
                        type: 'agenda',
                        duration: { days: 1 },
                        buttonText: 'agenda'
                    }
                },*/    
                RefetchResourcesOnNavigate: true,                
                defaultView: 'agendaDay',
                allDaySlot: false, //mostrar la fila todo el dia
                defaultDate: new Date(),
                locale: 'es',
                timeFormat: 'HH:mm', //vista de la hora en el evento
                eventOverlap: this.eventoverlap, //solapar eventos
                editable: this.editable, //editar eventos
                droppable: this.droppable, //arrastrar y soltar
                businessHours: {
                    // dias de la semana en array empezando por el 0 domingo
                    dow: [ 0, 1, 2, 3, 4, 5, 6 ], // seleccionamos los dias que van a tener el horario debajo indicado **opcional
                    start: '07:00',
                    end: '24:00',
                },
                resources: { // you can also specify a plain string like 'json/resources.json'
                    url: 'calendar/resources?pistas=1',
                    //cache: true,
                    error: function() {
                        $('#script-warning').show();
                    }
                },
                events: { // you can also specify a plain string like 'json/resources.json'
                    url: 'calendar/events?idClub=0',
                    //cache: true,
                    error: function() {
                        $('#script-warning').show();
                    }
                },
                //resources: this.cabecera(),
                eventDrop: function (event, delta, revertFunc) {
                    var finalFrase = "";
                    if (delta._days == 0){
                        finalFrase = "la hora?";
                    } else if (delta._days == 1) {
                        finalFrase = delta._days + " día la partida?"
                    } else if (delta._days > 1) {
                        finalFrase = delta._days + " días la partida?"
                    }

                    if (!confirm("¿Estás seguro de cambiar " + finalFrase)) {
                        revertFunc();
                    }
                    /**
                    * perform ajax call for db update
                    */
                    //$.post('calendar/update', {'id': event.id, 'start': event.start.format(), 'end': event.end.format()}); 
                    axios.put('calendar' + '/' + event.id, {id: event.id, start: event.start.format(), end: event.end.format()})
                        .then((response) => {
                            //console.log(response.data);
                            $("#error").html(response.data)
                        }, (error) => {
                            //console.log(error);
                            $("#error").html(error)
                        });
                    },

                eventResize: function(event, delta, revertFunc) {

                    if (!confirm("¿La partida " + event.title + " empezará a las " + event.start.format().substring(11, 19) + " acabará a las " + event.end.format().substring(11, 19) + "?")) {
                        revertFunc();
                    }
                },
            }
        
        }
    },
    
    watch: {
        club: function(val){
            this.args.businessHours.start = val.horario.substring(1,5);
            this.args.businessHours.end = val.horario.substring(6,11);
            this.refrescarCal();
        },
    },
    
    mounted: function(){
        this.args.resources.url = 'calendar/resources?pistas=' + this.club.pistas;
        this.args.events.url = 'calendar/events?idClub=' + this.club.id;
        this.refrescarCal();
    },
    
    computed(){
        
    },
    
    methods: {
        refrescarCal: function(){
            var self = this;
            self.cal = $(self.$el);
                        
            //cargo los nuevos eventos a generar en un array
            var events = {
                url: 'calendar/events?idClub=' + this.club.id,
                type: 'get',
                data: {}
            }
            
            self.cal.fullCalendar( 'removeEventSources');//eliminio todos los eventos de recursos
            self.cal.fullCalendar( 'addEventSource', events);//añado los eventos de la nueva eleccion          
            self.cal.fullCalendar( 'refetchEvents');//cargo los cambios
            
            self.args.resources.url = 'calendar/resources?pistas=' + self.club.pistas;
            self.cal.fullCalendar('refetchResources');//elimino las cabeceras de la anterior eleccion.
            
            this.cargarCal(this.args);            
        },

        cargarCal: function(args){
            
            var self = this;
            self.cal = $(self.$el);
            
            /*if (self.editable){
                args.eventRender = function (event, element) {
                var Npalas = event.palas.length;
                element.find(".fc-title")
                    .append("<div class='fc-palas'>"
                        + event.palas.filter(Boolean).length
                        + " Palas</div>");
                element.find(".fc-content")
                    .popover({
                        container: "body",
                        html : true, //habilito el paso de etiquetas html
                        title: "<b style='text-align:center'>Palas</b>",
                        placement: "top",
                        trigger:"click",
                        content: function palas(){
                            var listadoPalas = "<ul class='text-capitalize' style='margin-left:-20%'>";
                                for (var x = 0; x < Npalas; x++){
                                    listadoPalas += "<li>" + event.palas[x] + "</li>";
                                }
                                listadoPalas += "</ul>";
                                return listadoPalas;
                        },
                    })
                }
            } else{
                args.eventRender = function (event, element) {
                var Npalas = event.palas.length;
                element.find(".fc-title")
                    .append(function (n){
                        var listadoPalas = "<div class='fc-palas text-capitalize row'>";
                        for (var x = 0; x < Npalas; x++){
                            listadoPalas += "<div class='col-sm-4'>" + event.palas[x] + "</div>";
                        }
                        listadoPalas += "</div>";
                        return listadoPalas;
                    });
                }
            }*/
            
            //console.log(args);
            self.cal.fullCalendar(args);
          
        },
    }
});

new Vue({
    el: '#partidas',
    data :{
        pageInfo: {
            pageUrl: 'calendar'
        },
        eventos: [],
        club: {'id': 0, 'nombre': 0, 'pistas': 0, 'horario': 0},
    },
    
    mounted: function(){
       //this.getEventos();
       //this.cargarClub();
    },
    
    methods: {
        getEventos: function(){
            axios.get("/calendar/events").then((response) => {
                this.eventos = response.data;
                //console.log(this.eventos);  
            })
        },
        
        prueba: function (datosclub) {
            this.club = datosclub;
            
        }
    },
    
});

