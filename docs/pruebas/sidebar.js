$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

new Vue({
    el: '#sidebar',
  
    data: {
        pageInfo: {
            pageUrl: 'sidebar',
        },
        ver : false,
        mensaje : '',
        clases : '',
        getImagePath:'',
        nombre: '',
        extension : '',
        size: '',
        
    },
	
    mounted: function(){
        this.upload();
        this.vaciar();
    },
	 
    methods: {
        previewImage: function(event) {
            var input = event.target;
            // Comprobamos que hay archivo
            if (input.files && input.files[0]) {
                this.nombre = $('#urlImg').val().replace(/\\/g, '/').replace(/.*\//, '');
		this.extension= $('#urlImg').val().split('\\').pop().split('.').pop();
                this.getImagePath = URL.createObjectURL(input.files[0]);
                this.size =  input.files[0].size;
            }
        },
              
        crear: function() {
            var formatos = "jpeg,png,jpg,gif,svg";            
            var input = new FormData($('#upload')[0]);
                       
            if(this.extension !='' && formatos.match(this.extension )&& (this.size/1024) < 2048){
                axios.post(this.pageInfo.pageUrl,input)
                    .then((response) => {
                        this.ver = true;
                        this.clases = response.data.clases;
                        this.mensaje = response.data.mensaje;
                        cambiarFondoSidebar();
                    })
                    .catch((error) => {
                        $("#contenidoPrincipal").html(error.response.data);
                        });
            }else if(!formatos.match(this.extension)){
                this.ver = true;
                this.clases = 'alert alert-info';
                this.mensaje = 'Formato de imagen incorrecto';
            }else if((this.size/1024) > 2048){
                this.ver = true;
                this.clases = 'alert alert-info';
                this.mensaje = 'Imagen demasiado grande';
            }
            
        },
        upload: function(){
            $(':file').on('fileselect', function(event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                log = label;
                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }
                });
        },
        
        vaciar : function(){
            $('#vaciar').on('click', function() {
                this.getImagePath = "";
                this.nombre = "";
            });
        },
		
		      
  } 
})


