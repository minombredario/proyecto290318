$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var pistas = '';
Vue.component('clubs',{
   
    //props: ['buscar'],
   
    template : '<select class="form-control" v-on:change="datosClub($event.target.value)" name="clubs">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in clubs" v-bind:value="item.id" class="text-capitalize">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
        axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
    },
    
    data() {
        return {
            clubs: null,
           
        }
    },
    methods: {
        datosClub(value) {
            var id = value;
            axios.get('/Npistas?id='+value).then((response) => {
                this.$emit('busqueda', response.data[0] );  
            })
            
          
        },   
    },
    
});

Vue.component('hora',{
     props: ['eleccion'],
    
    template:   '<select class="form-control" name="eleccion" v-on:change="elegirHora($event.target.value)">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in hora" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
    mounted() {    
        axios.get('/horas').then((response) => {
                this.hora = response.data;
               
            })
        
    },
    
    data() {
        return {
            hora: null,
            
        }
    },
    
    methods: {
        elegirHora(value) {
            var id = value;
            this.$emit('horario', id);  
        },   
    },
    
    
});

Vue.component('nivel',{
    props: ['eleccion'],
    
    template:   '<select class="form-control" id="csProv" v-model="eleccion.nivel">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in niveles" v-bind:value="item.nombre">{{item.nombre}}</option>'+
                '</select>',
              
    mounted() {
        axios.get('/listNiv').then((response) => {
            this.niveles = response.data;
        })
    },
    
    data() {
        return {
            niveles: null,
            
        }
    },
    
});

new Vue({
    
    el: '#pistas',
    
    data :{
        pageInfo: {
            pageUrl: '/pistas',
        },
        horarios: [],
        alquilerPista:{
            hora_inicio: '',
            hora_fin: '',
            pista: '',
            nombre: '',
            nivel: '',
        },
        dias: [],
        pistasOcupadas: '',
        nombreClub: '',
        pistas: '',
        mensaje : '',
        clases : '',
        formErrors:{},
        formErrorsUpdate:{},
       
            
    },
    computed: {
        saberSemana: function(){
                arraySemana = new Array();

                // obtenemos el primer dia del año
                var primerdia = new Date(anyo, 0, 1);

                //obtenemos el primer dia de la primera semana del año
                fecha=new Date(anyo,0,1);
                primerDiaDelAno=anado[fecha.getDay()];

                //calculamos el tiempo que ha pasado desde el primer dia del año al dia actual
                fecha= new Date(anyo,0,primerDiaDelAno);

                fecha2= new Date(anyo,mes,((dia+contDia)+primerDiaDelAno));

                tiempopasado=fecha2-fecha;
                semanas=Math.floor(tiempopasado/1000/60/60/24/7);

                if(semanas==0){
                        semanas=52
                }

                // obtenemos la corrección necesaria
                var correccion = 6 - primerdia.getDay();

                // obtenemos los dias de la semana
                for(var i = 3; i <= 9; i++){
                        arraySemana.push(new Date(anyo, 0, (semanas - 1) * 7 + i + correccion));
                };

                // mostramos el resultado
                document.getElementById("dia").innerHTML = 
                        "<div><h1 class='text-center'> Hoy es " + dias[diasemana] + ", " + dia + " de " + meses[mes] +" de " + anyo +" </h1></div>";
                document.getElementById("semana").innerHTML = 
                        "<div class='col-xs-6'>El primer día de la semana es " + arraySemana[0].getDate() + " de " + meses[arraySemana[0].getMonth()] + " (" + dias[arraySemana[0].getDay()] + ")</div>" +
                        "<div class='col-xs-6'>El último día de la semana es " + arraySemana[6].getDate() + " de " + meses[arraySemana[6].getMonth()] + " (" + dias[arraySemana[6].getDay()] + ")</div>";

                        //calendarioSemana();


        },
        
    },
    
    //arranque
    mounted: function(){
        this.getHorario();       
        //this.getPistas();
        //this.getCalendario();
        //this.getDias();
        //this.getDatos();
        //this.getClubs();
        
    },
        
    //metodos
    methods: {
        getHorario: function(){
            axios.get('/horas').then((response) => {
                this.horarios = response.data;
            })
        },
        
        getPistas: function(busqueda){
            this.pistas = busqueda.pistas;
            this.nombreClub = busqueda.nombre;
            pistas = busqueda.pistas;
        },
        
        getClubs: function(){
            axios.get('/Nclubs').then((response) => {
                this.clubs = response.data;
            })
        },
        
        prueba: function (busqueda){
          console.log(busqueda);
            this.pistas = busqueda.id;
            this.nombreClub = busqueda.nombre;
        },
        
        create: function(pista){
            console.log(pista);
            $("#create-item").modal('show');
           /*  axios.post(this.pageInfo.pageUrl,input).then((response) => {
                    $("#contenidoPrincipal").html(response.response);
                    //this.nuevo = {'nombre' : ''};
                    $("#create-item").modal('hide');
                    //this.mostrar = true;
                    //this.clases = "alert alert-success";
                    //this.mensaje ="Registro creado con exito.";
                    //this.getItems();

                }, (error) => {
                    $("#contenidoPrincipal").html(error.response.data);
                    //this.formErrors = error.data;
                    //this.mostrar = true;
                    //this.clases = "alert alert-danger";
                    //this.mensaje ="No se ha creado el registro.";

               });*/
        },
        
        alquilerPistaHoras: function(hora,pista){
            this.alquilerPista.hora_inicio = hora.nombre;
            var h = '';
            var m = '';
                    if((parseInt(hora.nombre.substring(3,5))+30) == 60){
                        if((parseInt(hora.nombre.substring(0,2))+2)<10){
                            h  = "0" + (parseInt(hora.nombre.substring(0,2))+2);
                        }else{
                            h  = parseInt(hora.nombre.substring(0,2))+2;
                        }
                        
                        m = "00";
                    }else if((parseInt(hora.nombre.substring(3,5))+30) == 30){
                        if((parseInt(hora.nombre.substring(0,2))+1)<10){
                            h  = "0" + (parseInt(hora.nombre.substring(0,2))+1);
                        }else{
                            h  = parseInt(hora.nombre.substring(0,2))+1;
                           
                        }
                         m = "30";
                    }
            console.log("hora: " + h + " minuto: " + m);
            this.alquilerPista.hora_fin = h + ":" + m + ":00";
           
            this.alquilerPista.pista = pista;
            
        },
    }
});





