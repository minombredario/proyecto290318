$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var bus = new Vue();
var provincias = '';
Vue.component('provincias',{
   
    props: ['eleccion'],
   
    template : '<select class="form-control" v-model="eleccion.id_provincia" @change="enviarProvincia" name="provincia">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in provincias" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
        axios.get('/prov_json').then((response) => {
                    this.provincias = response.data.items;
                    provincias = response.data.items;
        })
    },
    
    data() {
        return {
            provincias: null,
        }
    },
   
    methods: {
        enviarProvincia() {
            bus.$emit('provincia', this.eleccion.id_provincia);
        },   
    },
  
});
 
Vue.component('poblaciones',{
   
    props: ['eleccion'],
   
    template : '<select id="pob" class="form-control" v-model="eleccion.id_poblacion" name="poblacion" disabled>'+
                    '<option value="" selected disabled>Elige una población</option>'+
                    '<option v-for="item in filtrarPoblacion" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
   
    mounted() {
            
            axios.get('/pob_json').then((response) => {
                this.poblaciones = response.data.items;
            })
       
    },
    
    data() {
        return {
            poblaciones: [],
            busqueda : '',
        }
    },
   
    created: function(){
         bus.$on('provincia', (id)=>{
            this.busqueda = id;
            $('#pob').prop('disabled', false);
           
        })
        
    },
   
    computed: {
            filtrarPoblacion(){
                return this.poblaciones.filter((poblacion) => {return poblacion.id_prov == this.busqueda} );
            }
    },
   
});

Vue.component('lista',{
    
    props: ['eleccion'],
    
    template : 
            '<div>'+
                '<instalacion v-for="item in instalaciones"'+
                    ' v-bind:key="item.id"'+
                    ' v-bind:item="item"'+
                    ' v-model:eleccion="eleccion.instalacion">'+
                '</instalacion>'+
            '</div>',
   
    mounted() {
        
        axios.get('/insta_json').then((response) => {
            this.instalaciones = response.data.items;
        })
    },
    
    data() {
        return {
            instalaciones: [],
            listado: [],
        }
    },
    
});
Vue.component('instalacion',{
    props: ['item','eleccion'],
    
    
    template : '<div class="col-xs-5"><label class="checkbox-inline"><input type="checkbox" v-bind:value="item.nombre" v-model="checkedNames" name="instalaciones[]"/>{{item.nombre}}</label></div>',
        
    data() {
            return {
                _checkedNames: []
            }
        },

        computed: {
            checkedNames: {
                get: function () {
                    return this._checkedNames;
                },
                set: function (newVal) {
                    console.log(newVal); // but with computed we have true/false value instead of array
                    this._checkedNames = newVal;
                }
            }
        }
        
});

new Vue({
    
    el: '#listados',
   
    data :{
        pageInfo: {
            pageUrl: path
        },
        items: [],
        busqueda: '',
        
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        loading: false,
        mostrar : false,
        mensaje : '',
        clases : '',
        offset: 3,//numeros de paginas a mostrar por cada lado
        formErrors:{},
        formErrorsUpdate:{},
        nuevo : {'nombre' : '', 'id_provincia' : '', 'id_poblacion' : '', 'interclubes' : '', 'instalaciones' : []},
        fillItem : {'id': ''},
        
        getImagePath:'',
        nombre: '',
        extension : '',
        size: '',
    },
    
    computed: {
       
        activo: function() {
          return this.pagination.current_page;
        },
        
        numPagina: function() {
              if (!this.pagination.to) {
                    return [];
              }
              var from = this.pagination.current_page - this.offset;
              if (from < 1) {
                from = 1;
              }
              var to = from + (this.offset * 2);
              if (to >= this.pagination.to) {
                to = this.pagination.to;
                  
              }
            var paginas = [];
            while (from <= to) {
                paginas.push(from);
                from++;
            }
          return paginas;
        },
        
        paginate: function(){
            if(this.pagination.current_page >= this.pagination.total){
                
                this.pagination.current_page = Math.max(0, this.pagination.total-1);
                
            }
            var index = (this.pagination.current_page-1) * this.pagination.per_page;
            
            if(this.pagination.current_page <= 1){
                
                return this.items.slice(0,this.pagination.per_page);
                
            }else{
               
                return this.items.slice(index,index+this.pagination.per_page);
            }
        },
    },
  
  
    //arranque
    mounted: function(){
        if(path == 'sidebar'){
            this.upload();
            this.vaciar();
        }else{
            this.getItems();//llamada al metodo para cargar los datos con la primera pagina
        }
    },
    
    //metodos
    methods: {
        
        cambiarPagina: function(pagina) {
            this.pagination.current_page = pagina;
        },
        
        getItems: function() {
            this.loading = true;
            $("#loading").modal('show');
            axios.get(this.pageInfo.pageUrl).then((response) => {
                
                if(response.data.items.length > 0){
                    this.items = response.data.items;
                    this.pagination = {total : response.data.items.length, per_page: 10, from: 1, to : Math.ceil(response.data.items.length/10), current_page: 1};
                    this.loading = false;
                    $("#loading").modal('hide');
                }else{
                    
                    this.clases = "alert alert-warning";
                    this.mensaje = "No hay conexión";
                    this.loading = false;
                    $("#loading").modal('hide');  
                };
                
            })
            
        },
        getItem: function() {
            this.loading = true;
            $("#loading").modal('show');
            axios.get(this.pageInfo.pageUrl+'?busqueda='+this.busqueda).then((response) => {
                if(response.data.items.length > 0){
                    this.items = response.data.items;
                    this.pagination = {total : response.data.items.length, per_page: 10, from: 1, to : Math.ceil(response.data.items.length/10), current_page: 1};
                    this.loading = false;
                    $("#loading").modal('hide');
                }else{
                    this.mostrar = true;
                    this.clases = "alert alert-warning";
                    this.mensaje ="No se ha encontrado el registro";
                    this.loading = false;
                    $("#loading").modal('hide');                                       
                };    
            })
        },
        
        resetBusqueda: function(){
            this.busqueda = '';
            this.mostrar = false;
            $('buscar').val="";
            this.getItems();
        },
              
        
        //funciones del formulario//
        
        crear: function() {
            var input = new FormData($('#upload')[0]);
            if(path == 'sidebar'){
                var formatos = "jpeg,png,jpg,gif,svg";            
                //var input = new FormData($('#upload')[0]);

                if(this.extension !='' && formatos.match(this.extension )&& (this.size/1024) < 2048){
                    axios.post(this.pageInfo.pageUrl,input)
                        .then((response) => {
                            this.mostrar = true;
                            this.clases = response.data.clases;
                            this.mensaje = response.data.mensaje;
                            cambiarFondoSidebar();
                        })
                        .catch((error) => {
                            $("#contenidoPrincipal").html(error.response.data);
                            });
                }else if(!formatos.match(this.extension)){
                    this.mostrar = true;
                    this.clases = 'alert alert-info';
                    this.mensaje = 'Formato de imagen incorrecto';
                }else if((this.size/1024) > 2048){
                    this.mostrar = true;
                    this.clases = 'alert alert-info';
                    this.mensaje = 'Imagen demasiado grande';
                }
                
            }else{
                //var input = this.nuevo;
                //var input = new FormData($('#upload')[0]);
                
                axios.post(this.pageInfo.pageUrl,input).then((response) => {
                    $("#contenidoPrincipal").html(response.response);
                    this.nuevo = {'nombre' : ''};
                    $("#create-item").modal('hide');
                    this.mostrar = true;
                    this.clases = "alert alert-success";
                    this.mensaje ="Registro creado con exito.";
                    this.getItems();

                }, (error) => {
                    $("#contenidoPrincipal").html(error.response.data);
                    this.formErrors = error.data;
                    this.mostrar = true;
                    this.clases = "alert alert-danger";
                    this.mensaje ="No se ha creado el registro.";

               });
            }
        },
        
        borrar: function(item) {
            
            if(path == 'pob_json'){
                this.fillItem.id = item.id;
                this.fillItem.nombre = capitalize(item.nombre);
                this.fillItem.provincia = capitalize(item.provincia);
            }
            if(path == 'lvl_json' || path == 'prov_json' || path == 'dist_json' || path == 'hora_json' || path == "prec_json"){
                this.fillItem.id = item.id;
                this.fillItem.nombre = capitalize(item.nombre);
            }
            if (path == 'club_json'){
                this.fillItem.id = item.id;
                this.fillItem.nombre = item.nombre;
            }
            
            $("#eliminar-item").modal('show');          
        },
        
        eliminar: function(id){
            
            axios.delete(this.pageInfo.pageUrl+'/'+id).then((response) => {
                this.resetBusqueda();
                this.fillItem = {'id': ''};
                $("#eliminar-item").modal('hide');
                this.mostrar = true;
                this.clases = "alert alert-success";
                this.mensaje ="Registro eliminado con exito.";
            },(error) =>{
                this.formErrors = error.data;
                this.mostrar = true;
                this.clases = "alert alert-danger";
                this.mensaje ="No se puede eliminar el registro.";
               });
            
        },
                
        editar: function(item) {
            if(provincias == ''){
                axios.get('/prov_json').then((response) => {
                    provincias = response.data.items;
                })
            };
            if(path == 'pob_json'){
                
                var provincia = provincias.filter((provincia) => provincia.nombre == item.provincia );
                this.fillItem.id = item.id;
                this.fillItem.nombre = item.nombre;
                this.fillItem.provincia = item.provincia;
                this.fillItem.id_provincia = provincia[0].id;
            }
            if(path == 'lvl_json' || path == 'prov_json' || path == "dist_json" || path == 'hora_json' || path == "prec_json"){
                this.fillItem.id = item.id;
                this.fillItem.nombre = item.nombre;
            }
            if (path == 'club_json'){
                this.fillItem.id = item.id;
                this.fillItem.nombre = item.nombre;
            }
            
            $("#edit-item").modal('show');
            
        },
        
        actualizar: function(id) {
            var input = this.fillItem;
            axios.put(this.pageInfo.pageUrl+'/'+id,input).then((response) =>{
                this.getItem();
                this.fillItem = {'id': ''};
                $("#edit-item").modal('hide');
                this.mostrar = true;
                this.clases = "alert alert-success";
                this.mensaje ="Registro actualizado con exito.";
            }, (error) => {
                this.formErrors = error.data;
                this.mostrar = true;
                this.clases = "alert alert-danger";
                this.mensaje ="El registro no se ha actualizado.";
                              
            });
        },
        
        ver: function(item){
            this.fillItem.id = item.id;
            this.fillItem.nombre = capitalize(item.nombre);
            this.fillItem.cif = item.cif;
            this.fillItem.telefono = item.telefono;
            this.fillItem.email = item.email;
            this.fillItem.password = item.password;
            this.fillItem.direccion = item.direccion;
            this.fillItem.poblacion = item.poblacion;
            this.fillItem.provincia = item.provincia;
            this.fillItem.responsable = item.responsable;
            this.fillItem.pistas = item.pistas;
            this.fillItem.precio = item.precio + " €";
            this.fillItem.interclubes = item.interclubes;
            this.fillItem.horario = item.horario;
            this.fillItem.torneos = item.torneo;
            
            
            $("#ver-item").modal('show');
        },
        
        //funciones para el sidebar//
        previewImage: function(event) {
            var input = event.target;
            // Comprobamos que hay archivo
            if (input.files && input.files[0]) {
                this.nombre = $('#urlImg').val().replace(/\\/g, '/').replace(/.*\//, '');
		this.extension= $('#urlImg').val().split('\\').pop().split('.').pop();
                this.getImagePath = URL.createObjectURL(input.files[0]);
                this.size =  input.files[0].size;
            }
        },
        upload: function(){
            $(':file').on('fileselect', function(event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                log = label;
                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }
                });
        },
        
        vaciar : function(){
            $('#vaciar').on('click', function() {
                this.getImagePath = "";
                this.nombre = "";
            });
        },
        
        
    
    }
    
});

function capitalize(item){
    return item[0].toUpperCase() + item.substring(1)
}


