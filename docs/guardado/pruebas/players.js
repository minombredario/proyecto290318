$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('provincias',{
    
    props: ['eleccion'],
    
    template : '<select class="form-control" id="csProv" v-model="eleccion.id_provincia">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in provincias" v-bind:value="item.id">{{item.nombre}}</option>'+
                '</select>',
    
    mounted() {
        axios.get('/listProv').then((response) => {
                this.provincias = response.data;
                provincias = this.provincias;
            })
    }, 
    
    data() {
        return {
            provincias: null,
            
        }
    },
    
});

Vue.component('nivel',{
    props: ['eleccion'],
    
    template:   '<select class="form-control" id="csProv" v-model="eleccion.id_nivel">'+
                    '<option value="" selected disabled><slot></slot></option>'+
                    '<option v-for="item in niveles" v-bind:value="item.id">{{item.nivel}}</option>'+
                '</select>',
              
    
    mounted() {
        axios.get('/niveles').then((response) => {
            this.niveles = response.data;
        })
    },
    
    data() {
        return {
            niveles: null,
            
        }
    },
    
});

new Vue({
    
    el: '#jugadores',
    
    data :{
        pageInfo: {
            pageUrl: 'play_json'
        },
        jugadores: [],
        busqueda: '',
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 3,//numeros de paginas a mostrar por cada lado
        formErrors:{},
        formErrorsUpdate:{},
        nuevo : {'poblacion':'', 'provincia': '', 'id_provincia':'','id_nivel':'','posicion':'INDIFERENTE'},
        fillItem : {'id':'','nombre':'', 'id_provincia':'','provincia':''},
    },
    
    computed: {
        activo: function() {
          return this.pagination.current_page;
        },
        numPagina: function() {
          if (!this.pagination.to) {
            return [];
          }
          var from = this.pagination.current_page - this.offset;
          if (from < 1) {
            from = 1;
          }
          var to = from + (this.offset * 2);
          if (to >= this.pagination.last_page) {
            to = this.pagination.last_page;
          }
          var paginas = [];
          while (from <= to) {
            paginas.push(from);
            from++;
          }
          return paginas;
        }
    },
  
    //arranque
    mounted: function(){
        this.getJugadores();//llamada al metodo para cargar los datos con la primera pagina
                
    },
    
    
    //metodos
    methods: {
               
        getJugadores: function() {
            axios.get(this.pageInfo.pageUrl+'?page='+this.pagination.current_page+'&busqueda='+this.busqueda).then((response) => {
                if(response.data.players.data.length > 0){           
                    this.jugadores = response.data.players.data;
                    this.pagination = response.data.pagination;
                    
                }else{
                    toastr.warning('Busqueda sin resultados.', 'Error', {timeOut: 5000});
                    
                };    
            })
        },
        
        resetBusqueda: function(){
            this.busqueda = '';
            $('buscar').val="";
            this.getPoblaciones();
        },
        
        
        cambiarPagina: function(pagina) {
            this.pagination.current_page = pagina;
            this.getPoblaciones(pagina);
        },
        
        //funciones del formulario//
        
        crear: function() {
            var input = this.nuevo;
            
            axios.post(this.pageInfo.pageUrl,input).then((response) => {
               
                this.cambiarPagina(this.pagination.current_page);
                this.nuevo = {'nombre':'','id_provincia':''};
               $("#create-item").modal('hide');
               toastr.success('Registro añadido con exito.', 'Añadido: '+ capitalize(input.nombre), {timeOut: 5000});
            }, (error) => {
                this.formErrors = error.data;
                
           });
        },
        
        borrar: function(item) {
            this.fillItem.id = item.id;
            this.fillItem.nombre = capitalize(item.nombre);
            this.fillItem.provincia = capitalize(item.provincia);
            $("#eliminar-item").modal('show');          
        },
        
        eliminar: function(id){
            
            axios.delete(this.pageInfo.pageUrl+'/'+id).then((response) => {
                this.cambiarPagina(this.pagination.current_page);
                toastr.success('Registro eliminado con exito.', 'Success Alert', {timeOut: 5000});
                $("#eliminar-item").modal('hide');  
            });
            
        },
                
        editar: function(item) {
            var provincia = provincias.filter((provincia) => provincia.nombre == item.provincia );
            this.fillItem.id = item.id;
            this.fillItem.nombre = item.nombre;
            this.fillItem.provincia = item.provincia;
            this.fillItem.id_provincia = provincia[0].id;
            $("#edit-item").modal('show');
            
        },
        
        actualizar: function(id) {
            var input = this.fillItem;
            axios.put(this.pageInfo.pageUrl+'/'+id,input).then((response) =>{
                this.cambiarPagina(this.pagination.current_page);
                this.fillItem = {'id':'','nombre':'', 'id_provincia':'','provincia':''}
                $("#edit-item").modal('hide');
                toastr.success('Registro actualizado con exito.', 'Actualizado '+ capitalize(input.nombre), {timeOut: 5000});
            }, (response) => {
                this.formErrors = response.data;
            });
        },
         
        
        
    
    }
    
});

function capitalize(item){
    return item[0].toUpperCase() + item.substring(1)
}

$(document).ready(function(){
    $('#csPob').prop('disabled', 'disabled');
    
    $('#csProv').change(function(){
        $('#csPob').empty();
        $('#csPob').prop('disabled', false);
        axios.get('/listPob?busqueda='+$(this).val()).then((response) => {
                poblaciones = response.data;
                for(var i = 0; i<poblaciones.length; i++){
                    $('#csPob').append("<option value='"+poblaciones[i].id+"'>"+poblaciones[i].nombre+"</option>");    
                }
        });
    });
});




