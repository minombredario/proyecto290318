var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var dias = ["Domingo","Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
var horas = []
var date = new Date();
var dia = date.getDate();
var diasemana = date.getDay();
var mes = date.getMonth();
var anyo = date.getFullYear();
anado=[2,1,7,6,5,4,3];
var arraySemana = [];
var contDia = 0;
var cambioDia =  new Date();

$("#ant").on('click',function(){
	cambiar($(this).val(),$(this).attr('id'));
});
$("#post").on('click',function(){
	cambiar($(this).val(),$(this).attr('id'))
});
$("#hoy").on('click',function(){
	(contDia = 0);
	cambiar($(this).val(),$(this).attr('id'));
});
$("#agenda").on('click', function(){
	calendarioAgenda();
	$("#ant").val('agenda');
	$("#post").val('agenda');
	$("#hoy").val('agenda');
});
$("#week").on('click', function() {
	calendarioSemana();
	$("#ant").val('week');
	$("#post").val('week');
	$("#hoy").val('week');
});

function horario(){
	for (var i = 0; i<24; i++){
		if (i <10){
			horas.push("0"+i+":00");
			horas.push("0"+i+":30");
		}else{
			horas.push(i+":00");
			horas.push(i+":30");
		}
	}
}

function cambiar(el,op){
	if(el == "agenda" && op == "ant"){
		contDia -= 1;
		cambiarDia()
	}
	if(el == "agenda" && op == "post"){
		contDia += 1;
		cambiarDia();
	}
	if(el == "agenda" && op == "hoy"){
		cambiarDia();
	}
	if(el == "week" && op == "ant"){
		contDia -= 7;
		saberSemana();
	}
	if(el == "week" && op == "post"){
		contDia += 7;
		saberSemana();
	}
	
	if(el == "week" && op == "hoy"){
		saberSemana();
	}
}

function calendarioAgenda(){
	var tabla = "<table class='table table-striped'><th></th>";
	
	//cabecera de la tabla con las pistas del club
	for(var i = 1; i <= 4; i++){
		tabla += "<th class='text-center'><p> Pista " + i +"</p><small>" + cambioDia.getDate()+"/"+ (cambioDia.getMonth()+1) + "</small></th>";
	}
	
	for (var i = 0; i <horas.length; i++){
		tabla += "<tr><td>"+ horas[i] + "</td>";
		for (var x = 0; x < 4; x++){
			if(arraySemana[x].getDate() == date.getDate()){
				tabla += "<td class='active'></td>";
			}else{
				tabla += "<td></td>";
			}
			
		}
		tabla+= "</tr>";
	};
	
	tabla += "</table>";
	$("#calendario").html(tabla);

}

function calendarioSemana(){

	var tabla = "<table class='table table-striped'><th></th>";
	//cabecera del calendario con los dias de la semana y numero del mes
	for(var i = 1; i <= dias.length; i++){
		if(i == 7 ){
			if(arraySemana[6].getDate() == dia && arraySemana[6].getMonth() == mes ){
				tabla += "<th class='active text-center'>"+ dias[0] + "<br><small>" + arraySemana[6].getDate() + " de " + meses[arraySemana[6].getMonth()] + "</small></th>";
			}else{
				tabla += "<th class='text-center'>"+ dias[0] + "<br><small>" + arraySemana[6].getDate() + " de " + meses[arraySemana[6].getMonth()] + "</small></th>";
			}
			
		}else{
			if(arraySemana[i-1].getDate() == dia && arraySemana[i-1].getMonth() == mes ){
			
				tabla += "<th class='active text-center'>"+ dias[i] + "<br><small>" + arraySemana[i-1].getDate() + " de " + meses[arraySemana[i-1].getMonth()] + "</small></th>";
			}else{
				tabla += "<th class='text-center'>"+ dias[i] + "<br><small>" + arraySemana[i-1].getDate() + " de " + meses[arraySemana[i-1].getMonth()] + "</small></th>";
			}
				
		}
			
		
	}
	//columna de las horas del día
	for (var i = 0; i <horas.length; i++){
		tabla += "<tr><td>"+ horas[i] + "</td>";
		for (var x = 0; x < dias.length; x++){
			if( (x == date.getDay()-1) && (arraySemana[x].getDate() == date.getDate())){
				tabla += "<td class='active'></td>";
			}else{
				tabla += "<td></td>";
			}
			
		}
		tabla+= "</tr>";
	};
	
	tabla += "</table>";
	$("#calendario").html(tabla);
}

function saberSemana(){
	arraySemana = new Array();
	
	// obtenemos el primer dia del año
	var primerdia = new Date(anyo, 0, 1);
	
	//obtenemos el primer dia de la primera semana del año
	fecha=new Date(anyo,0,1);
	primerDiaDelAno=anado[fecha.getDay()];
	
	//calculamos el tiempo que ha pasado desde el primer dia del año al dia actual
	fecha= new Date(anyo,0,primerDiaDelAno);
	
	fecha2= new Date(anyo,mes,((dia+contDia)+primerDiaDelAno));
	console.log(fecha2/1000/60/60/24);
	tiempopasado=fecha2-fecha;
	semanas=Math.floor(tiempopasado/1000/60/60/24/7);
		
	if(semanas==0){
		semanas=52
	}
	
	// obtenemos la corrección necesaria
	var correccion = 6 - primerdia.getDay();
		
	// obtenemos los dias de la semana
	for(var i = 3; i <= 9; i++){
		arraySemana.push(new Date(anyo, 0, (semanas - 1) * 7 + i + correccion));
	};
	
	// mostramos el resultado
	document.getElementById("dia").innerHTML = 
		"<div><h1 class='text-center'> Hoy es " + dias[diasemana] + ", " + dia + " de " + meses[mes] +" de " + anyo +" </h1></div>";
	document.getElementById("semana").innerHTML = 
		"<div class='col-xs-6'>El primer día de la semana es " + arraySemana[0].getDate() + " de " + meses[arraySemana[0].getMonth()] + " (" + dias[arraySemana[0].getDay()] + ")</div>" +
		"<div class='col-xs-6'>El último día de la semana es " + arraySemana[6].getDate() + " de " + meses[arraySemana[6].getMonth()] + " (" + dias[arraySemana[6].getDay()] + ")</div>";
	
		calendarioSemana();
	
		
}

function cambiarDia(){
	cambioDia =  new Date(anyo, mes, date.getDate()+contDia);
	document.getElementById("dia").innerHTML = 
		"<div class='text-center'><h1> Hoy es " + dias[date.getDay()] + ", " + date.getDate() + " de " + meses[date.getMonth()] +" de " + date.getFullYear() +" </h1></div>";
	
	calendarioAgenda();	
}

saberSemana();
horario();
calendarioAgenda();