$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

new Vue({
    
    el: '#listados',
    
    data :{
        pageInfo: {
            pageUrl: path
        },
        datos: [],
        busqueda: '',
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        loading: false,
        ver : false,
        mensaje : '',
        clases : '',
        offset: 3,//numeros de paginas a mostrar por cada lado
        formErrors:{},
        formErrorsUpdate:{},
        nuevo : {'nombre':''},
        fillItem : {'id':'','nombre':''},
        path : path,
    
    },
    
    computed: {
       
        activo: function() {
          return this.pagination.current_page;
        },
        
        numPagina: function() {
              if (!this.pagination.to) {
                    return [];
              }
              var from = this.pagination.current_page - this.offset;
              if (from < 1) {
                from = 1;
              }
              var to = from + (this.offset * 2);
              if (to >= this.pagination.to) {
                to = this.pagination.to;
                  
              }
            var paginas = [];
            while (from <= to) {
                paginas.push(from);
                from++;
            }
          return paginas;
        },
        
        paginate: function(){
            if(this.pagination.current_page >= this.pagination.total){
                
                this.pagination.current_page = Math.max(0, this.pagination.total-1);
                
            }
            var index = (this.pagination.current_page-1) * this.pagination.per_page;
            
            if(this.pagination.current_page <= 1){
                
                return this.datos.slice(0,this.pagination.per_page);
                
            }else{
               
                return this.datos.slice(index,index+this.pagination.per_page);
            }
        },
    },
  
    //arranque
    mounted: function(){
        
        this.getItems();//llamada al metodo para cargar los datos con la primera pagina
        
	},
        
       
    //metodos
    methods: {
        cambiarPagina: function(pagina) {
            this.pagination.current_page = pagina;
        },
        
        getItems: function() {
            this.loading = true;
            $("#loading").modal('show');
            axios.get(this.pageInfo.pageUrl).then((response) => {
                if(response.data.datos.length > 0){
                    this.datos = response.data.datos;
                    this.pagination = {total : response.data.datos.length, per_page: 10, from: 1, to : Math.ceil(response.data.datos.length/10), current_page: 1};
                    this.loading = false;
                    $("#loading").modal('hide');
                }else{
                    
                    this.clases = "alert alert-warning";
                    this.mensaje = "No hay conexión";
                    this.loading = false;
                    $("#loading").modal('hide');  
                };      
            })
        },
        
        getItem: function() {
            this.loading = true;
            $("#loading").modal('show');
            axios.get(this.pageInfo.pageUrl+'?busqueda='+this.busqueda).then((response) => {
                if(response.data.datos.length > 0){
                    this.datos = response.data.datos;
                    this.pagination = {total : response.data.datos.length, per_page: 10, from: 1, to : Math.floor(response.data.datos.length/10), current_page: 1};
                    //this.pagination = response.data.pagination;
                this.loading = false;
                    $("#loading").modal('hide');
                }else{
                    this.ver = true;
                    this.clases = "alert alert-warning";
                    this.mensaje ="No se ha encontrado el registro";
                    this.loading = false;
                    $("#loading").modal('hide');                                       
                };    
            })
        },
        
        resetBusqueda: function(){
            this.busqueda = '';
            this.ver = false;
            $('buscar').val="";
            this.getItems();
        },
        
        
        
        
        //funciones del formulario//
        
        crear: function() {
            var input = this.nuevo;
            
            axios.post(this.pageInfo.pageUrl,input).then((response) => {
                //this.cambiarPagina(this.pagination.current_page);
                this.nuevo = {'nombre':''};
                $("#create-item").modal('hide');
                this.ver = true;
                this.clases = "alert alert-success";
                this.mensaje ="Registro creado con exito.";
                
            }, (error) => {
                this.formErrors = error.data;
                this.ver = true;
                this.clases = "alert alert-danger";
                this.mensaje ="No se ha creado el registro.";
                               
           });
        },
        
        borrar: function(item) {
            this.fillItem.id = item.id;
            this.fillItem.nombre = capitalize(item.nombre);
            $("#eliminar-item").modal('show');          
        },
        
        eliminar: function(id){
            
            axios.delete(this.pageInfo.pageUrl+'/'+id).then((response) => {
                $("#eliminar-item").modal('hide');
                this.ver = true;
                this.clases = "alert alert-success";
                this.mensaje ="Registro eliminado con exito.";
                this.resetBusqueda();
                
            },(error) =>{
                this.formErrors = error.data;
                this.ver = true;
                this.clases = "alert alert-danger";
                this.mensaje ="No se puede eliminar el registro.";
               });
            
        },
                
        editar: function(item) {
            this.fillItem.id = item.id;
            this.fillItem.nombre = item.nombre;
            $("#edit-item").modal('show');
            
        },
        
        actualizar: function(id) {
            var input = this.fillItem;
            axios.put(this.pageInfo.pageUrl+'/'+id,input).then((response) =>{
                this.getBusqueda();
                this.fillItem = {'id':''};
               $("#edit-item").modal('hide');
                this.ver = true;
                this.clases = "alert alert-success";
                this.mensaje ="Registro actualizado con exito.";
            }, (error) => {
                this.formErrors = error.data;
                this.ver = true;
                this.clases = "alert alert-danger";
                this.mensaje ="El registro no se ha actualizado.";
               
            });
        },
         
        
    
    },
    
   
});

function capitalize(item){
    return item[0].toUpperCase() + item.substring(1)
}




