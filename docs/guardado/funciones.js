$(document).ready(function(){
      var path =   '';
      var url = '';
       
    /*añadir clase activa al pulsar*/
    $('.nav li').click(function() {
        $(this).siblings('li').removeClass('active');
        $(this).addClass('active');
    });
   
   /*quitar class modal-backdrop para que no de problemas con las ventanas modales*/ 
   //$("div").removeClass("modal-backdrop");
    
    
   cambiarFondoSidebar();
     
});

function listadosUsuarios(){
    $('#csPob').prop('disabled', 'disabled');
    
    axios.get('/listProv').then((response) => {
        provincias = response.data;
        for(var i = 0; i<provincias.length; i++){
            $('#csProv').append("<option value='"+provincias[i].id+"'>"+provincias[i].nombre+"</option>");    
        }
    });
    
    axios.get('/listNiv').then((response) => {
        niveles = response.data;
        
        for(var i = 0; i<niveles.length; i++){
            $('#csNiv').append("<option value='"+niveles[i].id+"'>"+niveles[i].nivel+"</option>");    
        }
    });
     
    $('#csProv').change(function(){
        $('#csPob').empty();
        $('#csPob').prop('disabled', false);
        axios.get('/listPob?busqueda='+$(this).val()).then((response) => {
                poblaciones = response.data;
                for(var i = 0; i<poblaciones.length; i++){
                    $('#csPob').append("<option value='"+poblaciones[i].id+"'>"+poblaciones[i].nombre+"</option>");    
                }
        });
    });
    
};

function cargarformulario(arg){
    //funcion que carga todos los formularios del sistema
        if (arg == "provincias"){  url = "/provincias"; path = 'prov_json'}
        if (arg == "poblaciones"){  url = "/poblaciones"; path = 'pob_json'}
        if (arg == "sidebar"){  url = "/sidebar"; path = 'sidebar'}
        if (arg == "niveles"){  url = "/niveles"; path = "lvl_json"}
        if (arg == "distancias"){  url = "/distancias"; path = "dist_json"}
        if (arg == "horarios"){  url = "/horarios"; path = "hora_json"}
        if (arg == "instalaciones"){  url = "/instalaciones"; path = "insta_json"}
        if (arg == "precios"){  url = "/precios"; path = "prec_json"}
        if (arg == "clubs"){  url = "/clubs"; path = "club_json"}
         if (arg == "pistas"){  url = "/calendar"; path = "pista_json"}
        if (arg == "nada"){  url = ""; }
        $("#contenidoPrincipal").html($("#capturado").html());
        axios.get(url).then((response) => {
            $("#contenidoPrincipal").html(response.data);
        });
};
 
function cambiarFondoSidebar(){
        $sidebar = $('.sidebar');
        
        axios.get('/cargar').then((response) => {
        var path = "../img/sidebar/";
        var img = "'"+path+response.data.url+"'";
        
        var image_src = $sidebar.data('image',img);
            if(image_src !== undefined){
                sidebar_container = '<div class="sidebar-background" style="background-image: url(' + img + ') "/>'
                $sidebar.append(sidebar_container);
            } 
        });    

};




