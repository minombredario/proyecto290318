<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Padel') }}</title>
        <link href="{{ asset('bootstrap/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="//rawgit.com/seankenny/fullcalendar/v2/dist/fullcalendar.css" />
        <link href="{{ asset('lib/css/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
        <!--link href="{{ asset('lib/css/scheduler.min.css') }}" rel='stylesheet' /-->
       
          
                        
        <!--     Fonts and icons     -->
        <link href="{{ asset('bootstrap/font-awesome.min.css') }}" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
        		
        <!-- jquery -->
        <script src="{{ asset('lib/js/jquery.min.js') }}"></script>
        <script src="{{ asset('lib/js/jquery-ui.min.js') }}"></script>
        
        <!-- vuejs -->
        <script type="text/javascript" src="https://unpkg.com/vue@2.2.6/dist/vue.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        
        <!--fullcalendar-->
        <script src="{{ asset('lib/js/moment.min.js') }}"></script>
        <!--script src="{{ asset('lib/js/fullcalendar.js') }}"></script-->
        <script src="{{ asset('lib/js/fullcalendar-day.js') }}"></script>
        <!--script src="{{ asset('lib/js/scheduler.min.js') }}"></script-->
        <script src="{{ asset('lib/js/locale-all.js') }}"></script>
        
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        
        <!--  Light Bootstrap Table core CSS    -->
         <link href="{{ asset('css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>

         <!--link href="{{ asset('css/my-style.css') }}" rel="stylesheet"/-->

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
           <div class="wrapper">
                <div class="sidebar" data-color="green" data-image="">

                    <!--data-color="blue | azure | green | orange | red | purple"
                       

                    -->

                    <div class="sidebar-wrapper">
                        <div class="logo">
                            <a href="#" class="simple-text">
                                {{ config('app.name', 'Padel') }}
                            </a>
                        </div>

                        <ul class="nav">
                            <li>
                                <a href="javascript:void(0)" onclick="cargarformulario('nada')">
                                    <i class="pe-7s-graph"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="cargarformulario('provincias')">
                                    <i class="pe-7s-note2"></i>
                                    <p>Provincias</p>
                                </a>
                            </li>

                            <li>
                                <a href="javascript:void(0)" onclick="cargarformulario('poblaciones')">
                                    <i class="pe-7s-paint-bucket"></i>
                                    <p>Poblaciones</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('sidebar')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Sidebar</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('niveles')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Niveles</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('distancias')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Distancias</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('horarios')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Horarios</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('instalaciones')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Instalaciones</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('precios')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Precios</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('clubs')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Clubs</p>
                                </a>
                            </li>
                            <li>
                               <a href="javascript:void(0)" onclick="cargarformulario('pistas')">
                                    <i class="pe-7s-plugin"></i>
                                    <p>Pistas</p>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="main-panel">
                    <nav class="navbar navbar-default navbar-fixed">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-left">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-refresh"></i> 17
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-comments-o"></i> 0
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            SEO
                                        </a>
                                    </li>
                                </ul>


                                <ul class="nav navbar-nav navbar-right">
                                @if (Auth::guest())
                                    <li><a data-toggle="modal" data-target="#login">Login</a></li>
                                        @include('auth.login')
                                        @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                                </ul>
                            </div>
                        </div>

                    </nav>


                    <div class="content">
                        <div class="container-fluid">
                            <div class="row" id="contenidoPrincipal">
                                
                                    
                            </div>
                        </div>
                    </div>


                    <footer class="footer">
                        <div class="container-fluid">
                            <nav class="pull-left">
                                <ul>
                                    <li>
                                        <a href="#">
                                            Home 
                                        </a>
                                    </li>

                                </ul>
                            </nav>
                            <p class="copyright pull-right">
                                &copy; 2017 <a href="#">Darío Navarro</a>
                            </p>
                        </div>
                    </footer>
					


                </div>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/funciones.js') }}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>       
    </body>
</html>


