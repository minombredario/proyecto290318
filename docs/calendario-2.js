$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Vue.component('clubs',{
   
    template : '<select class="form-control" v-model="this.club" @change="datosclub">'+
                    '<option value="Seleccione un club" selected disabled>Seleccione un club</option>'+    
                    '<option v-for="item in clubs" v-bind:value="item">{{item.nombre}}</option>'+
                '</select>',
    
    mounted() {
        axios.get('listado_clubs').then((response) => {
                    this.clubs = response.data;
                })
    }, 
    
    data() {
        return {
            clubs: [],
            club: {'id': '', 'nombre': '', 'horario': '', 'pistas' : ''},
            //pistas: null,
            //events:null,
        }
    },
    
    methods: {
        datosclub() {
            
            this.club = club;
            this.$emit('datosclub', this.club);
        },    
    },
       
});

var self = this;
self.cal = $(self.$el);
var inicio = '';
Vue.component('calendar', {
    template: '<div></div>',
    props: {
        eventos: {
            type: Array,
            required: false
        },
        pistas: {
            type: Array,
            required: false
        },
        
        club: {
            required: true,
            
        },
        editable: {
            type: Boolean,
            required: true,
            default: true
        },
        eventoverlap: {
            type: Boolean,
            required: true,
            default: false
        },
        droppable: {
            type: Boolean,
            required: true,
            default: true
        },
    },
    data: function(){
        return {
            cal: null,
            resources: resources: [{
      'id': 'resource1',
      'name': 'Resource 1'
    }, {
      'id': 'resource2',
      'name': 'Resource 2'
    }, {
      'id': 'resource3',
      'name': 'Resource 3',
      'className': ['red']
    }],
            events: this.eventos,
            cerrarPop : 1,
            args: {
                aspectRatio: 1.8,
                header: {
                    left: 'today prev,next',
                    center: 'title',
                    right: 'month,agendaWeek,resourceDay'
                },
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                buttonText: {
                    week: 'Semana',
                    day: 'Día',
                    month: 'Mes',
                    today: 'Hoy'
                },
                scrollTime: '07:00:00',
                slotDuration: '00:30:00',
                defaultView: 'resourceDay',
                firstDay:1,
                allDaySlot: false, //mostrar la fila todo el dia
                defaultDate: new Date(),
                axisFormat: 'HH:mm', //vista de la hora en el evento
                //eventOverlap: this.eventoverlap, //solapar eventos
                slotEventOverlap:false,
                editable: this.editable, //editar eventos
                droppable: this.droppable, //arrastrar y soltar*/
                defaultTimedEventDuration: '01:30:00',
                minTime: '00:00',
                maxTime: '24:00',
                firstHour: inicio,
                resources: [{"id": 1, "name" : "Agenda de Partidas"}],
                events: [],
                               
            },
        }
    }, 
    
    watch: {
        eventos: function(val){
            this.args.scrollTime = this.club.horario.substring(1,5);
            this.args.maxTime = this.club.horario.substring(6,11);
            this.resources = this.cargarResources(this.club.pistas);
            this.events = val;
            this.cargarCal();
        },
    },
    
    mounted: function(){
        this.cargarCal();
    },
    
    methods: {
        
        cargarResources: function(pistas){
            
            var resources =   [];
            
            for(var x = 0; x < pistas; x++){
                pista = x + 1;
                resources[x] = {"id": pista, "name" : "Pista " + pista}; 
            };
        
            return resources;
        },
        cerrarPop: function(){
            console.log('cerrar');
        },
        cargarCal: function(){
            
            var self = this;
            self.cal = $(self.$el);
            
            if (this.editable){
                this.args.eventRender = function (event, element) {
                    var Npalas = event.palas.length;
                    element.find(".fc-event-title")
                        .append("<div class='fc-palas'>"
                            + event.palas.filter(Boolean).length
                            + " Palas</div>");
                    element.find(".fc-palas")
                        .popover({
                            container: "body",
                            html : true, //habilito el paso de etiquetas html
                            title: "<b>Palas</b><button type='button' class='close' aria-label='Close' onclick='cerrarPop($(this))'><span aria-hidden='true'>×</span></button>",
                            placement: "top",
                            trigger:"hover",
                            content: function palas(){
                                var listadoPalas = "<ul style='margin-left:-20%'>";
                                    for (var x = 0; x < Npalas; x++){
                                        listadoPalas += "<li>" + event.palas[x] + "</li>";
                                        
                                    }
                                    listadoPalas += "</ul>";
                                    return listadoPalas;
                            },
                        }).click(function(e) { 
                    e.preventDefault(); 
                    $(this).focus(); 
                });
                }
            } else{
                this.args.eventRender = function (event, element) {
                var Npalas = event.palas.length;
                element.find(".fc-event-title")
                    .append(function (n){
                        var listadoPalas = "<div class='fc-palas row'>";
                        for (var x = 0; x < Npalas; x++){
                            listadoPalas += "<div class='col-sm-6'>" + event.palas[x] + "</div>";
                        }
                        listadoPalas += "</div>";
                        return listadoPalas;
                    });
                }
            }
            
           
            self.cal.fullCalendar('setResources', this.resources);//cargo las pistas del club
            self.cal.fullCalendar('removeEvents'); //eliminolos eventos del calendario
            self.cal.fullCalendar('render');//renderizo el calendario
            self.cal.fullCalendar('addEventSource', this.events);//cargo los evetos
            self.cal.fullCalendar(self.args);
          
        },
    }
});

new Vue({
    el: '#partidas',
    data :{
        pageInfo: {
            pageUrl: 'calendar'
        },
        club: [],
        eventos: [],
        
    },
    
    methods: {
        prueba: function (datosclub) {
            this.club = datosclub;     
            axios.get('calendar/events?idClub=' + datosclub.id).then((response) => {
                    this.eventos = response.data;
                })
        },
        
    },
    
});

function cerrarPop(e){
    //$('div[id ^= popover]').popover('hide');
    
    $('div[id ^= popover]').popover('hide');
    
    
  
  
    
}

