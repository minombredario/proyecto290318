<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidasFijas extends Model
{
    protected $fillable = [
       'club_id','pistas_id', 'user_id' ,'inicio_alquiler' 
    ];
    
    public $timestamps = false;

    public function partidaFija(){
        return $this->belongsToMany(Partidas::class, 'partidas_fijas');
    }
}
