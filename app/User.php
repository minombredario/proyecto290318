<?php

namespace App; 

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
       'nombre', 'apellidos', 'sexo', 'poblacion_id', 'telefono', 'email', 
       'password','nick', 'avatar', 'clubs_id', 'posicion', 'desplazamiento', 
       'disponibilidad', 'nivel_id', 'rol_id', 'notificaciones', 'comentarios',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    public function poblacion(){

         return $this->belongsTo(Poblaciones::class);
    }

    public function clubs(){

        return $this->belongsToMany(Clubs::class)
            ->withPivot('email');
    }

    public function rol(){
        return $this->belongsTo(Roles::class);
    }
    
    public function nivel(){
        return $this->belongsTo(Niveles::class);    
    }

    public function partidas(){
        return $this->belongsToMany(Clubs::class, 'partidas')
            ->withPivot('partida_id','inicio_alquiler', 'fin_alquiler', 'status');
    }
    public function JugadorPartida(){
        return $this->belongsToMany(Partidas::class, 'partidas_user')
            ->withPivot('partida_id', 'user_id', 'pala', 'pala2', 'pala3', 'pala4');
    }

    public function scopeBusquedaExtendida($query,$club,$nivel,$posicion,$dispon,$poblacion,$provincia, $sortBy, $orden){
        /*$club = str_ireplace(" ", "%", $club);
        $nivel = str_ireplace(" ", "%", $nivel);
        $posicion = str_ireplace(" ", "%", $posicion);
        $postal = str_ireplace(" ", "%", $postal);
        $poblacion = str_ireplace(" ", "%", $poblacion);
        $provincia = str_ireplace(" ", "%", $provincia);*/

        $posicion = $posicion == "TODAS" ? '' : $posicion;
        $nivel = $nivel == "TODOS" ? '' : $nivel;
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
        //$datos = [];
        
        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'provincias.id', '=', 'poblaciones.provincia_id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->Where('clubs.nombre','like', '%'. $club .'%')
            ->Where('users.posicion','like', '%'. $posicion .'%')
            ->Where('users.nivel_id','like', '%'. $nivel .'%')
            ->Where('poblaciones.nombre','like', '%'. $poblacion .'%')
            ->Where('provincias.nombre','like', '%'. $provincia.'%')
            ->Where(function ($query) use ($dispon){ 
                $dispon == '' 
                    ? $query->where('users.disponibilidad', '=', null)->orwhere('users.disponibilidad', 'like', '%' . $dispon . '%')
                    : $query->Where('users.disponibilidad', 'like', '%' . $dispon . '%');
                })
            ->orderBy($sortBy, $orden)
            ->paginate(8);
            
       //Raw('CASE WHEN user_id='. $value .' THEN content_id ELSE user_id END AS friendID'))
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.email', '=', $jugador->email)
                ->get();
                $jugador->clubs = $clubs;
        };

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $jugador->id)
                ->get();
                $jugador->partidas = $partidas;
        };    

       

        return $jugadores;


    }

    public function scopeBusqueda($query,$busqueda,$sortBy,$orden){
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
        $datos = [];

        
        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->where('users.nombre','like', '%'.$busqueda.'%')
            ->orWhere('users.nick','like', '%'.$busqueda.'%')
            ->orWhere('users.posicion','like', '%'.$busqueda.'%')
            ->orWhere('users.telefono','like', '%'.$busqueda.'%')
            ->orWhere('clubs.id','like', '%'.$busqueda.'%')
            ->orWhere('niveles.nombre','like', $busqueda.'%')
            ->orWhere('poblaciones.nombre','like', $busqueda.'%')
            ->orderBy($sortBy, $orden)
            ->paginate(8); 
       
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.email', '=', $jugador->email)
                ->get();
                $jugador->clubs = $clubs;
        };

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $jugador->id)
                ->get();
                $jugador->partidas = $partidas;
        };    

       

        return $jugadores;
    }
    public function scopeBusquedaClub($query,$busqueda,$sortBy,$orden,$club_id){
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
        $datos = [];

        
        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->whereRaw('`users`.`clubs_id` =' . $club_id .' and (`users`.`nombre` like "%' . $busqueda . '%" or `users`.`nick` like "%' . $busqueda . '%" or `users`.`posicion` like "%' . $busqueda . '%" or `users`.`telefono` like "%' . $busqueda .'%" or `clubs`.`id` like "%' . $busqueda . '%" or `niveles`.`nombre` like "' . $busqueda .'%" or `poblaciones`.`nombre` like  "' . $busqueda .'%")')
           ->orderBy($sortBy, $orden)
            ->paginate(8); 
       
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.email', '=', $jugador->email)
                ->get();
                $jugador->clubs = $clubs;
        };

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $jugador->id)
                ->get();
                $jugador->partidas = $partidas;
        };

        return $jugadores;

    }

    public function scopeBusquedaExtendidaClub($query,$club,$nivel,$posicion,$dispon,$poblacion,$provincia, $sortBy, $orden){
        
        $posicion = $posicion == "TODAS" ? '' : $posicion;
        $nivel = $nivel == "TODOS" ? '' : $nivel;
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
                
        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'provincias.id', '=', 'poblaciones.provincia_id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->Where('clubs.id','=', $club)
            ->Where('users.posicion','like', '%'. $posicion .'%')
            ->Where('users.nivel_id','like', '%'. $nivel .'%')
            ->Where('poblaciones.nombre','like', '%'. $poblacion .'%')
            ->Where('provincias.nombre','like', '%'. $provincia.'%')
            ->Where(function ($query) use ($dispon){ 
                $dispon == '' 
                    ? $query->where('users.disponibilidad', '=', null)->orwhere('users.disponibilidad', 'like', '%' . $dispon . '%')
                    : $query->Where('users.disponibilidad', 'like', '%' . $dispon . '%');
                })
            ->orderBy($sortBy, $orden)
            ->paginate(8);
            
       //Raw('CASE WHEN user_id='. $value .' THEN content_id ELSE user_id END AS friendID'))
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.email', '=', $jugador->email)
                ->get();
                $jugador->clubs = $clubs;
        };

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $jugador->id)
                ->get();
                $jugador->partidas = $partidas;
        };    

       

        return $jugadores;


    }


    public function scopeMostrarClubUser($query,$id, $club_id){

        $jugador = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->where('users.id','=', $id)
            ->get(); 
       
        $partidas = DB::table('partidas')
            ->where('partidas.user_id', '=', $id)
            ->where('partidas.clubs_id', '=', $club_id)
            ->get();

        $response = [
            'jugador' => $jugador,
            'partidas' => $partidas,
        ];    

        return $response;
  
    }

    public function scopeUser($query,$id){

        $jugador = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->where('users.id','=', $id)
            ->first(); 
       
        return $jugador;
  
    }
}
