<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jumbotron extends Model
{
	protected $table = 'jumbotron';

    protected $fillable = [
       'club_id','titulo','contenido', 'footer','nombre','web', 'visible',
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
