<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instalaciones extends Model
{
	protected $table = 'instalaciones';
    protected $fillable = [
       'nombre', 'slug'
    ];
    public $timestamps = false;
    
    public function clubs(){

        return $this->belongsToMany(Clubs::class);
    }
}
