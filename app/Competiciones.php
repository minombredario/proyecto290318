<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competiciones extends Model
{
    protected $table = 'competiciones';

    protected $fillable = [
       'club_id','competicion'
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
