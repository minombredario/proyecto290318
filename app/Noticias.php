<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $fillable = [
       'club_id','titulo','slug', 'titular','contenido','imagen','web', 'status', 'visible'
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
