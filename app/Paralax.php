<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paralax extends Model
{
   protected $table = 'paralax';

    protected $fillable = [
       'club_id','titulo','contenido', 'footer','nombre','web', 'visible',
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
