<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patrocinadores extends Model
{
   protected $fillable = [
       'club_id','nombre','slug', 'web','imagen','status','visible', 'patrocina'
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
