<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Welcome extends Model
{
   protected $table = 'welcome';

    protected $fillable = [
       'club_id','titulo','contenido', 'status','visible',
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
