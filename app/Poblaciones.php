<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Poblaciones extends Model
{
    protected $fillable = [
        'postal', 'nombre',	'slug',	'provincia_id',	'latitud', 'longitud'

    ];
    public $timestamps = false;
    
    public function provincia()
    {
        return $this->belongsTo(Provincias::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

     public function clubs(){
        return $this->hasMany(Clubs::class);
    }

    public function scopeBusqueda($query,$busqueda,$sortBy,$orden){
       
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
        
        return DB::table('poblaciones')
            ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
            ->select('poblaciones.id','poblaciones.postal','poblaciones.nombre', 'poblaciones.slug','poblaciones.provincia_id','provincias.nombre as provincia', 'poblaciones.latitud', 'poblaciones.longitud')
            ->orwhere('poblaciones.nombre' ,'like','%' . $busqueda .'%')
            ->orwhere('provincias.nombre' ,'like','%' . $busqueda .'%')
            ->orwhere('poblaciones.postal', 'like','%' . $busqueda .'%')
            ->orderBy($sortBy, $orden)
            ->paginate(8);            
    }

}
 