<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Clubs;
use Carbon\CarbonInterval;

class Partidas extends Model
{
    protected $table = 'partidas';
    protected $fillable = [
       'clubs_id','pistas_id', 'user_id' ,'nivelPartida_id', 'inicio_alquiler', 'fin_alquiler','status', 'abierta_por', 'partida_fija', 'equipo', 'mensaje', 'cartel', 'observaciones'
    ];
    
    public $timestamps = false;
    
    public function users(){

        return $this->belongsToMany(User::class,'partidas', 'id','user_id');
    }

    public function club(){

        return $this->belongsToMany(Clubs::class, 'partidas', 'id','clubs_id');
    }
    public function pista(){

        return $this->belongsToMany(Pistas::class, 'partidas', 'id','pistas_id');
    }

    public function partidaFija(){  
        return $this->belongsToMany(PartidasFijas::class,'partidas_fijas', 'id','user_id');
    }

    public function partidaJugador(){
        return $this->belongsToMany(User::class, 'partidas_user', 'id', 'id')
            ->withPivot('user_id', 'pala', 'pala2', 'pala3', 'pala4');
            
    }

    public function datosJugador($id){

        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'provincias.id', '=', 'poblaciones.provincia_id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
            ->Where('users.id','=', $id)
            ->get();
            
       //Raw('CASE WHEN user_id='. $value .' THEN content_id ELSE user_id END AS friendID'))
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.email', '=', $jugador->email)
                ->get();
                $jugador->clubs = $clubs;
        };

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $jugador->id)
                ->get();
                $jugador->partidas = $partidas;
        };    

       

        return $jugadores;
    }

    public function avatarJugadores($nick, $club){

        $avatar = DB::table('users')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->select('users.avatar', 'users.posicion', 'niveles.nombre as nivel')
            //->select('users.avatar')
            ->Where('users.nick','=', $nick)
            ->where('users.clubs_id', '=', $club)
            ->get();
            
       

        return $avatar;
    }

    public function scopeBusquedaClubPartida($query,$id){
        $date = Carbon::today();

        $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start','partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 'pistas.nombre as pista', 'users.nick', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel')
                ->where('partidas.clubs_id', '=', $id)
                ->whereDate('inicio_alquiler', '=', $date)
                ->orderBy('inicio_alquiler', 'ASC')
                ->get();

                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };

                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{
                            array_push($avatares,  ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => '']); 
                        }
                        
                    }
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };

                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;
    }


    public function scopeBuscarPartida($query,$id){
        $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start','partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 'pistas.nombre as pista', 'users.nick', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel')
                ->where('partidas.id', '=', $id)
                ->get();

                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };
                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{
                            array_push($avatares,  ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => '']); 
                        }
                        
                    }
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };
                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;
    }


    public function scopeBusqueda($query,$busqueda,$sortBy,$orden,$semana,$id){ 
        CarbonInterval::setLocale('es');
        $vista = str_ireplace(" ", "%",substr($semana,10,6)); 
        $ano  = (int)substr($semana,0,4); //2018-03-19
        $mes  = (int)substr($semana,5,2);
        $dia  = (int)substr($semana,8,2);
        $date = Carbon::create($ano,$mes,$dia);
        $from = $date->format('Y-m-d'); 
        $to   = $date->addDays(7)->format('Y-m-d');
               
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);

        if($vista == 'agenda'){
            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('poblaciones', 'clubs.poblacion_id', '=', 'poblaciones.id')
                ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->select('partidas.id as partida_id', 'partidas.pistas_id as pistas_id', 'partidas.fin_alquiler', 'partidas.inicio_alquiler', 'partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.clubs_id', 'partidas.nivelPartida_id', 'clubs.nombre as club','clubs.poblacion_id as poblacion_id', 'pistas.nombre as pista', 'poblaciones.nombre as poblacion', 'poblaciones.provincia_id as provincias_id', 'provincias.nombre as provincia', 'users.nick', 'clubs.pistas as Npistas', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel')
                ->whereDate('inicio_alquiler', '=', $from)
                ->where(function($query) use ($busqueda, $id){
                    $id == null ? 
                        $query->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                              ->orWhere('pistas.nombre','like', $busqueda .'%')
                              ->orWhere('poblaciones.nombre','like','%' . $busqueda .'%')
                              ->orWhere('provincias.nombre','like','%' . $busqueda .'%')
                              ->orWhere('users.nick','like','%' . $busqueda .'%')
                        : $query->where('clubs.nombre' ,'like','%' . $busqueda .'%')
                                ->where(function($partida) use ($id){
                                    $partida->Where('partidas.clubs_id','=', $id)->orWhere('partidas.user_id','=', $id);
                                })
                                ->orWhere(function($subquery) use ($busqueda){
                                    $busqueda == null ?
                                        '' 
                                        : $subquery->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                                                    ->orWhere('poblaciones.nombre','like','%' . $busqueda .'%')
                                                    ->orWhere('provincias.nombre','like','%' . $busqueda .'%')
                                                    ->orWhere('users.nick','like','%' . $busqueda .'%');
                                });
                })
                ->orderBy($sortBy, $orden)
                ->paginate(8);

                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->partida_id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };

                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->partida_id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{
                            array_push($avatares,  ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => '']); 
                        }
                        
                    };
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };

                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;

        }else if($vista == 'semana'){

            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('poblaciones', 'clubs.poblacion_id', '=', 'poblaciones.id')
                ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->select('partidas.id as partida_id', 'partidas.pistas_id as pistas_id', 'partidas.fin_alquiler', 'partidas.inicio_alquiler', 'partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.clubs_id', 'partidas.nivelPartida_id', 'clubs.nombre as club','clubs.poblacion_id as poblacion_id', 'pistas.nombre as pista', 'poblaciones.nombre as poblacion', 'poblaciones.provincia_id as provincias_id', 'provincias.nombre as provincia', 'users.nick', 'clubs.pistas as Npistas', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel')
                ->whereBetween('inicio_alquiler', [$from, $to])
                ->where(function($query) use ($busqueda, $id){
                    $id == null ? 
                        $query->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                              ->orWhere('pistas.nombre','like', $busqueda .'%')
                              ->orWhere('poblaciones.nombre','like','%' . $busqueda .'%')
                              ->orWhere('provincias.nombre','like','%' . $busqueda .'%')
                              ->orWhere('users.nick','like','%' . $busqueda .'%')
                        : $query->where('clubs.nombre' ,'like','%' . $busqueda .'%')
                                ->where(function($partida) use ($id){
                                    $partida->Where('partidas.clubs_id','=', $id)->orWhere('partidas.user_id','=', $id);
                                })
                                ->orWhere(function($subquery) use ($busqueda){
                                    $busqueda == null ?
                                        '' 
                                        : $subquery->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                                                    ->orWhere('poblaciones.nombre','like','%' . $busqueda .'%')
                                                    ->orWhere('provincias.nombre','like','%' . $busqueda .'%')
                                                    ->orWhere('users.nick','like','%' . $busqueda .'%');
                                });
                })
                ->orderBy($sortBy, $orden)
                ->paginate(8);

                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->partida_id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };

                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->partida_id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{
                            array_push($avatares,  ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => '']); 
                        }
                        
                    };
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };

                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;


        }
        
    }
    //public function scopeBusquedaPartidaClub($query,$semana,$club_id){ 
    public function scopeBusquedaPartidaClub($query,$semana,$club_id, $user_id){ 
        CarbonInterval::setLocale('es');
        $vista = str_ireplace(" ", "%",substr($semana,10,6)); 
        $ano  = (int)substr($semana,0,4); //2018-03-19
        $mes  = (int)substr($semana,5,2);
        $dia  = (int)substr($semana,8,2);
        $date = Carbon::create($ano,$mes,$dia);
        $from = $date->format('Y-m-d'); 
        $to   = $date->addDays(7)->format('Y-m-d');
               
              
        if($vista == 'agenda'){
            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->select('partidas.id as id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 'pistas.nombre as pista', 'users.nick', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.observaciones')
                ->whereDate('inicio_alquiler', '=', $from)
                ->where('partidas.clubs_id', '=', $club_id)
                ->get();
                //meto los datos de la partida de cada partida del club
                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };
                

                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{

                            array_push($avatares,  $pala == '' ? ['avatar' => "", 'nivel' => '', 'posicion' => ''] : ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => '']); 
                        }
                        
                    }
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };
                
                //meto los datos de los jugadores de cada partida
                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;

        }else if($vista == 'semana'){

            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('users', 'partidas.user_id', '=', 'users.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.fin_alquiler', 'partidas.inicio_alquiler', 'partidas.user_id as user_id', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 'pistas.nombre as pista', 'users.nick', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.observaciones')
                //->whereBetween('inicio_alquiler', [$from, $to])
                //->where('partidas.clubs_id', '=', $club_id)
                //->where('partidas.user_id', '=', $user_id)
                ->where(function($query) use ($user_id, $club_id){
                    $user_id != 'club' ? 
                        $query->where('partidas.clubs_id', '=', $club_id)
                              ->where('partidas.user_id', '=', $user_id)
                        : $query->where('partidas.clubs_id', '=', $club_id);
                                
                })
                ->orderBy('start', 'ASC')
                ->get();

                foreach ($partidas as $partida){
                    $jugadoresPorPartida = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.*','users.nick')
                        ->where('partidas_user.partida_id', '=', $partida->partida_id)
                        ->get();
                        $partida->jugadoresPartida = $jugadoresPorPartida;
                };

                //cargo los avatares en la consulta
                foreach($partidas as $partida){
                    $avatares = [];
                    $palas = DB::table('partidas_user')
                        ->join('users','users.id', '=', 'partidas_user.user_id')
                        ->select('partidas_user.pala','partidas_user.pala2','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
                        ->where('partidas_user.partida_id', '=', $partida->id)
                        ->get();

                    foreach($palas[0] as $pala){
                        $avatar = $this->avatarJugadores($pala, $partida->clubs_id);
                        if(sizeof($avatar)){
                            $avatar = json_decode(json_encode($avatar),true);
                            if($avatar[0]['avatar'] == null){
                                array_push($avatares, ['avatar' => "../img/avatar/avatarH.png", 'nivel' => $avatar[0]['nivel'], 'posicion' => $avatar[0]['posicion'] ]); 
                            }else{
                                array_push($avatares, $avatar[0] );
                            }
                            
                        }else{
                            array_push($avatares,  ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '#', 'posicion' => '']); 
                        }
                        
                    }
                    $partida->avatares = $avatares;
                    //$partida->avatares = array_flatten($avatares);
                };

                foreach ($partidas as $partida){
                    $datosJugador = $this->datosJugador($partida->user_id);
                        $partida->datosJugador = $datosJugador;
                };

                return $partidas;

        }
    }

}
