<?php

namespace App\Http\Controllers\Club;

use Auth;
use App\Clases;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index()
    {
        $club = Auth::user();

        $clases = Clases::where('club_id', '=', $club->id)->get();

        return response()->json($clases);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $club = Auth::user();

        $create = Clases::create([
                'club_id'       => $club->id,
                'clases'        => $request->get('tabla'),
              ]);
       return response()->json(['status' => 'success','msg'=> 'Tabla creada con éxito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clases  $clases
     * @return \Illuminate\Http\Response
     */
    public function show(Clases $clases)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clases  $clases
     * @return \Illuminate\Http\Response
     */
    public function edit(Clases $clases)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clases  $clases
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clases $clases)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clases  $clases
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clases $clases)
    {
        //
    }
}
