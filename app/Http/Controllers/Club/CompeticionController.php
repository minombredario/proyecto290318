<?php

namespace App\Http\Controllers\Club;

use App\Competiciones;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Competiciones  $competiciones
     * @return \Illuminate\Http\Response
     */
    public function show(Competiciones $competiciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Competiciones  $competiciones
     * @return \Illuminate\Http\Response
     */
    public function edit(Competiciones $competiciones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Competiciones  $competiciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competiciones $competiciones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Competiciones  $competiciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Competiciones $competiciones)
    {
        //
    }
}
