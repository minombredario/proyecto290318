<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Auth;
use App\Clubs;
use App\Partidas;
use App\Users;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index(){
    	$id_club = 1;
    	$club = Clubs::mostrar($id_club);
    	
    	$partidas = Partidas::BusquedaClubPartida($id_club);
    	
    	Config::set('app.name', $club['club'][0]->nombre);

        return view('index', compact('partidas', 'club'));
       
    }
}
