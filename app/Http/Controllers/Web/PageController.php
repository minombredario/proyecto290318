<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
//crear tabla en base de datos
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Database\Migrations\Migration;

use App\User;
use App\Clubs;
use App\Instalaciones;
use App\Partidas;
use App\Provincias;
use App\Niveles;
use App\Noticias;
use App\Poblaciones;
use App\Columnas;
use App\Paralax;
use App\Jumbotron;
use App\Social;
use App\Slider;
use App\Welcome;
use App\Patrocinadores;
use Carbon\Carbon;


use Faker\Factory as Faker;


class PageController extends Controller
{
    public function index(){
        return view('web.home');
    }

    public function registerUser(Request $request){

      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'nullable',
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,' . $request->get('nick'),
        'nivel_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6|confirmed',
        ]);

      if($validar){
        
        $create = User::create($request->except('avatar'));
        $user = User::find($create->id);
        $user->update([
          'password' => bcrypt($request->get('password'))
        ]);

        $user->clubs()->attach([
          $request->get('clubs_id') => [
              'email' => $request->get('email'),//asigno al id del club el email del usuario inertandolo a traves de su id.
              'password' => bcrypt($request->get('password')),
            ]
          ]);

        if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(150,150, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename;
                $user->update([
                   'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
                 ]); 

              }else{
                $avatar = $request->get('sexo') == "Hombre"? 'http://127.0.0.1:8000/img/avatar/avatarH.png' : 'http://127.0.0.1:8000/img/avatar/avatarM.png';
                $user->update([
                  'avatar' => $avatar,
                ]);

              }


              return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);
            }


    }

    public function getJumbotron($club_id){

        $jumbotron = Jumbotron::where([ ['club_id', '=', $club_id], ['visible', '=', 1] ])->get();

        return response()->json($jumbotron);

    }

     public function getWelcome($club_id){
       
        $welcome = Welcome::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($welcome);
    }

    public function getSocial($club_id){
       
        $social = Social::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($social);
    }

    public function getColumnas($club_id){
        
        $columnas = Columnas::where('club_id', '=', $club_id)->where('visible', '=', 1)->get();

        return response()->json($columnas);
    }

    public function getPartidas(Request $request){

          $semana = $request->get('semana');
          $club_id = $request->get('club_id');
          $user_id = 'club';
                   
          $partidas = Partidas::BusquedaPartidaClub($semana,$club_id,$user_id);

          return response()->json($partidas);
    }

    public function getNoticias($club_id){
      
      $noticias = Noticias::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->orderBy('id', 'DESC')->take(3)->get();
            
      return response()->json($noticias);
    }

    public function indexNoticias(Request $request){
      
      $busqueda = $request->get('busqueda');
      $id = $request->get('id');
      $sortBy = $request->get('sortBy') == ''? 'id': $request->get('sortBy');
      $orden =  $request->get('orden') == ''? 'DESC': $request->get('orden'); 
      
      if($busqueda != '' || $busqueda != null){
         $noticias = Noticias::where([ ['club_id', '=', $id], ['titulo', 'like', '%'. $busqueda .'%'],['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])
            ->orWhere([ ['club_id', '=', $id], ['contenido', 'like', '%'. $busqueda .'%'],['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])
            ->orderBy($sortBy, $orden)
            ->paginate(3);
      }else{
         $noticias = Noticias::where([ ['club_id', '=', $id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->orderBy($sortBy, $orden)->paginate(3);
      }
      

      $response = [
        'pagination' => [
          'total' => $noticias->total(),
          'per_page' => $noticias->perPage(),
          'current_page' => $noticias->currentPage(),
          'last_page' => $noticias->lastPage(),
          'from' => $noticias->firstItem(),
          'to' => $noticias->lastItem()
        ],

        'items' => $noticias,                    
      ];   

      return response()->json($response);


    }

    public function getSlider($club_id){
       
        $slider = Slider::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($slider);
    }

    public function getGalerias($club_id){
        
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club_id . '/galerias';
        $carpetas = [];
        
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($path);
        
        // Recorre todos los elementos del directorio
      while (($archivo = readdir($gestor)) !== false)  {
        if ($archivo != "." && $archivo != "..") {
          $carpeta = ['nombre' => $archivo];
            array_push($carpetas, $carpeta);
          }
        };
        return response()->json($carpetas); 
    }

    public function getPatrocinadores($club_id){
        
        $patrocinadores = Patrocinadores::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($patrocinadores);
    }

    public function galeria($galeria){
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image\club\\' .'1\galerias\\' . $galeria;
        $pathImag = '..\image\club\\' . '1\galerias\\' . $galeria;
        $imagenes = [];

        // Abre un gestor de directorios para la ruta indicada
        $gestor = @opendir($path);
        
        // Recorre todos los elementos del directorio
      while (($archivo = readdir($gestor)) !== false)  {
        if ($archivo != "." && $archivo != "..") {
          array_push($imagenes, $pathImag . "\\" . $archivo);
          }
        };
        return response()->json($imagenes); 
    }

    
}
