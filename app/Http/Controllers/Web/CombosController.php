<?php
 
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Provincias;
use App\Poblaciones;
use App\Instalaciones;
use App\Niveles;
use App\User;
use App\Clubs;
use App\Partidas;
use Image;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class CombosController extends Controller
{
    public function comboProvincias()
    {
        $provincias = Provincias::orderBy('nombre','ASC')->get();
        
        return response()->json($provincias);
    }
    /*public function estadisticas(){
        $partidas = Partidas::get();
        $Nclubs = Clubs::get()->count();
        $Nsocios = User::get()->count();
        $Npartidas = Partidas::get()->count();
        //SELECT SUBSTRING(inicio_alquiler,11,19) as hotPartida, count(SUBSTRING(inicio_alquiler,11,19)) as topHora FROM `partidas` group By hotPartida ORDER BY `hotPartida` DESC
        $top5hora = DB::table('partidas')
            ->groupBy('label')
            ->selectRaw('SUBSTRING(inicio_alquiler,12,19) as label, count(SUBSTRING(inicio_alquiler,11,19)) as value')
            ->orderBy('value', 'DESC')
            ->limit(5)
            ->get();
        $top5clubs = DB::table('partidas')
                    ->join('clubs', 'clubs.id', '=', 'partidas.clubs_id')
                    ->groupBy('clubs_id', 'label')
                    ->selectRaw('clubs.nombre as label, clubs_id, count(clubs_id) as value')
                    ->orderBy('value', 'DESC')
                    ->limit(5)
                    ->get();
        $top5jugadores = DB::table('partidas')
                    ->join('users', 'users.id', '=', 'partidas.user_id')
                    ->selectRaw('users.nick as label, user_id, count(user_id) as value')
                    ->groupBy('user_id', 'label')
                    ->orderBy('value', 'DESC')
                    ->limit(5)
                    ->get();

        $response = [
            'Nclubs'        => $Nclubs,
            'Nsocio'        => $Nsocios,
            'Npartidas'     => $partidas->count(),
            'top5hora'      => $top5hora,
            'top5jugadores' => $top5jugadores,
            'top5clubs'     => $top5clubs,
        ];


        return response()->json($response);
    }*/

    public function estadisticas(Request $request){
        
        $id = $request->get('club_id');
        $partidas = ($id != 'null' && $id != '') ? Partidas::where('clubs_id', '=', $id)->get() : Partidas::get();
        $Nclubs = Clubs::get()->count();
        $Nsocios = ($id != 'null' && $id != '') ? User::where('clubs_id', '=', $id)->get()->count() : User::get()->count();
        $Npartidas = $partidas->count();
        //SELECT SUBSTRING(inicio_alquiler,11,19) as hotPartida, count(SUBSTRING(inicio_alquiler,11,19)) as topHora FROM `partidas` group By hotPartida ORDER BY `hotPartida` DESC
        
        $top5hora = ($id != 'null' && $id != '') ? DB::table('partidas')
                            ->groupBy('label')
                            ->selectRaw('SUBSTRING(inicio_alquiler,12,19) as label, count(SUBSTRING(inicio_alquiler,11,19)) as value')
                            ->where('clubs_id', '=', $id)
                            ->orderBy('value', 'DESC')
                            ->limit(5)
                            ->get():
                            DB::table('partidas')
                            ->groupBy('label')
                            ->selectRaw('SUBSTRING(inicio_alquiler,12,19) as label, count(SUBSTRING(inicio_alquiler,11,19)) as value')
                            ->orderBy('value', 'DESC')
                            ->limit(5)
                            ->get();

         $top5clubs = ($id != 'null' && $id != '') ? [] : DB::table('partidas')
                    ->join('clubs', 'clubs.id', '=', 'partidas.clubs_id')
                    ->groupBy('clubs_id', 'label')
                    ->selectRaw('clubs.nombre as label, clubs_id, count(clubs_id) as value')
                    ->orderBy('value', 'DESC')
                    ->limit(5)
                    ->get();

        $top5jugadores = ($id != 'null' && $id != '') ? DB::table('partidas')
                    ->join('users', 'users.id', '=', 'partidas.user_id')
                    ->selectRaw('users.nick as label, user_id, count(user_id) as value')
                    ->where('partidas.clubs_id', '=', $id)
                    ->groupBy('user_id', 'label')
                    ->orderBy('value', 'DESC')
                    ->limit(5)
                    ->get()
                    : DB::table('partidas')
                    ->join('users', 'users.id', '=', 'partidas.user_id')
                    ->selectRaw('users.nick as label, user_id, count(user_id) as value')
                    ->groupBy('user_id', 'label')
                    ->orderBy('value', 'DESC')
                    ->limit(5)
                    ->get();

         $response = [
            'Nclubs'        => $Nclubs,
            'Nsocio'        => $Nsocios,
            'Npartidas'     => $partidas->count(),
            'top5hora'      => $top5hora,
            'top5jugadores' => $top5jugadores,
            'top5clubs'     => $top5clubs,
        ];


        return response()->json($response);
    }

    public function comboInstalaciones(){
        $instalaciones = Instalaciones::orderBy('nombre','ASC')->get();

        return response()->json($instalaciones);
    }

    public function comboNiveles(){
        $niveles = Niveles::orderBy('id','ASC')->get();

        return response()->json($niveles);
    }
    public function comboClubs(){
        $clubs = Clubs::orderBy('nombre','ASC')->get();

        return response()->json($clubs);
    }

    public function poblacionProvincia(Request $request)
    {
        $busqueda = $request->get('busqueda');
    
        $poblaciones = Poblaciones::with('provincia')->where('postal','=',$busqueda)->orderBy('nombre','ASC')->get();
        
        return response()->json($poblaciones);
    }

    public function pistasDisponibles(Request $request){
        //YYYY-MM-DD
        CarbonInterval::setLocale('es');
        $carbon = Carbon::now('Europe/London');

        $hora = $carbon->addHour(1)->second(0)->toTimeString();
        $hoy = $carbon->toDateString();
        
        $club_id = $request->get('club');
        $year = substr($request->get('fecha'),6,10);
        $mes = substr($request->get('fecha'), 3,2);
        $dia = substr($request->get('fecha'),0,2);
        $fecha = $year . '-' . $mes . '-' . $dia;

        //si la fecha de entrada es la misma que el dia actual busco las partidas que hay entre la hora actual de la consulta y el final del dia
        //si la fecha es distinta, busco las partidas del dia completo.
        $fechaFrom = $hoy == $fecha ? ($fecha . ' ' . $hora) : ($fecha . ' 00:00:00');
        $fechaTo = $fecha . ' 22:30:00';
            $pistas = Partidas::with('club')->where('clubs_id', '=', $club_id)->whereBetween('inicio_alquiler',array($fechaFrom, $fechaTo))->get();
        if(!$pistas->count()){
            $pistas = Partidas::with('club')->where('clubs_id', '=', $club_id)->where('inicio_alquiler','like', $fecha . '%')->get();
        }

        
        return response()->json($pistas);
 

        
    }

    public function clubUser(Request $request){
        
        $user = $request->get('userEmail');
        $club = $request->get('clubId');
       

        $jugadores = DB::table('users')
                    ->join('poblaciones', 'poblaciones.id', '=','users.poblacion_id')
                    ->join('niveles', 'niveles.id', '=', 'users.nivel_id')
                    ->join('clubs','clubs.id', '=', 'users.clubs_id')
                    ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id', 'poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
                    ->where('users.email','=', $user)
                    ->where('users.clubs_id', '=', $club)
                    ->get();    
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.user_email', '=', $user)
                ->get();
                $jugador->clubs = $clubs;
        }; 

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $user)
                ->get();
                $jugador->partidas = $partidas;
        };              
        
        return response()->json($jugadores);

    }

    public function upload(Request $request)
    {
    	return $request;
        if($request->hasFile('image')){
            $file = $request->file('image'); //recupero la imagen

            $filename = $file->getClientOriginalName();
           
            $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
            $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                $constraint->aspectRatio();                 
            });
            $img->stream(); //convierto la nueva imagen

            $path = Storage::disk('public')->put('image'. "/" . $filename, $img); // almaceno la ruta de la imagen en una variable 

            //$post->fill(['file' => substr(asset($path), 0, -1) . "image/" . $filename])->save(); //guardo la ruta en la base de datos
            return response()->json(['status' => 'success','msg'=> 'Imágen subida con éxito']);
        }else{
        	 return response()->json(['status' => 'error','msg'=> 'No se ha podido subir la imágen']);
        };
    }

}
