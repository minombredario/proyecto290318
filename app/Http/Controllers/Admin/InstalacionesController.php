<?php
 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Instalaciones;

class InstalacionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        
        $instalaciones = Instalaciones::where('nombre','like', '%'.$busqueda.'%')->orderBy($sortBy, $orden)->paginate(8);
        
        
        $response = [
            'pagination' => [
                'total' => $instalaciones->total(),
                'per_page' => $instalaciones->perPage(),
                'current_page' => $instalaciones->currentPage(),
                'last_page' => $instalaciones->lastPage(),
                'from' => $instalaciones->firstItem(),
                'to' => $instalaciones->lastItem()
            ],
            
            'items' => $instalaciones,
            
        ];   

        return response()->json($response);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);
        try {
            $create = Instalaciones::create($request->all());
            $response = ['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito'];
        } catch (QueryException $e) {
            if(strpos($e, "Duplicate")){
                $response = ['status' => 'error','msg'=> 'Intalación ' . $request->get('nombre') . ' registro duplicado'];
            }
        };
        return response()->json($response);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Instalaciones::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);

      $instalacion = Instalaciones::find($id);
      if($instalacion->count()){
        $instalacion->update($request->all());
        return response()->json(['status'=>'success','msg'=> $instalacion['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $instalacion['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $instalacion = Instalaciones::find($id);
        if($instalacion->count()){
          $instalacion->delete();
          return response()->json(['status'=>'success','msg'=> $instalacion['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $instalacion['nombre'] ]);
        }
    }
}


