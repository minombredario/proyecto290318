<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Auth;
use App\Clubs;
use App\Poblaciones;
use App\Provincias;
use App\Utilities\GoogleMaps;
use Image;

class ClubsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');
        
        $clubs = Clubs::busqueda($busqueda,$sortBy,$orden);

        $response = [
            'pagination' => [
                'total' => $clubs->total(),
                'per_page' => $clubs->perPage(),
                'current_page' => $clubs->currentPage(),
                'last_page' => $clubs->lastPage(),
                'from' => $clubs->firstItem(),
                'to' => $clubs->lastItem()
            ],
            
            'items' => $clubs,
            
        ];   

        return response()->json($response);

       
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
      $validar = $this->validate($request, [
                  'nombre' => 'required|unique:clubs,nombre,email,web',
                  'slug' => 'required',
                  'cif' => 'required|size:9',
                  'telefono' => 'nullable',
                  'movil' => 'required|integer|max:999999999',
                  'email' => 'required|string|email|max:255|unique:clubs,nombre,email,web',
                  'password' => 'required|string|min:6|confirmed',
                  'responsable' => 'required',
                  'logo' => 'nullable',
                  'pistas' => 'required|integer|min:1',
                  'horario' => 'nullable',
                  'web' => 'required||unique:clubs,nombre,email,web',
                  'direccion' => 'required',
                  'poblacion_id' => 'required|numeric',
                  'latitud' => 'numeric|nullable',
                  'longitud' => 'numeric|nullable',
                ]); 
      if($validar){
        $poblacion = Poblaciones::where('id', '=', $request->get('poblacion_id'))->first();
        $provincia = Provincias::where('id','=',$poblacion->provincia->id)->first();
        $coordenadas = GoogleMaps::geocodeAddress( $request->get('direccion'),  $poblacion->nombre, $provincia->nombre, $poblacion->postal );

        $telefono = ($request->get('telefono') == 'null' || $request->get('telefono') == '') ? null: $request->get('telefono');
      
        $create = Clubs::create([
                  'nombre' => $request->get('nombre'),
                  'slug' => $request->get('slug'),
                  'cif' => $request->get('cif'),
                  'telefono' => $telefono,
                  'movil' => $request->get('movil'),
                  'email' => $request->get('email'),
                  'logo' => $request->get('logo'),
                  'password' => bcrypt($request->get('password')),
                  'responsable' => $request->get('responsable'),
                  'pistas' => $request->get('pistas'),
                  'horario' => $request->get('horario'),
                  'web' => $request->get('web'),
                  'direccion' => $request->get('direccion'),
                  'poblacion_id' => $request->get('poblacion_id'),
                  'remember_token' => $request->get('remember_token'),
                  'latitud' => $coordenadas['lat'],//$request->get('latitud') == null? 0.00: $request->get('latitud'),
                  'longitud' => $coordenadas['lng'],//$request->get('longitud') == null? 0.00: $request->get('longitud'),
              ]);
          $instalaciones = explode(",", $request->get('instalaciones'));
          $create->instalaciones()->attach($instalaciones);
          
          if($request->hasFile('logo')){
              $club = Clubs::find($create->id); 
              $file = $request->file('logo'); //recupero la imagen

              $filename = $file->getClientOriginalName();

              $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
              $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
              });
              $img->stream(); //convierto la nueva imagen

              $path = Storage::disk('public')->put('image'. '/club/' . $create->id . '/logo/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
              $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $create->id . '/logo/' . $filename;
              $club->update([
                'logo' => $rutaImagen, //guardo la ruta en la base de datos 
              ]); 

           }
          return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        

        $club = Clubs::mostrar($id);
        
        /*
        $instalaciones = [];
        $club_instalaciones =  Clubs::with('instalaciones')->find($id);
        foreach ($club_instalaciones->instalaciones as $instalacion) {
            array_push ($instalaciones, $instalacion);
        }   

    
        $response = [
            'club' => $club,
            'instalaciones' => $instalaciones,
        ];
        */
        
       
        return response()->json($club);
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      $club = Clubs::find($id); 
       
      $validar = $this->validate($request, [
          'nombre' => 'required|unique:clubs,nombre,'. $id,
          'slug' => 'required',
          'cif' => 'required|size:9',
          'telefono' => 'nullable',
          'movil' => 'required|integer|max:999999999',
          'email' => 'required|string|email|max:255|unique:clubs,email,'. $id,
          'responsable' => 'required',
          'pistas' => 'required|integer|min:1',
          'horario' => 'nullable|string',
          'web' => 'required|unique:clubs,web,'. $id,
          'direccion' => 'required',
          'poblacion_id' => 'required|numeric',
        ]);
        
        
        if($club->count() && $validar){
            $poblacion = Poblaciones::where('id', '=', $request->get('poblacion_id'))->first();
            $provincia = Provincias::where('id','=',$poblacion->provincia->id)->first();
            $coordenadas = GoogleMaps::geocodeAddress( $request->get('direccion'),  $poblacion->nombre, $provincia->nombre, $poblacion->postal );
              
            if(!is_array($request->get('instalaciones'))){
              $instalaciones = explode(",", $request->get('instalaciones'));
              $club->instalaciones()->sync($instalaciones);
            }
            
            $club->update($request->except(['logo', 'password','latitud','longitud']));
            $club->update(['latitud' => $coordenadas['lat'],'longitud' => $coordenadas['lng']]);
            //si el campo del password viene relleno, lo validamos y actualizamos datos
            if($request->get('password') != 'null' && $request->get('password') != ''){
              $validar = $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
              ]);

              $validar? $club->update([ 'password' => bcrypt($request->get('password'))]) : '';

            }

            if($request->hasFile('logo')){
              $file = $request->file('logo'); //recupero la imagen

              $filename = $file->getClientOriginalName();

              $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
              $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
              });
              $img->stream(); //convierto la nueva imagen

              $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/logo/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
              $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/logo/' . $filename;
              $club->update(['logo' => $rutaImagen]); //guardo la ruta en la base de datos 
            }
          return response()->json(['status'=>'success','msg'=> $club['nombre'] . ' actualizada con éxito']);
        }else {
            return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $club['nombre'] ]);
      }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $club = Clubs::find($id);
        if($club->count()){
          $club->delete();
          return response()->json(['status'=>'success','msg'=> $club['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $club['nombre'] ]);
        }
    }
    public function crearTablaClub($nombre){

        Schema::create($nombre, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();;
            
            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id']);
            $table->unique(['club_id']);
        });
    }
}

