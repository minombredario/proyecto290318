<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Niveles;
use Image;

class JugadoresController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
     
    public function index(Request $request)
    {

      $busqueda = $request->get('busqueda');
      $club = ($request->get('club') == 'undefined' || $request->get('club') == '' || $request->get('club') == 'null') ? '': $request->get('club');
      $nivel = ($request->get('nivel') == 'undefined' || $request->get('nivel') == '' || $request->get('nivel') == 'null') ? '': $request->get('nivel');
      $posicion = ($request->get('posicion') == 'undefined' || $request->get('posicion') == '' || $request->get('posicion') == 'null') ? '': $request->get('posicion');
      $dispon = ($request->get('dispon') == 'undefined' || $request->get('dispon') == '' || $request->get('dispon') == 'null') ? '' : $request->get('dispon');
      $poblacion = ($request->get('poblacion') == 'undefined' || $request->get('poblacion') == '' || $request->get('poblacion') == 'null') ? '': $request->get('poblacion');
      $provincia = ($request->get('provincia') == 'undefined' || $request->get('provincia') == '' || $request->get('provincia') == 'null') ? '': $request->get('provincia');
      $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
      $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');
      
      if($busqueda != '' || $busqueda != null){
        $jugadores = User::busqueda($busqueda,$sortBy,$orden);
      }else{
        $jugadores = User::busquedaExtendida($club,$nivel,$posicion,$dispon,$poblacion,$provincia,$sortBy,$orden);
      }
      

      $response = [
        'pagination' => [
          'total' => $jugadores->total(),
          'per_page' => $jugadores->perPage(),
          'current_page' => $jugadores->currentPage(),
          'last_page' => $jugadores->lastPage(),
          'from' => $jugadores->firstItem(),
          'to' => $jugadores->lastItem()
        ],

        'items' => $jugadores,
        'prueba' => $poblacion,                    
      ];   

      return response()->json($response);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'nullable',
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,' . $request->get('nick'),
        'nivel_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6|confirmed',
        ]);

      if($validar){
        
        $create = User::create($request->except('avatar'));
        $user = User::find($create->id);
        $user->update([
          'password' => bcrypt($request->get('password'))
        ]);

        $user->clubs()->attach([
          $request->get('clubs_id') => [
              'email' => $request->get('email'),//asigno al id del club el email del usuario inertandolo a traves de su id.
              'password' => bcrypt($request->get('password')),
            ]
          ]);

        if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(150,150, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename;
                $user->update([
                   'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
                 ]); 

              }else{
                $avatar = $request->get('sexo') == "Hombre"? 'http://127.0.0.1:8000/img/avatar/avatarH.png' : 'http://127.0.0.1:8000/img/avatar/avatarM.png';
                $user->update([
                  'avatar' => $avatar,
                ]);

              }


              return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);
            }


          }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {

      $jugador = User::find($id);

      return response()->json($jugador);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      
      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $id, //compruebo que el nick no esta registrado ya en ese club.
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'nullable',
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,1',
        'nivel_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255',
      ]);

      $jugador = User::find($id);

      if($jugador->count() && $validar){

        $jugador->update($request->except(['avatar', 'password']));

          //si el campo del password viene relleno, lo validamos y actualizamos datos
        if($request->get('password') != 'null' && $request->get('password') != ''){
          $validar = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
          ]);

          $validar? $jugador->update(['password' => bcrypt($request->get('password'))]) : '';

        }

        $jugador->clubs()->sync([
          $request->get('clubs_id') => [
            'email' => $request->get('email'), //asigno al id del club el email del usuario inertandolo a traves de su id.
          ]
        ]);

        if($request->hasFile('avatar')){

          $file = $request->file('avatar'); //recupero la imagen

          $filename = $file->getClientOriginalName();

          $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
          $img->resize(150,150, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
            $constraint->aspectRatio();                 
          });
          $img->stream(); //convierto la nueva imagen
          
          $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
          $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename;
          $jugador->update([
             'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
           ]); 

        }

        return response()->json(['status'=>'success','msg'=> $jugador['nombre'] . ' actualizada con éxito']);
      }else{
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $jugador['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
      $jugador = User::find($id);
      if($jugador->count()){
        $jugador->delete();
        return response()->json(['status'=>'success','msg'=> $jugador['nombre'] . ' eliminada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $jugador['nombre'] ]);
      }
    }
  }


