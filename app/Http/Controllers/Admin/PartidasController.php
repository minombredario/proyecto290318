<?php
 
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Partidas;

class PartidasController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index(Request $request)
    {
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');
        $semana = $request->get('semana');
        $id = $request->get('id') == null? '': $request->get('id');

        $partidas = Partidas::busqueda($busqueda,$sortBy,$orden,$semana,$id); 
       
        $response = [
            'pagination' => [
                'total' => $partidas->total(),
                'per_page' => $partidas->perPage(),
                'current_page' => $partidas->currentPage(),
                'last_page' => $partidas->lastPage(),
                'from' => $partidas->firstItem(),
                'to' => $partidas->lastItem()
            ],
            
            'items' => $partidas,
            
        ];   

        return response()->json($response);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
        //'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        $validar = $this->validate($request, [
          'clubs_id'        => 'required|integer|unique:partidas,clubs_id,null,id,pistas_id,'. $request->get('pistas_id') . ',inicio_alquiler,' . $request->get('inicio_alquiler'), 
          'user_id'         => 'required|integer',
          'pistas_id'       => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'. $request->get('clubs_id') . ',inicio_alquiler,' . $request->get('inicio_alquiler'),
          'nivelPartida_id' => 'required|integer',
          'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'. $request->get('clubs_id') . ',pistas_id,' . $request->get('pistas_id'),
          'fin_alquiler'    => 'required|date',
          'status'          => 'required|in:ABIERTA,CERRADA',
          'abierta_por'     => 'required|in:CLUB,USUARIO,INVITADO',
          'partida_fija'    => 'required|in:SI,NO',
          'equipo'    => 'required|in:SI,NO'

        ]);

        $jugadoresPartida = $request->get('jugadoresPartida');
        /*$contJug = 0;
        return response()->json($jugadoresPartida);
        foreach ($jugadoresPartida as $clave=>$value){
            if($value != '' && $value != 'null' && $value != null){
                $contJug++;
            };
        };*/

        if($validar){
            $create = Partidas::create($request->all());
            $partida = Partidas::find($create->id);
            //$user =  User::find($response->get('user_id'))
            $partida->partidaJugador()->sync([
                $partida->id => [
                    'partida_id' => $partida->id,
                    'user_id'    => $request->get('user_id'),
                    'pala'       => $jugadoresPartida['pala'],
                    'pala2'      => $jugadoresPartida['pala2'],
                    'pala3'      => $jugadoresPartida['pala3'],
                    'pala4'      => $jugadoresPartida['pala4'],
                ]
        
            ]);   


        }    
        return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
       
        $partida = Partidas::with('instalaciones')->find($id);
        $poblacion = Poblaciones::with('provincia')->find($partida['poblacion_id']);
        
        $response = [
            'poblacion' => $poblacion,
            'partida' => $partida
        ];

        return response()->json($response);
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    { //unique:users,clubs_id,null,id,nick,1',
  
        $validar = $this->validate($request, [
              'clubs_id'        => 'required|integer|unique:partidas,clubs_id,null,id,pistas_id,'. $id . ',inicio_alquiler,' . $id, 
              'pistas_id'       => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'. $id . ',inicio_alquiler,' . $id, 
              'user_id'         => 'required|integer',
              'nivelPartida_id' => 'required|integer',
              'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'. $id . ',pistas_id,' . $id,
              'fin_alquiler'    => 'required|date',
              'status'          => 'required|in:ABIERTA,CERRADA',
              'abierta_por'     => 'required|in:CLUB,USUARIO,INVITADO',
              'partida_fija'    => 'required|in:SI,NO',
              'equipo'    => 'required|in:SI,NO'
            ]);
        $jugadoresPartida = $request->get('jugadoresPartida');
        
        $partida = Partidas::find($id);

        if($partida->count() && $validar){

            $partida->update($request->all());

            $partida->partidaJugador()->sync([
                $partida->id => [
                    'user_id'    => $jugadoresPartida[0]['user_id'],
                    'pala'       => $jugadoresPartida[0]['pala'],
                    'pala2'      => $jugadoresPartida[0]['pala2'],
                    'pala3'      => $jugadoresPartida[0]['pala3'],
                    'pala4'      => $jugadoresPartida[0]['pala4'],
                ]
        
            ]);   
            return response()->json(['status'=>'success','msg'=> $partida['inicio_alquiler'] . ' actualizada con éxito']);
        } else {
            return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $partida['inicio_alquiler'] ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $partida = Partidas::find($id);
        if($partida->count()){
          $partida->delete();
          return response()->json(['status'=>'success','msg'=> $partida['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $partida['nombre'] ]);
        }
    }
}


