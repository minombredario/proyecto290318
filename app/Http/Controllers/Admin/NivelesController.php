<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

use App\Niveles;

class NivelesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        
        $niveles = Niveles::where('nombre','like', '%'.$busqueda.'%')->orderBy($sortBy, $orden)->paginate(8);
        
        $response = [
            'pagination' => [
                'total' => $niveles->total(),
                'per_page' => $niveles->perPage(),
                'current_page' => $niveles->currentPage(),
                'last_page' => $niveles->lastPage(),
                'from' => $niveles->firstItem(),
                'to' => $niveles->lastItem()
            ],
            
            'items' => $niveles,
            
        ];   

        return response()->json($response);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nombre' => 'required',
            'descripcion' => 'nullable',
        ]);

        try {
            $create = Niveles::create($request->all());
            $response = ['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito'];
        } catch (QueryException $e) {
            if(strpos($e, "Duplicate")){
                $response = ['status' => 'error','msg'=> 'Nivel ' . $request->get('nombre') . ' registro duplicado'];
            }
        };

        
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Niveles::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'nombre' => 'required',
        'descripcion' => 'nullable',
      ]);

      $nivel = Niveles::find($id);
      if($nivel->count()){
        $nivel->update($request->all());
        return response()->json(['status'=>'success','msg'=> $nivel['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $nivel['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $nivel = Niveles::find($id);
        if($nivel->count()){
          $nivel->delete();
          return response()->json(['status'=>'success','msg'=> $nivel['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $nivel['nombre'] ]);
        }
    }
}
