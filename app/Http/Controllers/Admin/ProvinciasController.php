<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

use App\Provincias;

class ProvinciasController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        
        $provincias = Provincias::where('nombre','like', '%'.$busqueda.'%')->orderBy($sortBy, $orden)->paginate(8);
                
        $response = [
            'pagination' => [
                'total' => $provincias->total(),
                'per_page' => $provincias->perPage(),
                'current_page' => $provincias->currentPage(),
                'last_page' => $provincias->lastPage(),
                'from' => $provincias->firstItem(),
                'to' => $provincias->lastItem()
            ],
            
            'items' => $provincias,
            
        ];   

        return response()->json($response);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $validar = $this->validate($request, [
          'nombre' => 'required|unique:provincias,nombre',
          'slug' => 'required|unique:provincias,slug',
          'latitud' => 'numeric|nullable',
          'longitud' => 'numeric|nullable',
        ]);

        if($validar){
            $create = Provincias::create($request->all());
            $response =['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito'];
        }
            
        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Provincias::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $validar = $this->validate($request, [
          'nombre' => 'required|unique:provincias,nombre,' . $id,
          'slug' => 'required|unique:provincias,slug,' . $id,
          'latitud' => 'numeric|nullable',
          'longitud' => 'numeric|nullable',
        ]);

      $provincia = Provincias::find($id);
      if($provincia->count() && $validar){
        $provincia->update($request->all());
        return response()->json(['status'=>'success','msg'=> $provincia['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $provincia['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $provincia = Provincias::find($id);
        if($provincia->count()){
          $provincia->delete();
          return response()->json(['status'=>'success','msg'=> $provincia['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $provincia['nombre'] ]);
        }
    }
}

