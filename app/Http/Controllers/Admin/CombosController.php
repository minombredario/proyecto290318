<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Provincias;

class CombosController extends Controller
{
    public function cmbprvs()
    {
        $provincias = Provincias::orderBy('nombre','ASC')->get();
        
        return response()->json($provincias);
    }

}
