<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

use App\Poblaciones;


class PoblacionesController extends Controller
{
	  
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index(Request $request)
    {
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        $poblaciones = Poblaciones::busqueda($busqueda,$sortBy,$orden);
        
        $response = [
            'pagination' => [
                'total' => $poblaciones->total(),
                'per_page' => $poblaciones->perPage(),
                'current_page' => $poblaciones->currentPage(),
                'last_page' => $poblaciones->lastPage(),
                'from' => $poblaciones->firstItem(),
                'to' => $poblaciones->lastItem()
            ],
            
            'items' => $poblaciones,
            
        ];     
        
        return response()->json($response);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
         
        $validar = $this->validate($request, [
          'postal' => 'required|integer|min:1000|max:52080|unique:poblaciones,postal,null,id,nombre,' . $request->get('nombre'), 
          'nombre' => 'required|unique:poblaciones,nombre,null,id,postal,' . $request->get('postal'),
          'slug' => 'required',
          'latitud' => 'numeric|nullable',
          'longitud' => 'numeric|nullable',
        ]);
       if($validar){
          $create = Poblaciones::create($request->all());
          $response = ['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito'];
        }

       return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Poblaciones::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
        $validar = $this->validate($request, [
              'postal'    => 'required|integer|min:1000|max:52080|unique:poblaciones,postal,null,id,nombre,' . $id, 
              'nombre'    => 'required|unique:poblaciones,nombre,null,id,postal,' . $id,
              'slug'      => 'required',
              'latitud'   => 'numeric|nullable',
              'longitud'  => 'numeric|nullable',
              
          ]);

      $poblacion = Poblaciones::find($id);
      if($poblacion->count() && $validar){
        $poblacion->update($request->all());
        return response()->json(['status'=>'success','msg'=> $poblacion['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $poblacion['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $poblacion = Poblaciones::find($id);
        if($poblacion->count()){
          $poblacion->delete();
          return response()->json(['status'=>'success','msg'=> $poblacion['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $poblacion['nombre'] ]);
        }
    }
    function sanitize($input) {
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = sanitize($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $input  = cleanInput($input);
            $output = mysql_real_escape_string($input);
        }
        return $output;
    }
}

