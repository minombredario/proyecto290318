<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Roles;

class RolesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $busqueda = $request->get('busqueda');
        
        $roles = Roles::where('nombre','like', '%'.$busqueda.'%')->orderBy('id', 'ASC')->paginate(8);
        
        $response = [
            'pagination' => [
                'total' => $roles->total(),
                'per_page' => $roles->perPage(),
                'current_page' => $roles->currentPage(),
                'last_page' => $roles->lastPage(),
                'from' => $roles->firstItem(),
                'to' => $roles->lastItem()
            ],
            
            'items' => $roles,
            
        ];   

        return response()->json($response);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);


        $create = Roles::create($request->all());
        return response()->json(['status' => 'success','msg'=>'Rol creado con éxito']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Roles::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);


      $rol = Roles::find($id);
      if($rol->count()){
        $rol->update($request->all());
        return response()->json(['status'=>'success','msg'=> $rol['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $rol['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $rol = Roles::find($id);
        if($rol->count()){
          $rol->delete();
          return response()->json(['status'=>'success','msg'=> $rol['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $rol['nombre'] ]);
        }
    }
}

