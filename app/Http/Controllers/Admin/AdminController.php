<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class AdminController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){
    	$admin = Auth::user();
        return view('admin.dashboard', compact('admin'));
    }

}
