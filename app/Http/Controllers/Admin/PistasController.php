<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Pistas;

class PistasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        

        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        
        $pistas = Pistas::where('nombre','like', '%'.$busqueda.'%')->orderBy('id', $orden)->paginate(8);
        /*$pistas = DB::table('pistas')
            ->selectRaw('id','CAST(nombre as integer)','slug')
            ->orderByRaw('CAST(nombre as integer)', $orden)
            ->paginate(8);*/

        $response = [
            'pagination' => [
                'total' => $pistas->total(),
                'per_page' => $pistas->perPage(),
                'current_page' => $pistas->currentPage(),
                'last_page' => $pistas->lastPage(),
                'from' => $pistas->firstItem(),
                'to' => $pistas->lastItem()
            ],
            
            'items' => $pistas,
            
        ];   

        return response()->json($response);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {

        $this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);

        try {
            $create = Pistas::create($request->all());
            $response = ['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito'];
        } catch (QueryException $e) {
            if(strpos($e, "Duplicate")){
                $response = ['status' => 'error','msg'=> 'Pista ' . $request->get('nombre') . ' registro duplicado'];
            }
        };
        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function show($id)
    {
        return Pistas::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function update(Request $request, $id)
    {
      	$this->validate($request, [
          'nombre' => 'required',
          'slug' => 'required',
        ]);

      $pista = Pistas::find($id);
      if($pista->count()){
        $pista->update($request->all());
        return response()->json(['status'=>'success','msg'=> $pista['nombre'] . ' actualizada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $pista['nombre'] ]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return IlluminateHttpResponse
     */
    public function destroy($id)
    {
        $pista = Pistas::find($id);
        if($pista->count()){
          $pista->delete();
          return response()->json(['status'=>'success','msg'=> $pista['nombre'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $pista['nombre'] ]);
        }
    }
}

