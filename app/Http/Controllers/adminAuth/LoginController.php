<?php

namespace App\Http\Controllers\adminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';
    protected $redirectAfterLogout = 'admin/iniciarSesion';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        $recuperarPass = "admin-password/reset";
        $dashboard = 'dashboardAdmin';

        return view('admin.login', compact(['recuperarPass','dashboard']));
    }

    public function login(Request $request)
    {
        
        $validar = $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        //compruebo que el correo esta.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);

        //si todo es correcto paso a la vista del dashboard
        if($validar && Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            return redirect()->intended(route('admin.dashboard'));
        };
        //si no es correcto redirijo a la dirección del login con el form data
        //return redirect()->back()->withInput($request->only('email','remember'));
        //return response()->json($request->only('email','remember'));
        
        $response = [
            'pass' => $this->sendFailedLoginResponse($request),
            'validar' => $validar,
            'recuperar' => $request->only('email','remember'),
        ];
        
        return response()->json($response);

        
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/iniciarSesion');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
