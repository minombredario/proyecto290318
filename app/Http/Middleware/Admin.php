<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        
        // Si el usuario no esta logueado, responde con un error  403.
        if ( ! Auth::check()) {
            abort(403, 'Unauthorized action.');
        }
        
        // Si el usuario es administrador, devuelvo la vista adminPanel.
        if (Auth::check() && Auth::user()->rol_id == 1) {

           return $next($request);
        }
        
        // The user is subscribed; continue with the request.
        return redirect('admin/login')->with('error','You have not admin access');
    }
}
