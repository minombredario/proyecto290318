<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidasUsuario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
       'partida_id', 'user_id', 'pala', 'pala2', 'pala3', 'pala4',
    ];

}
